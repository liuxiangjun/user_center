package com.moore.ms.api;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.common.collect.Maps;
import com.moore.ms.common.repository.AppCompanySubaccountRepository;
import com.moore.ms.common.response.ApiResponseEntity;
import com.moore.ms.common.response.ErrorDetail;
import com.moore.ms.common.utils.DateUtil;
import com.moore.ms.common.utils.HttpClientUtils;
import com.moore.ms.common.utils.MooreIdHash;
import com.moore.ms.common.utils.PasswordUtils;
import com.moore.ms.config.EnvConfig;
import com.moore.ms.domain.*;
import com.moore.ms.respository.UserRepository;
import com.moore.ms.service.*;
import com.moore.ms.vo.command.*;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.*;




@RestController
@RequestMapping(value = "/m/v1.0/authentication")
public class AuthenticationController {


    private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired private MooreIdHash mooreIdHash;
    @Autowired private EnvConfig envConfig;
    @Autowired private UserService userService;
    @Autowired private AuthenticationService authenticationService;
    @Autowired private WeixinMooreBindingService weixinMooreBindingService;
    @Autowired private AppMobileCodeService appMobileCodeService;
    @Autowired private DefaultKaptcha defaultKaptcha;
    @Autowired private DefaultKaptcha whiteKaptcha;
    @Autowired private FeedbackService feedbackService;
    @Value("${spring.profiles.active}")
    private String producation;
    @Autowired private EmailSendService emailSendService;
    @Autowired private WxMpService wxMpService;
    @Autowired private TaskExecutor taskExecutor;
    @Autowired private AppCompanySubaccountRepository appCompanySubaccountRepository;
    @Autowired private UserRepository userRepository;

    //图片验证码
    @RequestMapping(value = "/image-code-white",method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> imageCodeWhite() throws Exception {

        ApiResponseEntity are = new ApiResponseEntity();
        String capText = whiteKaptcha.createText().toLowerCase();
        BufferedImage bi = whiteKaptcha.createImage(capText);
        return imageCodeComm(are,capText,bi);

    }

    //图片验证码
    @RequestMapping(value = "/image-code",method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> imageCode() throws Exception {

        ApiResponseEntity are = new ApiResponseEntity();
        String capText = defaultKaptcha.createText().toLowerCase();
        BufferedImage bi = defaultKaptcha.createImage(capText);
        return imageCodeComm(are,capText,bi);
    }

    @RequestMapping(value = "/getUserCount",method = RequestMethod.GET)
    public long getUserCount() throws Exception {
        long userCount = userRepository.count();
        System.out.println("xxxuserCount:"+userCount);
        throw new Exception("这里是抛出异常");
//        return userCount;
    }

    @RequestMapping(value = "/getUserCountFileLog",method = RequestMethod.GET)
    public long getUserCountFileLog(){
        long userCount = userRepository.count();
        int a = 12 / 0;
        System.out.println(a);

        log.warn("getUserCountFileLog:"+userCount);
        return userCount;
    }


    // 手机号登陆
    @RequestMapping(value = "/phone-code",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> phoneCode(@RequestBody PhoneCodeCommand command, HttpServletRequest request) {

        ApiResponseEntity are = new ApiResponseEntity();
        if (command.getIsVoice() == null){
            command.setIsVoice(0);
        }
        if (StringUtils.isBlank(command.getPhone()) || StringUtils.isBlank(command.getCap()) || command.getType() == null){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }

        if (!command.getCap().equals(PasswordUtils.encodeCat(command.getCode().toLowerCase()))){
            are.addInfoError(new ErrorDetail("cap","cap.error","图形验证码错误"));
            return are.createResponseEntity();
        }

        List<AppMobileCode> codes = appMobileCodeService.getCodesByMobileToday(command.getPhone());
        if(CollectionUtils.isNotEmpty(codes) && codes.size() >=5){
            are.addInfoError(new ErrorDetail("over","over.send","今日发送次数过多，可联系客服"));
            return are.createResponseEntity();
        }

        User user = this.userService.findByPhone(command.getPhone());

        Integer isValidUser = 0;
        if (command.getType().intValue() == 0 && user != null){//手机注册
            are.addInfoError(new ErrorDetail("phone","phone.existed","手机已存在"));
        }else if (command.getType().intValue() == 1 && user == null){//手机密码找回
            are.addInfoError(new ErrorDetail("phone","phone.not_existed","手机不存在"));
        }
        
        if (command.getType() == 1){
            isValidUser = 1;
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }

        String ip = request.getHeader("X-Real-IP");
        try {
            HttpClientUtils.sendEmailWithParams(envConfig.getMooreServerForSms() + "sms/code", "phone=" + command.getPhone() + "&ip=" + ip + "&isValidUser=" + isValidUser+ "&type=" + command.getIsVoice());
        }catch (Exception e){
            are.addInfoError(new ErrorDetail("remote","remote.error","远程调用失败"));
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }
        Map<String,String> map = new HashMap<>();
        map.put("success","true");
        are.setData(map);
        return are.createResponseEntity();
    }


    //手机号注册，1）如果是微信端，则将openid绑定。2）注册用户通过utm标记来源
    @RequestMapping(value = "/phone-register",method = RequestMethod.PUT)
    public ResponseEntity<Map<String, Object>> phoneRegister(@RequestBody PhoneRegisterCommand command, HttpServletRequest request) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getPhone()) || StringUtils.isBlank(command.getPassword()) || StringUtils.isBlank(command.getCode()) || StringUtils.isBlank(command.getSource())){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }

        String code = appMobileCodeService.findByPhoneAndType(command.getPhone(),new Short("6"));
        User user = this.userService.findByPhone(command.getPhone());
        if (!code.equalsIgnoreCase(command.getCode())){
            are.addInfoError(new ErrorDetail("code","code.error","验证码错误"));
        }

        if (user != null){
            are.addInfoError(new ErrorDetail("phone","phone.existed","手机已存在"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }

        String ip = request.getHeader("X-Real-IP");

//        if (StringUtils.isBlank(command.getInviteCode())){
//            String inviteCode = HttpServletHelper.getCookie(request,"inviteCode");
//            command.setInviteCode(inviteCode);
//        }

        user = userService.createUser(command.getSource(),command.getPassword(),"",command.getPhone(),command.getUtmKey(),ip,command.getOpenId(),new Short("0"),command.getInviteCode(),command.getImportId(),new Short("1"),
                                    command.getUtmCampaign(),command.getUtmContent(),command.getUtmMedium(),command.getUtmSource(),command.getUtmTerm());


       if (StringUtils.isNotBlank(command.getOpenId())){
           this.weixinMooreBindingService.save(command.getOpenId(),user.getId(),user.getEmail());
       }

        //用户基本信息
        if (StringUtils.isNotBlank(command.getFullname())){
            user.setFullname(command.getFullname());
            user.setSchoolName(command.getSchoolName());
            user.setFieldOfStudy(command.getFieldOfStudy());
            user.setEducation(command.getEducation());
            user.setContactEmail(command.getEmail());
            this.userService.save(user);
//            this.userScoreService.saveUserScore(user);
        }

        String token = this.authenticationService.createToken(user.getId(),command.getOpenId());


        Map<String,Object> map = new HashMap<>();
        map.put("token",token);
        map.put("userId",user.getId());
        are.setData(map, HttpStatus.CREATED);

        return are.createResponseEntity();
    }

    //email 注册，1）如果是微信端，则将openid绑定。2）注册用户通过utm标记来源
    @RequestMapping(value = "/email-register",method = RequestMethod.PUT)
    public ResponseEntity<Map<String, Object>> emailRegister(@RequestBody EmailRegisterCommand command, HttpServletRequest request) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getEmail()) || StringUtils.isBlank(command.getPassword()) || StringUtils.isBlank(command.getSource())){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }

        User user = userService.findByEmail(command.getEmail().toLowerCase());

        if (user != null && (user.getAutoRegister() == null || user.getAutoRegister().intValue() == 0)){
            are.addInfoError(new ErrorDetail("email","emai.existed","邮箱已存在"));
        }

        String ip = request.getHeader("X-Real-IP");

//        if (StringUtils.isBlank(command.getInviteCode())){
//            String inviteCode = HttpServletHelper.getCookie(request,"inviteCode");
//            command.setInviteCode(inviteCode);
//
//        }
        AppCompanySubaccount subaccount = appCompanySubaccountRepository.findByHrEmail(command.getEmail().toLowerCase());
        if(subaccount != null){
            are.addInfoError(new ErrorDetail("hrEmail","hrEmail.existed","邮箱已存在"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }

        user = userService.createUser(command.getSource(),command.getPassword(),command.getEmail().toLowerCase(),"",command.getUtmKey(),ip,command.getOpenId(),new Short("0"),command.getInviteCode(),command.getImportId(),new Short("0"),
                command.getUtmCampaign(), command.getUtmContent(), command.getUtmMedium(), command.getUtmSource(), command.getUtmTerm());


        if (StringUtils.isNotBlank(command.getOpenId())){
            this.weixinMooreBindingService.save(command.getOpenId(),user.getId(),user.getEmail());
        }

        String token = this.authenticationService.createToken(user.getId(),command.getOpenId());


        Map<String,Object> map = new HashMap<>();
        map.put("token",token);
        map.put("userId",user.getId());
        are.setData(map, HttpStatus.CREATED);

        //注册成功后需要发送认证邮件  因为需要构建模板从而导致大量接口好使  改为异步处理
        String email = user.getEmail();
        Long userId = user.getId();
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpClientUtils.sendEmailWithParams(envConfig.getMooreServerForEmail() + "register/valid.email", "email=" + email + "&userId=" + userId + "&inviteCode=" + command.getInviteCode());
                }catch (Exception e){
                    e.printStackTrace();
//            are.addInfoError(new ErrorDetail("remote","remote.error","远程调用失败"));
//            are.getErrorEntity().setMessage("操作失败");
//                    return are.createResponseEntity();
                }
            }
        });



        return are.createResponseEntity();
    }


    @RequestMapping(value = "/phone-findpassword",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> phoneFindpassword(@RequestBody PhonePasswordCommand command) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getPhone()) || StringUtils.isBlank(command.getPassword()) || StringUtils.isBlank(command.getCode())){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("获取失败");
            return are.createResponseEntity();
        }

        String code = appMobileCodeService.findByPhoneAndType(command.getPhone(),new Short("6"));


        if (!code.equalsIgnoreCase(command.getCode())){
           are.addInfoError(new ErrorDetail("code","code.error","验证码错误"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }

        User user = this.userService.findByPhone(command.getPhone());
        user.setPassword(PasswordUtils.encodePassword(command.getPassword()));
        userService.save(user);

        Map<String,Object> map = new HashMap<>();
        map.put("success",true);
        are.setData(map);

        return are.createResponseEntity();
    }


    @RequestMapping(value = "/email-findpassword",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> emailFindpassword(@RequestParam(name = "email") String email,
                                                                 @RequestParam(name="bp",required = false) String bp) {

        ApiResponseEntity are = new ApiResponseEntity();
        User user = userService.findByEmail(email.toLowerCase());

        if (user == null){
            are.addInfoError(new ErrorDetail("email","email.not_existed","邮箱不存在"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }

        try {
            HttpClientUtils.sendEmailWithParams(envConfig.getMooreServerForEmail() + "findpwd.email", "email=" + email + (StringUtils.isNotBlank(bp) ? ("&jobId=" + bp) : ""));
        }catch (Exception e){
            are.addInfoError(new ErrorDetail("remote","remote.error","远程调用失败"));
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }


        Map<String,Object> map = new HashMap<>();
        map.put("success",true);
        are.setData(map);
        return are.createResponseEntity();
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> qrcode(@RequestBody UserDoLoginCommand command) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getUsername())){
            are.addInfoError(new ErrorDetail("username","username.isempty","邮箱或手机为空"));
            are.getErrorEntity().setMessage("登陆失败");
            return are.createResponseEntity();
        }

        User user = userService.findByEmailOrPhone(command.getUsername().toLowerCase());

        if (user == null){
            are.addInfoError(new ErrorDetail("username","username.not_existed","邮箱或手机不存在"));
        }

        if (user != null && !user.getPassword().equals(PasswordUtils.encodePassword(command.getPassword()))){
            are.addInfoError(new ErrorDetail("password","password.error","密码错误"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("登陆失败");
            return are.createResponseEntity();
        }

        Map<String,Object> data = Maps.newHashMap();

        //mobile登陆
        if(StringUtils.isBlank(command.getOpenId())){
            String token = authenticationService.createToken(user.getId(),"");
            data.put("token",token);
            data.put("userId",user.getId());
            are.setData(data);
            return are.createResponseEntity();
        }

        //微信绑定
        // TODO： 为什么有绑定的就返回错误
        boolean binding = weixinMooreBindingService.findIsUserBinded(user.getId());

        if (binding){
            are.addInfoError(new ErrorDetail("account","account.isbinded","该账户已被绑定"));
            are.getErrorEntity().setMessage("登陆失败");
            return are.createResponseEntity();
        }

        weixinMooreBindingService.save(command.getOpenId(),user.getId(),user.getEmail() == null ? "":user.getEmail());

        String token = authenticationService.createToken(user.getId(),command.getOpenId());
        data.put("token",token);
        data.put("userId",user.getId());
        are.setData(data);
        return are.createResponseEntity();

    }


    //验证码登陆
    @RequestMapping(value = "/login/code",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> login(@RequestBody UserDoLoginCommand command) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getUsername())){
            are.addInfoError(new ErrorDetail("username","username.isempty","手机为空"));
            are.getErrorEntity().setMessage("登陆失败");
            return are.createResponseEntity();
        }

        User user = userService.findByEmailOrPhone(command.getUsername().toLowerCase());

        String code = appMobileCodeService.findByPhoneAndType(command.getUsername(),new Short("6"));

        if (user == null){
            are.addInfoError(new ErrorDetail("user","user.error","用户不存在"));
        }

        if (!code.equalsIgnoreCase(command.getPassword())){
            are.addInfoError(new ErrorDetail("code","code.error","验证码错误"));
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("操作失败");
            return are.createResponseEntity();
        }

        Map<String,Object> data = Maps.newHashMap();

        //mobile登陆
        if(StringUtils.isBlank(command.getOpenId())){
            String token = authenticationService.createToken(user.getId(),"");
            data.put("token",token);
            are.setData(data);
            return are.createResponseEntity();
        }

        if(StringUtils.isNotBlank(command.getOpenId())){
            WeixinMooreBinding userBinding =  weixinMooreBindingService.findByUserId(user.getId());
            WeixinMooreBinding weixinBinding = weixinMooreBindingService.findByOpenId(command.getOpenId());
            if(userBinding == null && weixinBinding == null){
                WxMpUser wxMpUser = null;
                try {
                    wxMpUser = wxMpService.getUserService().userInfo(command.getOpenId(),"zh_CN");
                } catch (WxErrorException e) {
                    e.printStackTrace();
                }
                weixinBinding = new WeixinMooreBinding(command.getOpenId(),user.getId(),null,wxMpUser.getUnionId(),1);
                weixinBinding.setNickname(wxMpUser.getNickname());
                weixinBinding.setHeadimgurl(wxMpUser.getHeadImgUrl());
                weixinMooreBindingService.save(weixinBinding);
            }
        }


        String token = authenticationService.createToken(user.getId(),command.getOpenId());
        data.put("token",token);
        are.setData(data);
        return are.createResponseEntity();


    }


    @RequestMapping(value = "/autologin",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> autoLogin(@RequestParam(value = "userId",required = false) Long userId,
                                                         @RequestParam(value = "importId",required = false) String importId,
                                                         @RequestParam(value = "phone",required = false) String phone,
                                                         @RequestParam(value = "email",required = false) String email,
                                                         @RequestParam(value = "newemail",required = false) String newemail,
                                                         @RequestParam(value = "newphone",required = false) String newphone) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isNotBlank(newemail) || StringUtils.isNotBlank(newphone)){
            if (userId == null){
                are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
                are.getErrorEntity().setMessage("登陆失败");
                return are.createResponseEntity();
            }
        }

        User user = null;

        if (userId != null && userId != 0 && StringUtils.isBlank(newemail)&& StringUtils.isBlank(newphone) ){
            user = userService.findByUserId(userId);
        }else if (StringUtils.isNotBlank(importId)){
            user = userService.findByImportId(importId);
        }else if (StringUtils.isNotBlank(email)){
            user = userService.findByEmail(email);
        }else if(StringUtils.isNotBlank(phone)){
            user = userService.findByPhone(phone);
        }else if (StringUtils.isNotBlank(newemail)){
            user = userService.findByEmail(newemail);
            if (user != null && !user.getId().equals(userId)){
                are.addInfoError(new ErrorDetail("user","user.error","用户信息错误"));
                are.getErrorEntity().setMessage("登陆失败");
                return are.createResponseEntity();
            }
        }else if(StringUtils.isNotBlank(newphone)){
            user = userService.findByPhone(newphone);
            if (user != null && !user.getId().equals(userId)){
                are.addInfoError(new ErrorDetail("user","user.error","用户信息错误"));
                are.getErrorEntity().setMessage("登陆失败");
                return are.createResponseEntity();
            }
        }

        if (user == null){
            are.addInfoError(new ErrorDetail("user","user.not_existed","用户不存在"));
            are.getErrorEntity().setMessage("登陆失败");
            return are.createResponseEntity();
        }

        user.setLastLoginTime(new Date());
        this.userService.save(user);


        Map<String,Object> data = Maps.newHashMap();

        String token = authenticationService.createToken(user.getId(),"");
        data.put("token",token);
        are.setData(data);
        return are.createResponseEntity();


    }


    @RequestMapping(value = "/feedback",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> feedback(@RequestBody FeedbackCommand command) {

        ApiResponseEntity are = new ApiResponseEntity();

        if ((StringUtils.isBlank(command.getEmail()) && StringUtils.isBlank(command.getMobile()) && StringUtils.isBlank(command.getOpenId()) ) || StringUtils.isBlank(command.getSource()) ){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("保存失败");
            return are.createResponseEntity();
        }

        if (are.hasError()){
            are.getErrorEntity().setMessage("保存失败");
            return are.createResponseEntity();
        }

        if (StringUtils.isBlank(command.getType())){
            command.setType("0");
        }

        Map<String,Object> data = Maps.newHashMap();

        feedbackService.addFeedBack(command.getEmail().toLowerCase(),command.getContent(),command.getSource(),command.getUserId(),command.getOpenId(),command.getType(),command.getMobile());


        if(command.getType().equals("2")){
            String email = "yiqiang@moore.ren";
            if (producation.equalsIgnoreCase("prod")){
                email = "pd.service@moore.ren";
            }
            Map<String, Object> map = new HashMap<>();
            map.put("time", DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
            map.put("mobile",command.getMobile() );
            map.put("content",command.getContent() );
            String content = this.emailSendService.buildEmailContentByTemplate(map,"feedback/index");
            this.emailSendService.sendSubmail("用户反馈","摩尔人半导体招聘",content,email,"feedback_not_get_code");
        }

        data.put("success",true);
        are.setData(data);
        return are.createResponseEntity();


    }

    /**
     * 微信软文查看权限
     * @return
     */
    @RequestMapping(value = "/read/check",method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> readCheck(@RequestParam("openId") String openId) {

        ApiResponseEntity are = new ApiResponseEntity();
        boolean hasAuth= false;

        WeixinMooreBinding binding = weixinMooreBindingService.findByOpenId(openId);

        boolean subscribe = false;
        try {
            WxMpUser wxMpUser = this.wxMpService.getUserService().userInfo(openId,"zh_CN");
            subscribe = wxMpUser.getSubscribe();
        }catch (Exception e){

        }

        if (binding != null){
            User user = userService.findByUserId(binding.getUserId());
            if (user != null && user.getIsCompleteStudentIntension() != null && user.getIsCompleteStudentIntension().intValue() == 1 && subscribe){
                hasAuth = true;
            }
        }

        Map<String,Object> data = Maps.newHashMap();
        data.put("hasAuth",hasAuth);
        are.setData(data);
        return are.createResponseEntity();


    }


    @RequestMapping(value = "/email/validata",method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> emailValidata(@RequestParam("code") String code) {

        ApiResponseEntity are = new ApiResponseEntity();
        Map<String,Object> data = this.authenticationService.checkEmailValidataByCode(code);
        are.setData(data);
        return are.createResponseEntity();

    }


    @RequestMapping(value = "/email/reset-password",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> resetPassword(@RequestBody ResetPasswordCommand command) {

        ApiResponseEntity are = new ApiResponseEntity();

        if (StringUtils.isBlank(command.getEmail()) || StringUtils.isBlank(command.getPassword())){
            are.addInfoError(new ErrorDetail("params","params.error","参数错误"));
            are.getErrorEntity().setMessage("保存失败");
            return are.createResponseEntity();
        }

        User user = userService.findByEmail(command.getEmail());
        user.setPassword(PasswordUtils.encodePassword(command.getPassword()));
        this.userService.save(user);

        Map<String,Object> data = Maps.newHashMap();
        data.put("success",true);
        are.setData(data);
        return are.createResponseEntity();


    }


    //////////////private method //////////////////

    private ResponseEntity<Map<String, Object>> imageCodeComm(ApiResponseEntity are,String capText, BufferedImage bi) throws Exception{

        Base64.Encoder base64 = Base64.getEncoder();
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        String cat = PasswordUtils.encodeCat(capText);
        // write the data out
        Map<String,String> map = new HashMap<>();
        ImageIO.setUseCache(false);
        ImageIO.write(bi, "jpg", bs);
        String imgsrc = null;
        try {
            map.put("cap",cat);
            imgsrc = base64.encodeToString(bs.toByteArray());
            map.put("imgsrc","data:image/jpg;base64,"+imgsrc);
            are.setData(map);
            bs.flush();
        } finally {
            bs.close();
        }
        return are.createResponseEntity();
    }


}
