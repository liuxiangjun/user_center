package com.moore.ms.sms;


import cn.submail.sdk.config.AppConfig;
import com.alibaba.fastjson.JSONObject;
import com.moore.ms.common.utils.HttpClientUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SubmailSmsSender {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${sms.active}")
    public String active;

    @Value("${sms.sendcloud.smsurl}")
    private String sendcloudUrl;

    @Value("${sms.sendcloud.smsKey}")
    private String sendcloudKey;

    @Value("${sms.sendcloud.smsUser}")
    private String sendcloudUser;

    @Autowired
    private AppConfig appConfig;


	public String send(String phone, Map<String,String> variable, String project) {
        if (active.equals("submail")){
            MESSAGEXsend submail = new MESSAGEXsend(appConfig);
            submail.addTo(phone);
            submail.setProject(project);
            for (Map.Entry<String,String> entry : variable.entrySet()){
                submail.addVar(entry.getKey(),entry.getValue());
            }
            return submail.xsendSms();
        }else{

            String params = "smsUser="+ sendcloudUser +"&templateId="+project+"&phone=" + phone+"&vars="+ JSONObject.toJSONString(variable);
            String signature = sendcloudKey +"&"+ params + "&"+ sendcloudKey;
            signature = DigestUtils.md5Hex(signature);

            try {
                String resultStr = HttpClientUtils.normalGet(sendcloudUrl+"?"+params + "&signature="+signature);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

	}
}
