package com.moore.ms.sms.entity;

/**
 * Created by maqi on 16/8/5.
 */
public class SmsTemplateVariable {
//    private String t1 = "@var(user_name)您的面试安排有修改， @var{company_shortname}-@var(job_title)的面试。修改为，时间：@var(interview_time)；" +
//            "方式：电话面试；联系人：@var(hr_name)；电话：@var(hr_contact_phone)；如面试时间不合适，请第一时间联系HR。具体关注@var(web_address)";
//
//    private String t2 = "@var(user_name)您的面试安排有修改， 自@var{company_shortname}-@var(job_title)的面试。修改为，时间：@var(interview_time)；" +
//            "地址：@var(company_address)；联系人：@var(hr_name)；电话：@var(hr_contact_phone)；如面试时间不合适，请第一时间联系HR。具体关注@var(web_address)";
//
//    private String t3 = "@var(user_name)，您有来自@var(company_shortname)-@var(job_title)的面试通知。时间:@var(interview_time)；" +
//            "方式:电话面试；联系人:@var(hr_name)；电话:@var(hr_contact_phone); 如面试时间不合适，请第一时间联系HR。" +
//            "具体关注@var(web_address)";
//
//    private String t4 = "@var(user_name)，您有来自@var(company_shortname)-@var(job_title)的面试通知。时间:@var(interview_time)；" +
//            "地址：@var(company_address)；联系人：@var(hr_name)；电话:@var(hr_contact_phone)；如面试时间不合适，请第一时间联系HR。" +
//            "具体关注@var(web_address)";

    private String user_name;
    private String company_shortname;
    private String company_address;
    private String job_title;
    private String interview_time;
    private String hr_name;
    private String hr_contact_phone;
    private String web_address;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCompany_shortname() {
        return company_shortname;
    }

    public void setCompany_shortname(String company_shortname) {
        this.company_shortname = company_shortname;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getInterview_time() {
        return interview_time;
    }

    public void setInterview_time(String interview_time) {
        this.interview_time = interview_time;
    }

    public String getHr_name() {
        return hr_name;
    }

    public void setHr_name(String hr_name) {
        this.hr_name = hr_name;
    }

    public String getHr_contact_phone() {
        return hr_contact_phone;
    }

    public void setHr_contact_phone(String hr_contact_phone) {
        this.hr_contact_phone = hr_contact_phone;
    }

    public String getWeb_address() {
        return web_address;
    }

    public void setWeb_address(String web_address) {
        this.web_address = web_address;
    }

    @Override
    public String toString() {
        return "SmsTemplateVariable{" +
                "user_name='" + user_name + '\'' +
                ", company_shortname='" + company_shortname + '\'' +
                ", company_address='" + company_address + '\'' +
                ", job_title='" + job_title + '\'' +
                ", interview_time='" + interview_time + '\'' +
                ", hr_name='" + hr_name + '\'' +
                ", hr_contact_phone='" + hr_contact_phone + '\'' +
                ", web_address='" + web_address + '\'' +
                '}';
    }
}
