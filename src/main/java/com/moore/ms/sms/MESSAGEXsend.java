package com.moore.ms.sms;

import cn.submail.sdk.config.AppConfig;
import cn.submail.sdk.config.MailConfig;
import cn.submail.sdk.config.MessageConfig;

/**
 * Created by 汤云飞 on 2017/3/29.
 */
public class MESSAGEXsend extends SenderWapper {
    /**
     * If the mode is mail,it's an instance of {@link MailConfig}. If the mode
     * is message,it's an instance of {@link MessageConfig}
     * */
    protected AppConfig config = null;
    public static final String ADDRESSBOOK = "addressbook";
    public static final String TO = "to";
    public static final String PROJECT = "project";
    public static final String VARS = "vars";
    public static final String LINKS = "links";

    public MESSAGEXsend(AppConfig config) {
        this.config = config;
    }

    public void addTo(String address) {
        requestData.addWithComma(TO, address);
    }

    public void addAddressBook(String addressbook) {
        requestData.addWithComma(ADDRESSBOOK, addressbook);
    }

    public void setProject(String project) {
        requestData.put(PROJECT, project);
    }

    public void addVar(String key, String val) {
        requestData.addWithJson(VARS, key, val);
    }

    @Override
    public ISender getSender() {
        return new Message(this.config);
    }

    public void xsend(){
        getSender().xsend(requestData);
    }

    public String xsendSms(){
        return getSender().xsendSms(requestData);
    }
}
