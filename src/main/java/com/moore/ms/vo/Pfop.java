package com.moore.ms.vo;

/**
 * Created by pickj on 2017/11/6.
 */
public class Pfop {
    private String persistentId;
    private String inputKey;
    private String inputBucket;

    public String getPersistentId() {
        return persistentId;
    }

    public void setPersistentId(String persistentId) {
        this.persistentId = persistentId;
    }

    public String getInputKey() {
        return inputKey;
    }

    public void setInputKey(String inputKey) {
        this.inputKey = inputKey;
    }

    public String getInputBucket() {
        return inputBucket;
    }

    public void setInputBucket(String inputBucket) {
        this.inputBucket = inputBucket;
    }
}
