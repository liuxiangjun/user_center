package com.moore.ms.vo.resume;

public class Job {

    private String id;

    private Education education;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }
}
