package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/21.
 */
public class CollectCompanyCommand {

    private Long companyId;
    private Short isDelete;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }
}
