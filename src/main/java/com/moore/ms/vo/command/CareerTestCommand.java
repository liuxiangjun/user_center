package com.moore.ms.vo.command;

/**
 * Created by pickj on 2017/12/9.
 */
public class CareerTestCommand {
    private String fullname;
    private String company;
    private String jobTitle;
    private String phone;
    private String salary;
    private String salaryraise;
    private Integer chain;
    private Long oneFunction;
    private Long function;
    private String level;
    private String city;
    private String expectZipcode;
    private Short workyear;
    private Short edu;
    private String openId;
    private String email;
    private String ip;
    private String school_name;
    private String field_of_study;
    private String utmKey;
    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;
    private String join_time;
    private String finish_school_year;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalaryraise() {
        return salaryraise;
    }

    public void setSalaryraise(String salaryraise) {
        this.salaryraise = salaryraise;
    }

    public Integer getChain() {
        return chain;
    }

    public void setChain(Integer chain) {
        this.chain = chain;
    }

    public Long getOneFunction() {
        return oneFunction;
    }

    public void setOneFunction(Long oneFunction) {
        this.oneFunction = oneFunction;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Short getWorkyear() {
        return workyear;
    }

    public void setWorkyear(Short workyear) {
        this.workyear = workyear;
    }

    public Short getEdu() {
        return edu;
    }

    public void setEdu(Short edu) {
        this.edu = edu;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }



    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getField_of_study() {
        return field_of_study;
    }

    public void setField_of_study(String field_of_study) {
        this.field_of_study = field_of_study;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getJoin_time() {
        return join_time;
    }

    public void setJoin_time(String join_time) {
        this.join_time = join_time;
    }

    public String getFinish_school_year() {
        return finish_school_year;
    }

    public void setFinish_school_year(String finish_school_year) {
        this.finish_school_year = finish_school_year;
    }

    @Override
    public String toString() {
        return "CareerTestCommand{" +
                "fullname='" + fullname + '\'' +
                ", company='" + company + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", phone='" + phone + '\'' +
                ", salary='" + salary + '\'' +
                ", salaryraise='" + salaryraise + '\'' +
                ", chain=" + chain +
                ", oneFunction=" + oneFunction +
                ", function=" + function +
                ", level='" + level + '\'' +
                ", city='" + city + '\'' +
                ", expectZipcode='" + expectZipcode + '\'' +
                ", workyear=" + workyear +
                ", edu=" + edu +
                ", openId='" + openId + '\'' +
                ", email='" + email + '\'' +
                ", ip='" + ip + '\'' +
                ", school_name='" + school_name + '\'' +
                ", field_of_study='" + field_of_study + '\'' +
                ", utmKey='" + utmKey + '\'' +
                ", utmSource='" + utmSource + '\'' +
                ", utmMedium='" + utmMedium + '\'' +
                ", utmCampaign='" + utmCampaign + '\'' +
                ", utmContent='" + utmContent + '\'' +
                ", utmTerm='" + utmTerm + '\'' +
                ", join_time='" + join_time + '\'' +
                ", finish_school_year='" + finish_school_year + '\'' +
                '}';
    }
}
