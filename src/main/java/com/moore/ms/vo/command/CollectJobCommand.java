package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/16.
 */
public class CollectJobCommand {

    private Long companyJobId;
    private Short isDelete;


    public Long getCompanyJobId() {
        return companyJobId;
    }

    public void setCompanyJobId(Long companyJobId) {
        this.companyJobId = companyJobId;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }
}
