package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/22.
 */
public class AvatarCommand {

    private String cropping;

    private String avatar;

    public String getCropping() {
        return cropping;
    }

    public void setCropping(String cropping) {
        this.cropping = cropping;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
