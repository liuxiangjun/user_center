package com.moore.ms.vo.command;


/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserBaseInfoCommand {

    private String fullname;
    private Short sex;
    private String mobilePhone;
    private String contactEmail;
    private Short workingYear;
    private Short education;
    private String currentZipcode;
    private String currentCityName;
    private String currentCompanyName;
    private String currentJobTitle;
    private Short englishListenLevel;
    private Short englishReadLevel;
    private String finishSchoolYear;
    private Short groupType;
    private String researchDirection;
    private Short birthdayYear;


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public String getCurrentZipcode() {
        return currentZipcode;
    }

    public void setCurrentZipcode(String currentZipcode) {
        this.currentZipcode = currentZipcode;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Short getEnglishListenLevel() {
        return englishListenLevel;
    }

    public void setEnglishListenLevel(Short englishListenLevel) {
        this.englishListenLevel = englishListenLevel;
    }

    public Short getEnglishReadLevel() {
        return englishReadLevel;
    }

    public void setEnglishReadLevel(Short englishReadLevel) {
        this.englishReadLevel = englishReadLevel;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public Short getBirthdayYear() {
        return birthdayYear;
    }

    public void setBirthdayYear(Short birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    @Override
    public String toString() {
        return "UserBaseInfoCommand{" +
                "fullname='" + fullname + '\'' +
                ", sex=" + sex +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", workingYear=" + workingYear +
                ", education=" + education +
                ", currentZipcode='" + currentZipcode + '\'' +
                ", currentCityName='" + currentCityName + '\'' +
                ", currentCompanyName='" + currentCompanyName + '\'' +
                ", currentJobTitle='" + currentJobTitle + '\'' +
                ", englishListenLevel=" + englishListenLevel +
                ", englishReadLevel=" + englishReadLevel +
                ", finishSchoolYear='" + finishSchoolYear + '\'' +
                ", groupType=" + groupType +
                ", researchDirection='" + researchDirection + '\'' +
                ", birthdayYear=" + birthdayYear +
                '}';
    }
}
