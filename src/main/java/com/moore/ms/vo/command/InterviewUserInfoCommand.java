package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class InterviewUserInfoCommand {

    private String fullname;

    private String school_name;

    private String field_of_study;

    private Short edu;

    private String searchdirection;

    private String finish_year;

    private String cityNames;

    private String cityCodes;

    private Long function;

    private String mobile;

    private String verifycode;

    private String isVerify;

    private String utmSource;

    private String utmMedium;

    private String utmCampaign;

    private String utmContent;

    private String utmTerm;

    private Short intention = new Short("0");

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getField_of_study() {
        return field_of_study;
    }

    public void setField_of_study(String field_of_study) {
        this.field_of_study = field_of_study;
    }

    public Short getEdu() {
        return edu;
    }

    public void setEdu(Short edu) {
        this.edu = edu;
    }

    public String getSearchdirection() {
        return searchdirection;
    }

    public void setSearchdirection(String searchdirection) {
        this.searchdirection = searchdirection;
    }

    public String getFinish_year() {
        return finish_year;
    }

    public void setFinish_year(String finish_year) {
        this.finish_year = finish_year;
    }

    public String getCityNames() {
        return cityNames;
    }

    public void setCityNames(String cityNames) {
        this.cityNames = cityNames;
    }

    public String getCityCodes() {
        return cityCodes;
    }

    public void setCityCodes(String cityCodes) {
        this.cityCodes = cityCodes;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getVerifycode() {
        return verifycode;
    }

    public void setVerifycode(String verifycode) {
        this.verifycode = verifycode;
    }

    public String getIsVerify() {
        return isVerify;
    }

    public void setIsVerify(String isVerify) {
        this.isVerify = isVerify;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public Short getIntention() {
        return intention;
    }

    public void setIntention(Short intention) {
        this.intention = intention;
    }
}
