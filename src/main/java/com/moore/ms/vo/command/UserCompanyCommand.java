package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserCompanyCommand {

    private Long id;

    private String comapnyName;

    private String jobTitle;

    private String joinTime;

    private String leaveTime;

    private String jobContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComapnyName() {
        return comapnyName;
    }

    public void setComapnyName(String comapnyName) {
        this.comapnyName = comapnyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getJobContent() {
        return jobContent;
    }

    public void setJobContent(String jobContent) {
        this.jobContent = jobContent;
    }

    @Override
    public String toString() {
        return "UserCompanyCommand{" +
                "id=" + id +
                ", comapnyName='" + comapnyName + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", joinTime='" + joinTime + '\'' +
                ", leaveTime='" + leaveTime + '\'' +
                ", jobContent='" + jobContent + '\'' +
                '}';
    }
}
