package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserInfoCommand {

    private String openId;

    private String fullname;

    private String schoolName;

    private String fieldOfStudy;

    private Short education;

    private String finishSchoolYear;

    private String mobile;

    private String email;

    private String code;

    private String utmKey;

    private Long activityId;

    private Long companyId;

    private Long jobId;


    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;



    private String expectCityName;

    private String expectZipcode;

    private Short channelByEmail;
    private Short channelByWechat;
    private Short channelBySms;

    private Long schoolOneFunction;

    private Float currentSalaryYear;

    private String researchDirection;
    private String dtxOpenId;

    private Short intention = new Short("0");

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Short getChannelByEmail() {
        return channelByEmail;
    }

    public void setChannelByEmail(Short channelByEmail) {
        this.channelByEmail = channelByEmail;
    }

    public Short getChannelByWechat() {
        return channelByWechat;
    }

    public void setChannelByWechat(Short channelByWechat) {
        this.channelByWechat = channelByWechat;
    }

    public Short getChannelBySms() {
        return channelBySms;
    }

    public void setChannelBySms(Short channelBySms) {
        this.channelBySms = channelBySms;
    }

    public Long getSchoolOneFunction() {
        return schoolOneFunction;
    }

    public void setSchoolOneFunction(Long schoolOneFunction) {
        this.schoolOneFunction = schoolOneFunction;
    }

    public Float getCurrentSalaryYear() {
        return currentSalaryYear;
    }

    public void setCurrentSalaryYear(Float currentSalaryYear) {
        this.currentSalaryYear = currentSalaryYear;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    public Short getIntention() {
        return intention;
    }

    public void setIntention(Short intention) {
        this.intention = intention;
    }

    public String getDtxOpenId() {
        return dtxOpenId;
    }

    public void setDtxOpenId(String dtxOpenId) {
        this.dtxOpenId = dtxOpenId;
    }
}
