package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserExpectCommand {

    private String expectJobTitle;

    private Short expectJobStatus;

    private String expectCityName;

    private String expectZipcode;

    private Short expectJobSalary;

    private String expectJobDescription;

    public String getExpectJobTitle() {
        return expectJobTitle;
    }

    public void setExpectJobTitle(String expectJobTitle) {
        this.expectJobTitle = expectJobTitle;
    }

    public Short getExpectJobStatus() {
        return expectJobStatus;
    }

    public void setExpectJobStatus(Short expectJobStatus) {
        this.expectJobStatus = expectJobStatus;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Short getExpectJobSalary() {
        return expectJobSalary;
    }

    public void setExpectJobSalary(Short expectJobSalary) {
        this.expectJobSalary = expectJobSalary;
    }

    public String getExpectJobDescription() {
        return expectJobDescription;
    }

    public void setExpectJobDescription(String expectJobDescription) {
        this.expectJobDescription = expectJobDescription;
    }

    @Override
    public String toString() {
        return "UserExpectCommand{" +
                "expectJobTitle='" + expectJobTitle + '\'' +
                ", expectJobStatus=" + expectJobStatus +
                ", expectCityName='" + expectCityName + '\'' +
                ", expectZipcode='" + expectZipcode + '\'' +
                ", expectJobSalary=" + expectJobSalary +
                ", expectJobDescription='" + expectJobDescription + '\'' +
                '}';
    }
}
