package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/24.
 */
public class SendCloudHookCommand {
    private String event;
    private String message;
    private String apiUser;
    private Long maillistTaskId;
    private String emailId;
    private String recipient;
    private Integer labelId;
    private Long timestamp;
    private String token;
    private String signature;
    private String ip;
    private String explorerName;
    private String explorerVer;
    private String oSName;
    private String oSVer;
    private String url;
    private Integer subStat;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public Long getMaillistTaskId() {
        return maillistTaskId;
    }

    public void setMaillistTaskId(Long maillistTaskId) {
        this.maillistTaskId = maillistTaskId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getExplorerName() {
        return explorerName;
    }

    public void setExplorerName(String explorerName) {
        this.explorerName = explorerName;
    }

    public String getExplorerVer() {
        return explorerVer;
    }

    public void setExplorerVer(String explorerVer) {
        this.explorerVer = explorerVer;
    }

    public String getoSName() {
        return oSName;
    }

    public void setoSName(String oSName) {
        this.oSName = oSName;
    }

    public String getoSVer() {
        return oSVer;
    }

    public void setoSVer(String oSVer) {
        this.oSVer = oSVer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public Integer getSubStat() {
        return subStat;
    }

    public void setSubStat(Integer subStat) {
        this.subStat = subStat;
    }
}
