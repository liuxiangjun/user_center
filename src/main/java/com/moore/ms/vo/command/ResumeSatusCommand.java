package com.moore.ms.vo.command;

/**
 *
 * add by 李登修  2018/9/11
 *
 */
public class ResumeSatusCommand {

    private Short isresumePublic;

    private Short currentJobSatus;

    private String blockdomain;

    public Short getIsresumePublic() {
        return isresumePublic;
    }

    public void setIsresumePublic(Short isresumePublic) {
        this.isresumePublic = isresumePublic;
    }

    public Short getCurrentJobSatus() {
        return currentJobSatus;
    }

    public void setCurrentJobSatus(Short currentJobSatus) {
        this.currentJobSatus = currentJobSatus;
    }

    public String getBlockdomain() {
        return blockdomain;
    }

    public void setBlockdomain(String blockdomain) {
        this.blockdomain = blockdomain;
    }
}
