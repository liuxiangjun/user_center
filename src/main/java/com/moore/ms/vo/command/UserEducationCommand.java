package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserEducationCommand {

    private Long id;

    private String schoolName;

    private String fieldOfStudy;

    private Short degree;

    private String finishYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getDegree() {
        return degree;
    }

    public void setDegree(Short degree) {
        this.degree = degree;
    }

    public String getFinishYear() {
        return finishYear;
    }

    public void setFinishYear(String finishYear) {
        this.finishYear = finishYear;
    }

    @Override
    public String toString() {
        return "UserEducationCommand{" +
                "id=" + id +
                ", schoolName='" + schoolName + '\'' +
                ", fieldOfStudy='" + fieldOfStudy + '\'' +
                ", degree=" + degree +
                ", finishYear='" + finishYear + '\'' +
                '}';
    }
}
