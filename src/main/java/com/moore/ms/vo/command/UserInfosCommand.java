package com.moore.ms.vo.command;

import org.apache.commons.lang3.StringUtils;
import com.moore.ms.domain.User;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserInfosCommand {

    private Long userId;

    private String mobile;


    private String cityName;

    private Integer cityId;

    private Short userWorkYear;

    public UserInfosCommand() {
    }

    public UserInfosCommand(User user) {
        this.userId = user.getId();
        this.mobile = user.getMobilePhone();
        if (StringUtils.isNotBlank(user.getCurrentCityName())){
            this.cityName = user.getCurrentCityName();
            if (StringUtils.isNotBlank(user.getCurrentZipcode())){
                try {
                    this.cityId = Integer.valueOf(user.getCurrentZipcode());
                }catch(NumberFormatException ex){
                    ex.printStackTrace();
                }
            }
        }
        if (StringUtils.isNotBlank(user.getExpectCityName())){
            this.cityName = user.getExpectCityName();
            if (StringUtils.isNotBlank(user.getExpectZipcode())){
//                this.cityId = Integer.valueOf(user.getExpectZipcode());
            }
        }

        if (user.getWorkingYear() != null){
            this.userWorkYear = user.getWorkingYear();
        }else{
            this.userWorkYear = new Short("0");
        }

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Short getUserWorkYear() {
        return userWorkYear;
    }

    public void setUserWorkYear(Short userWorkYear) {
        this.userWorkYear = userWorkYear;
    }
}
