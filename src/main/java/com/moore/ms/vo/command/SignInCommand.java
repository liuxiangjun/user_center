package com.moore.ms.vo.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;
import com.moore.ms.domain.SignInCustomKey;
import com.moore.ms.domain.SignInCustomValue;

import java.util.List;
import java.util.Map;

public class SignInCommand {
    private Map signInfo;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String openId;
    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long userId;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String name;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobile;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String code;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String schoolName;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fieldOfStudy;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Short education;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishSchoolYear;
    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short groupType;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmKey;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmCampaign;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmContent;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmMedium;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmSource;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String utmTerm;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String dtxOpenId;
    private List<SignInCustomKey> customKeys;
    private List<SignInCustomValue> customValues;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public List<SignInCustomKey> getCustomKeys() {
        return customKeys;
    }

    public void setCustomKeys(List<SignInCustomKey> customKeys) {
        this.customKeys = customKeys;
    }

    public Map getSignInfo() {
        return signInfo;
    }

    public void setSignInfo(Map signInfo) {
        this.signInfo = signInfo;
    }

    public List<SignInCustomValue> getCustomValues() {
        return customValues;
    }

    public void setCustomValues(List<SignInCustomValue> customValues) {
        this.customValues = customValues;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public String getDtxOpenId() {
        return dtxOpenId;
    }

    public void setDtxOpenId(String dtxOpenId) {
        this.dtxOpenId = dtxOpenId;
    }
}
