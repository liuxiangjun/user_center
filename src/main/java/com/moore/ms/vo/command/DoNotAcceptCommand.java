package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/19.
 */
public class DoNotAcceptCommand {
    private Long id;
    private String reason;
    private String otherreason;
    private String airId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOtherreason() {
        return otherreason;
    }

    public void setOtherreason(String otherreason) {
        this.otherreason = otherreason;
    }

    @Override
    public String toString() {
        return "DoNotAcceptCommand{" +
                "id=" + id +
                ", reason='" + reason + '\'' +
                ", otherreason='" + otherreason + '\'' +
                '}';
    }

    public String getAirId() {
        return airId;
    }

    public void setAirId(String airId) {
        this.airId = airId;
    }
}
