package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/12/7.
 */
public class NotifyInterviewCommand {
    /*
    协商的key
     */
    private String key;
    /*
    用户id
     */
    private Long userId;

    /*
    欢迎词
     */
    private String message;

    /*
    职位名称
     */
    private String jobTitle;

    /*
    公司简称
     */
    private String companyName;

    /*
    面试地点
     */
    private String address;

    /*
    面试时间
     */
    private String time;

    /*
    联系人电话
     */
    private String phone;

    /*
    联系人
     */
    private String contact;

    /*
    备注
     */
    private String remark;

    /**
     * 职位Id
     */
    private Long jobId;

    /**
     * 邀请记录Id
     */
    private Long recordId;

    /**
     * 邀请记录Id
     */
    private Long deliverId;

    /*
    类型 1:面试提醒，2:明日面试提醒，3:重新安排面试
     */
    private Integer type;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Long deliverId) {
        this.deliverId = deliverId;
    }
}
