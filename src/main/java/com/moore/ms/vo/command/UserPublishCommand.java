package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserPublishCommand {

    private Long id;

    private String title;

    private String publications;

    private String publishTime;

    private String jointlyPeople;

    private String description;

    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublications() {
        return publications;
    }

    public void setPublications(String publications) {
        this.publications = publications;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getJointlyPeople() {
        return jointlyPeople;
    }

    public void setJointlyPeople(String jointlyPeople) {
        this.jointlyPeople = jointlyPeople;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "UserPublishCommand{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", publications='" + publications + '\'' +
                ", publishTime='" + publishTime + '\'' +
                ", jointlyPeople='" + jointlyPeople + '\'' +
                ", description='" + description + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
