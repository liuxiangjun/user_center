package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserIntentionInfoCommand {

    private String openId;

    private String fullname;

    private String companyName;

    private String expectCityName;

    private String expectZipcode;

    private Long functionId;

    private Short intention = new Short("0");

    private Short rank;

    private String mobile;

    private String email;

    private String utmKey;

    private Long activityId;

    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;

    private Short channelByEmail;
    private Short channelByWechat;
    private Short channelBySms;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Short getRank() {
        return rank;
    }

    public void setRank(Short rank) {
        this.rank = rank;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public Short getChannelByEmail() {
        return channelByEmail;
    }

    public void setChannelByEmail(Short channelByEmail) {
        this.channelByEmail = channelByEmail;
    }

    public Short getChannelByWechat() {
        return channelByWechat;
    }

    public void setChannelByWechat(Short channelByWechat) {
        this.channelByWechat = channelByWechat;
    }

    public Short getChannelBySms() {
        return channelBySms;
    }

    public void setChannelBySms(Short channelBySms) {
        this.channelBySms = channelBySms;
    }

    public Short getIntention() {
        return intention;
    }

    public void setIntention(Short intention) {
        this.intention = intention;
    }
}
