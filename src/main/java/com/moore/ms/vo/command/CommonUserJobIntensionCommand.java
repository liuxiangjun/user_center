package com.moore.ms.vo.command;

/**
 * Created by pickj on 2017/9/17.
 */
public class CommonUserJobIntensionCommand {
    private String userId;
    private String openId;
    private String phone;
    private String email;
    private String groupType;
    private String expectCityName;
    private String expectZipcode;
    private String sex;
    private String chainId;
    private String functionId;
    private String rank;
    private String currentSalaryYear;
    private String fullname;
    private String schoolName ;
    private String fieldOfStudy ;
    private String education ;
    private String researchDirection ;
    private String finishSchoolYear ;
    private String schoolOneFunction ;
    private String utmSource ;
    private String utmMedium ;
    private String utmCampaign ;
    private String utmContent ;
    private String utmTerm ;
    private String utmKey ;
    private String ip ;
    private String code;
    private String channelByEmail;
    private String channelByWechat;
    private String channelBySms;
    private String intention;
    private String workingYear;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCurrentSalaryYear() {
        return currentSalaryYear;
    }

    public void setCurrentSalaryYear(String currentSalaryYear) {
        this.currentSalaryYear = currentSalaryYear;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public String getSchoolOneFunction() {
        return schoolOneFunction;
    }

    public void setSchoolOneFunction(String schoolOneFunction) {
        this.schoolOneFunction = schoolOneFunction;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getChannelByEmail() {
        return channelByEmail;
    }

    public void setChannelByEmail(String channelByEmail) {
        this.channelByEmail = channelByEmail;
    }

    public String getChannelByWechat() {
        return channelByWechat;
    }

    public void setChannelByWechat(String channelByWechat) {
        this.channelByWechat = channelByWechat;
    }

    public String getChannelBySms() {
        return channelBySms;
    }

    public void setChannelBySms(String channelBySms) {
        this.channelBySms = channelBySms;
    }

    public String getIntention() {
        return intention;
    }

    public void setIntention(String intention) {
        this.intention = intention;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }
}
