package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/8.
 */
public class QRCodeCommand {
    Integer sceneId;
    String type = "temp";
    Integer expireSeconds = 604800;


    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(Integer expireSeconds) {
        this.expireSeconds = expireSeconds;
    }
}
