package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/10.
 */
public class PhonePasswordCommand {

    private String phone;

    private String password;

    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
