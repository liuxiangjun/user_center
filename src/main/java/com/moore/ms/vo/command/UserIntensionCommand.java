package com.moore.ms.vo.command;

import java.util.List;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserIntensionCommand {

    private List<UserInfosCommand> userInfos;

    private List<Integer> sendChannel;

    private Integer trigger;

    private String eventCode;

    public List<UserInfosCommand> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfosCommand> userInfos) {
        this.userInfos = userInfos;
    }

    public List<Integer> getSendChannel() {
        return sendChannel;
    }

    public void setSendChannel(List<Integer> sendChannel) {
        this.sendChannel = sendChannel;
    }

    public Integer getTrigger() {
        return trigger;
    }

    public void setTrigger(Integer trigger) {
        this.trigger = trigger;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }
}
