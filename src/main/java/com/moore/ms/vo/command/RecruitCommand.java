package com.moore.ms.vo.command;

/**
 *
 * add by 李登修  2018/9/10
 *
 */
public class RecruitCommand {

    private Long id;

    private String expectCityName;

    private String oneFunction;

    private Long function;

    private Integer rank;

    private float currentSalary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getOneFunction() {
        return oneFunction;
    }

    public void setOneFunction(String oneFunction) {
        this.oneFunction = oneFunction;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public float getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(float currentSalary) {
        this.currentSalary = currentSalary;
    }
}
