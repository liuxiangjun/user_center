package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/9.
 */
public  class UserDoLoginCommand {

    private String username;

    private String password;

    private String openId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
