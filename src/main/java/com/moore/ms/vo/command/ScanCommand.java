package com.moore.ms.vo.command;

/**
 * Created by 汤云飞 on 2016/11/22.
 */
public class ScanCommand {

    private String openId;

    private String code;

    private Long jobId;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
