package com.moore.ms.vo;

import java.util.List;

/**
 * Created by 汤云飞 on 2017/1/13.
 */
public class EngineAnalysisVo {

    private List<Long> companyIds;

    private String rank;

    private List<Long> functionIds;

    private List<String> functionFullPaths;

    public List<Long> getCompanyIds() {
        return companyIds;
    }

    public void setCompanyIds(List<Long> companyIds) {
        this.companyIds = companyIds;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<Long> getFunctionIds() {
        return functionIds;
    }

    public void setFunctionIds(List<Long> functionIds) {
        this.functionIds = functionIds;
    }

    public List<String> getFunctionFullPaths() {
        return functionFullPaths;
    }

    public void setFunctionFullPaths(List<String> functionFullPaths) {
        this.functionFullPaths = functionFullPaths;
    }

    @Override
    public String toString() {
        return "EngineAnalysisVo{" +
                "companyIds=" + companyIds +
                ", rank='" + rank + '\'' +
                ", functionIds=" + functionIds +
                ", functionFullPaths=" + functionFullPaths +
                '}';
    }
}
