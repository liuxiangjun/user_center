package com.moore.ms.vo;

import com.moore.ms.domain.VipBombRecommendHistory;

public class BombTalentUtils {
    private VipBombRecommendHistory vipBombRecommendHistory;


    public VipBombRecommendHistory getVipBombRecommendHistory() {
        return vipBombRecommendHistory;
    }

    public void setVipBombRecommendHistory(VipBombRecommendHistory vipBombRecommendHistory) {
        this.vipBombRecommendHistory = vipBombRecommendHistory;
    }
}
