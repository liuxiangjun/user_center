package com.moore.ms.vo;


import com.moore.ms.common.utils.ConfigEnum;
import com.moore.ms.domain.UserEducation;

import java.util.Date;

public class UserEducationVo {
    private Long userId;
    private String schoolName;
    private String fieldOfStudy;
    private String degree;
    private Integer education;
    private String finishYear;
    private Date createTime;
    private Date modifyTime;
    private Long id;

    public UserEducationVo() {
    }

    public UserEducationVo(UserEducation userEducationtemp) {
        this.id = userEducationtemp.getId();
        this.userId = userEducationtemp.getUserId();
        this.schoolName = userEducationtemp.getSchoolName();
        this.fieldOfStudy = userEducationtemp.getFieldOfStudy();
        this.education = userEducationtemp.getDegree() == null ? 0 : userEducationtemp.getDegree().intValue();
        if(userEducationtemp.getDegree() != null) {
            this.degree = ConfigEnum.FieldOfStudy.getName(userEducationtemp.getDegree());
        }
        this.finishYear = userEducationtemp.getFinishYear();
    }



    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getFinishYear() {
        return finishYear;
    }

    public void setFinishYear(String finishYear) {
        this.finishYear = finishYear;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEducation() {
        return education;
    }

    public void setEducation(Integer education) {
        this.education = education;
    }
}