package com.moore.ms.vo.cityspecial;

/**
 *
 *  add by LiDengXiu 2018/9/13
 *
 */
public class JobResponse {

    private String workingYear;

    private Long jobId;

    private String title;

    private String city_name;

    private String degree_requirement;

    private Float salaryStart;

    private Float salaryLimit;

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDegree_requirement() {
        return degree_requirement;
    }

    public void setDegree_requirement(String degree_requirement) {
        this.degree_requirement = degree_requirement;
    }

    public Float getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Float salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Float getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(Float salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }
}
