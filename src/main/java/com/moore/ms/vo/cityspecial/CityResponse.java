package com.moore.ms.vo.cityspecial;

import java.util.List;

/**
 *
 *  add by LiDengXiu 2018/9/13
 *
 */
public class CityResponse {

    private Long cityId;

    private String cityName;

    private List<CompanyResponse> companyResponse;

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public List<CompanyResponse> getCompanyResponse() {
        return companyResponse;
    }

    public void setCompanyResponse(List<CompanyResponse> companyResponse) {
        this.companyResponse = companyResponse;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
