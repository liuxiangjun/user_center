package com.moore.ms.vo;

/**
 * Created by 汤云飞 on 2016/11/10.
 */
public class Account {

    private Long userId;

    private String openId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }


}
