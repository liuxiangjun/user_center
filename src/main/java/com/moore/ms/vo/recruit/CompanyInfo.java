package com.moore.ms.vo.recruit;

/**
 * Created by 李登修 2018/8/15.
 */
public class CompanyInfo {

    private String logo;

    private String company_fullname;

    private CompanyJob companyJob;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompany_fullname() {
        return company_fullname;
    }

    public void setCompany_fullname(String company_fullname) {
        this.company_fullname = company_fullname;
    }

    public CompanyJob getCompanyJob() {
        return companyJob;
    }

    public void setCompanyJob(CompanyJob companyJob) {
        this.companyJob = companyJob;
    }
}
