package com.moore.ms.vo.recruit;

/**
 * Created by 李登修 2018/8/15.
 */
public class CompanyJob {

    private Long jobId;

    private String title;

    private String city_name;

    private Short degree_requirement;

    private Float salaryStart;

    private Float salaryLimit;

    private Short company_job_status;

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public Short getDegree_requirement() {
        return degree_requirement;
    }

    public void setDegree_requirement(Short degree_requirement) {
        this.degree_requirement = degree_requirement;
    }

    public Float getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Float salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Float getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(Float salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public Short getCompany_job_status() {
        return company_job_status;
    }

    public void setCompany_job_status(Short company_job_status) {
        this.company_job_status = company_job_status;
    }
}
