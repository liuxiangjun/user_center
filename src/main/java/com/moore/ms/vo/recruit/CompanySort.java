package com.moore.ms.vo.recruit;

import java.util.List;

/**
 * Created by 李登修 2018/8/15.
 */
public class CompanySort {

    private Short company_sort;
    private List<CompanyMessage> companyMessage;

    public Short getCompany_sort() {
        return company_sort;
    }

    public void setCompany_sort(Short company_sort) {
        this.company_sort = company_sort;
    }

    public List<CompanyMessage> getCompanyMessage() {
        return companyMessage;
    }

    public void setCompanyMessage(List<CompanyMessage> companyMessage) {
        this.companyMessage = companyMessage;
    }
}
