package com.moore.ms.vo.recruit;

import java.util.List;

/**
 * Created by 李登修 2018/8/15.
 */
public class CompanyMessage {

    private Long companyId;

    private String logo;

    private String company_fullname;

    private String tags;

    private Short industryScale;

    private String description;

    private List<CompanyJob> jobs;

    private Short company_sort;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompany_fullname() {
        return company_fullname;
    }

    public void setCompany_fullname(String company_fullname) {
        this.company_fullname = company_fullname;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Short getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(Short industryScale) {
        this.industryScale = industryScale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CompanyJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<CompanyJob> jobs) {
        this.jobs = jobs;
    }

    public Short getCompany_sort() {
        return company_sort;
    }

    public void setCompany_sort(Short company_sort) {
        this.company_sort = company_sort;
    }
}
