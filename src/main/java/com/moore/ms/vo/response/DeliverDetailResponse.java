package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/18.
 */
public class DeliverDetailResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long deliverId;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer mainStatus;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String createTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyLogo;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryStart;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryLimit;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobSnapshot;

    @JsonSerialize(nullsUsing = NullBooleanSerializer.class)
    private Boolean hasConfirm;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String hasConfirmInterviewTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String hasConfirmContactName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String hasConfirmContactPhone;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;


    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<DeliverHistoryResponse> timeline;


    public Long getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Long deliverId) {
        this.deliverId = deliverId;
    }

    public Integer getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(Integer mainStatus) {
        this.mainStatus = mainStatus;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public Long getJobSnapshot() {
        return jobSnapshot;
    }

    public void setJobSnapshot(Long jobSnapshot) {
        this.jobSnapshot = jobSnapshot;
    }

    public Short getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Short salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Short getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(Short salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public Boolean getHasConfirm() {
        return hasConfirm;
    }

    public void setHasConfirm(Boolean hasConfirm) {
        this.hasConfirm = hasConfirm;
    }

    public String getHasConfirmInterviewTime() {
        return hasConfirmInterviewTime;
    }

    public void setHasConfirmInterviewTime(String hasConfirmInterviewTime) {
        this.hasConfirmInterviewTime = hasConfirmInterviewTime;
    }

    public String getHasConfirmContactName() {
        return hasConfirmContactName;
    }

    public void setHasConfirmContactName(String hasConfirmContactName) {
        this.hasConfirmContactName = hasConfirmContactName;
    }

    public String getHasConfirmContactPhone() {
        return hasConfirmContactPhone;
    }

    public void setHasConfirmContactPhone(String hasConfirmContactPhone) {
        this.hasConfirmContactPhone = hasConfirmContactPhone;
    }

    public List<DeliverHistoryResponse> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<DeliverHistoryResponse> timeline) {
        this.timeline = timeline;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }
}
