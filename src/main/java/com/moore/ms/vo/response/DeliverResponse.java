package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/18.
 */
public class DeliverResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long deliverId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String addTime;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short jobStatus;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryStart;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryLimit;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short hasChangeTime;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;

    public Long getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Long deliverId) {
        this.deliverId = deliverId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Short getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Short jobStatus) {
        this.jobStatus = jobStatus;
    }


    public Short getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Short salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Short getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(Short salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public Short getHasChangeTime() {
        return hasChangeTime;
    }

    public void setHasChangeTime(Short hasChangeTime) {
        this.hasChangeTime = hasChangeTime;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }
}
