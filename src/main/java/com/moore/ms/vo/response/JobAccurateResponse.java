package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class JobAccurateResponse implements Comparable<JobAccurateResponse>{

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyLogo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryStart;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryLimit;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String edu;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String addTime;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTime;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long companyId;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float commission;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float recommendedAward;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer industryChain;
    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short rank;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer hasMillion;

    public JobAccurateResponse(String jobCityName, String normalizeLogo, String companyShortName, String jobTitle, Integer jobSalaryStart, Integer jobSalaryLimit, String workingYear, String edu, String addTime, Long jobId, Double commission, Double recommendedAward, Integer hasMillion) {
        this.cityName = jobCityName;
        this.companyLogo = normalizeLogo;
        this.companyName = companyShortName;
        this.jobTitle = jobTitle;
        this.salaryStart = jobSalaryStart == null ? "0" : jobSalaryStart.toString();
        this.salaryLimit = jobSalaryLimit == null ? "0" : jobSalaryLimit.toString();
        this.workingYear = workingYear;
        this.edu = edu;
        this.addTime = addTime;
        this.jobId = jobId;
        if (commission != null){
            this.commission = Float.valueOf(commission.toString());
        }
        if (recommendedAward != null){
            this.recommendedAward = Float.valueOf(recommendedAward.toString());
        }
        this.hasMillion = hasMillion;
    }

    public JobAccurateResponse() {


    }


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(String salaryStart) {
        this.salaryStart = salaryStart;
    }

    public String getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(String salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public Float getRecommendedAward() {
        return recommendedAward;
    }

    public void setRecommendedAward(Float recommendedAward) {
        this.recommendedAward = recommendedAward;
    }


    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime;
    }

    public Integer getIndustryChain() {
        return industryChain;
    }

    public void setIndustryChain(Integer industryChain) {
        this.industryChain = industryChain;
    }

    public Short getRank() {
        return rank;
    }

    public void setRank(Short rank) {
        this.rank = rank;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }

    public Integer getHasMillion() {
        return hasMillion;
    }

    public void setHasMillion(Integer hasMillion) {
        this.hasMillion = hasMillion;
    }

    @Override
    public int compareTo(JobAccurateResponse o) {
        if (this.rank.compareTo(o.getRank()) == 0){
            return Integer.valueOf(o.getSalaryLimit()).compareTo(Integer.valueOf(this.salaryLimit));
        }else{
            return o.getRank().compareTo(this.rank);
        }

    }
}
