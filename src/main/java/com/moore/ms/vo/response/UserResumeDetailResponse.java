package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullListSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/22.
 */
public class UserResumeDetailResponse {

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String avatar;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fullname;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short sex;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobilePhone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String contactEmail;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short education;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCompanyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentJobTitle;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short englishListenLevel;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short englishReadLevel;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isDeliverable;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isBaseComplete;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fileName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String userAvatar;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cropping;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String uploadedResume;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<UserCompany> userCompanies;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<UserEducation> userEducations;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishSchoolYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private List<UserProject> userProjects;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Short birthdayYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String researchDirection;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private List<UserJobIntension> userJobIntension;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Short isResumePublic;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Short currentJobStatus;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private String blockdomain;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Short groupType;


    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public Short getIsResumePublic() {
        return isResumePublic;
    }

    public void setIsResumePublic(Short isResumePublic) {
        this.isResumePublic = isResumePublic;
    }

    public Short getCurrentJobStatus() {
        return currentJobStatus;
    }

    public void setCurrentJobStatus(Short currentJobStatus) {
        this.currentJobStatus = currentJobStatus;
    }

    public String getBlockdomain() {
        return blockdomain;
    }

    public void setBlockdomain(String blockdomain) {
        this.blockdomain = blockdomain;
    }

    public List<UserJobIntension> getUserJobIntension() {
        return userJobIntension;
    }

    public void setUserJobIntension(List<UserJobIntension> userJobIntension) {
        this.userJobIntension = userJobIntension;
    }

    public Short getBirthdayYear() {
        return birthdayYear;
    }

    public void setBirthdayYear(Short birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    public List<UserProject> getUserProjects() {
        return userProjects;
    }

    public void setUserProjects(List<UserProject> userProjects) {
        this.userProjects = userProjects;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Short getEnglishListenLevel() {
        return englishListenLevel;
    }

    public void setEnglishListenLevel(Short englishListenLevel) {
        this.englishListenLevel = englishListenLevel;
    }

    public Short getEnglishReadLevel() {
        return englishReadLevel;
    }

    public void setEnglishReadLevel(Short englishReadLevel) {
        this.englishReadLevel = englishReadLevel;
    }

    public Short getIsDeliverable() {
        return isDeliverable;
    }

    public void setIsDeliverable(Short isDeliverable) {
        this.isDeliverable = isDeliverable;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUploadedResume() {
        return uploadedResume;
    }

    public void setUploadedResume(String uploadedResume) {
        this.uploadedResume = uploadedResume;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getCropping() {
        return cropping;
    }

    public void setCropping(String cropping) {
        this.cropping = cropping;
    }

    public List<UserCompany> getUserCompanies() {
        return userCompanies;
    }

    public void setUserCompanies(List<UserCompany> userCompanies) {
        this.userCompanies = userCompanies;
    }

    public List<UserEducation> getUserEducations() {
        return userEducations;
    }

    public void setUserEducations(List<UserEducation> userEducations) {
        this.userEducations = userEducations;
    }

    public Short getIsBaseComplete() {
        return isBaseComplete;
    }

    public void setIsBaseComplete(Short isBaseComplete) {
        this.isBaseComplete = isBaseComplete;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public static class UserCompany{

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private Long id;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String comapnyName;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String jobTitle;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String joinTime;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String leaveTime;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String jobContent;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getComapnyName() {
            return comapnyName;
        }

        public void setComapnyName(String comapnyName) {
            this.comapnyName = comapnyName;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getJoinTime() {
            return joinTime;
        }

        public void setJoinTime(String joinTime) {
            this.joinTime = joinTime;
        }

        public String getLeaveTime() {
            return leaveTime;
        }

        public void setLeaveTime(String leaveTime) {
            this.leaveTime = leaveTime;
        }

        public String getJobContent() {
            return jobContent;
        }

        public void setJobContent(String jobContent) {
            this.jobContent = jobContent;
        }
    }

        public static class UserJobIntension{

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private Long id;

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private String expectCityName;

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private float currentSalary;

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private Integer rank;

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private String function;

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private String oneFunction;

            public Long getId() {
                return id;
            }

            public void setId(Long id) {
                this.id = id;
            }

            public String getExpectCityName() {
                return expectCityName;
            }

            public void setExpectCityName(String expectCityName) {
                this.expectCityName = expectCityName;
            }

            public float getCurrentSalary() {
                return currentSalary;
            }

            public void setCurrentSalary(float currentSalary) {
                this.currentSalary = currentSalary;
            }

            public Integer getRank() {
                return rank;
            }

            public void setRank(Integer rank) {
                this.rank = rank;
            }

            public String getFunction() {
                return function;
            }

            public void setFunction(String function) {
                this.function = function;
            }

            public String getOneFunction() {
                return oneFunction;
            }

            public void setOneFunction(String oneFunction) {
                this.oneFunction = oneFunction;
            }
        }

    public static class UserEducation{

        @JsonSerialize(nullsUsing = NullLongSerializer.class)
        private Long id;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String schoolName;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String fieldOfStudy;

        @JsonSerialize(nullsUsing = NullShortSerializer.class)
        private Short degree;

        @JsonSerialize(nullsUsing = NullStringSerializer.class)
        private String finishYear;


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getFieldOfStudy() {
            return fieldOfStudy;
        }

        public void setFieldOfStudy(String fieldOfStudy) {
            this.fieldOfStudy = fieldOfStudy;
        }

        public Short getDegree() {
            return degree;
        }

        public void setDegree(Short degree) {
            this.degree = degree;
        }

        public String getFinishYear() {
            return finishYear;
        }

        public void setFinishYear(String finishYear) {
            this.finishYear = finishYear;
        }


    }

    public static class UserProject{

        private Long id;

        private String title;

        private String jobTitle;

        private String startTime;

        private String finishTime;

        private String description;

        private String link;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

}
