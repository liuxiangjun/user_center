package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserPublishResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String title;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String publications;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String publishTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jointlyPeople;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String description;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublications() {
        return publications;
    }

    public void setPublications(String publications) {
        this.publications = publications;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getJointlyPeople() {
        return jointlyPeople;
    }

    public void setJointlyPeople(String jointlyPeople) {
        this.jointlyPeople = jointlyPeople;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
