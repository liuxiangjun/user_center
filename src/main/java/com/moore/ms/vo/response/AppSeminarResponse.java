package com.moore.ms.vo.response;

/**
 * Created by 汤云飞 on 2017/9/9.
 */
public class AppSeminarResponse {

    private Long id;

    private String title;

    private String name;

    private String img;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
