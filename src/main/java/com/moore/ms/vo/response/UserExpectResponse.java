package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserExpectResponse {

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectJobTitle;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short expectJobStatus;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectZipcode;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short expectJobSalary;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectJobDescription;

    public String getExpectJobTitle() {
        return expectJobTitle;
    }

    public void setExpectJobTitle(String expectJobTitle) {
        this.expectJobTitle = expectJobTitle;
    }

    public Short getExpectJobStatus() {
        return expectJobStatus;
    }

    public void setExpectJobStatus(Short expectJobStatus) {
        this.expectJobStatus = expectJobStatus;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Short getExpectJobSalary() {
        return expectJobSalary;
    }

    public void setExpectJobSalary(Short expectJobSalary) {
        this.expectJobSalary = expectJobSalary;
    }

    public String getExpectJobDescription() {
        return expectJobDescription;
    }

    public void setExpectJobDescription(String expectJobDescription) {
        this.expectJobDescription = expectJobDescription;
    }
}
