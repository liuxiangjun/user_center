package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/22.
 */
public class ResumeBaseInfoResponse {

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fullname;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short sex;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobilePhone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String contactEmail;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short education;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentZipCode;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCompanyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentJobTitle;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short englishListenLevel;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short englishReadLevel;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishSchoolYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short groupType;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short birthdayYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short birthdayMonth;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private String researchDirection;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public String getCurrentZipCode() {
        return currentZipCode;
    }

    public void setCurrentZipCode(String currentZipCode) {
        this.currentZipCode = currentZipCode;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Short getEnglishListenLevel() {
        return englishListenLevel;
    }

    public void setEnglishListenLevel(Short englishListenLevel) {
        this.englishListenLevel = englishListenLevel;
    }

    public Short getEnglishReadLevel() {
        return englishReadLevel;
    }

    public void setEnglishReadLevel(Short englishReadLevel) {
        this.englishReadLevel = englishReadLevel;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public Short getBirthdayYear() {
        return birthdayYear;
    }

    public void setBirthdayYear(Short birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

    public Short getBirthdayMonth() {
        return birthdayMonth;
    }

    public void setBirthdayMonth(Short birthdayMonth) {
        this.birthdayMonth = birthdayMonth;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }
}
