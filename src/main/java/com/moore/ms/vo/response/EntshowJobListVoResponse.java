package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class EntshowJobListVoResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String title;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String city;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String degree;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryStart;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryEnd;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String functionIds;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String commonProTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Short getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Short salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Short getSalaryEnd() {
        return salaryEnd;
    }

    public void setSalaryEnd(Short salaryEnd) {
        this.salaryEnd = salaryEnd;
    }

    public String getFunctionIds() {
        return functionIds;
    }

    public void setFunctionIds(String functionIds) {
        this.functionIds = functionIds;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }

    public String getCommonProTime() {
        return commonProTime;
    }

    public void setCommonProTime(String commonProTime) {
        this.commonProTime = commonProTime;
    }
}
