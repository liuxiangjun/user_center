package com.moore.ms.vo.response;

/**
 * @author 刘明新
 * @date 2017/12/8 下午7:40
 */
public class RewardDetailResponse {

    private Long employerCompanyId;
    private Integer num;
    private String logo;
    private String companyShortName;
    private Long average;

    public Long getEmployerCompanyId() {
        return employerCompanyId;
    }

    public void setEmployerCompanyId(Long employerCompanyId) {
        this.employerCompanyId = employerCompanyId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }
}
