package com.moore.ms.vo.response;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/29.
 */
public class PromotionResponse {

    private Long id;

    private String title;

    private String text;

    private String pic;

    private String link;

    private String shareLink;

    private Integer SceneId;

    private String inviteCode;

    private List<WellJobTypeResponse> types;

    private String ticket;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Integer getSceneId() {
        return SceneId;
    }

    public void setSceneId(Integer sceneId) {
        SceneId = sceneId;
    }

    public List<WellJobTypeResponse> getTypes() {
        return types;
    }

    public void setTypes(List<WellJobTypeResponse> types) {
        this.types = types;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }
}
