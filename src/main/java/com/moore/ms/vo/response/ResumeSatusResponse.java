package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;

public class ResumeSatusResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Short isResumePublic;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Short currentJobStatus;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private String blockdomain;

    public Short getIsResumePublic() {
        return isResumePublic;
    }

    public void setIsResumePublic(Short isResumePublic) {
        this.isResumePublic = isResumePublic;
    }

    public Short getCurrentJobStatus() {
        return currentJobStatus;
    }

    public void setCurrentJobStatus(Short currentJobStatus) {
        this.currentJobStatus = currentJobStatus;
    }

    public String getBlockdomain() {
        return blockdomain;
    }

    public void setBlockdomain(String blockdomain) {
        this.blockdomain = blockdomain;
    }
}
