package com.moore.ms.vo.response;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class WxUserHasIntentionResponse {

    private Long id;

    private String mobile;

    private String openId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
