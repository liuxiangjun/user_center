package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullFloatSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/19.
 */
public class InviteResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String addTime;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryStart;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short salaryLimit;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float commission;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float recommendedAward;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityName;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long companyId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Short getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(Short salaryStart) {
        this.salaryStart = salaryStart;
    }

    public Short getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(Short salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public Float getRecommendedAward() {
        return recommendedAward;
    }

    public void setRecommendedAward(Float recommendedAward) {
        this.recommendedAward = recommendedAward;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }
}
