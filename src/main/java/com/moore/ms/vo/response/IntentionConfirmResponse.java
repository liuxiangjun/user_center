package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/19.
 */
public class IntentionConfirmResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobId;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobSnapshotId;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long companyId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyShortname;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String userName;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short communicationStatus;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String importId;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long entityId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String email;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobile;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String lastFeedBackContent;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long engineUserId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String engineUserIdHash;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getJobSnapshotId() {
        return jobSnapshotId;
    }

    public void setJobSnapshotId(Long jobSnapshotId) {
        this.jobSnapshotId = jobSnapshotId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyShortname() {
        return companyShortname;
    }

    public void setCompanyShortname(String companyShortname) {
        this.companyShortname = companyShortname;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Short getCommunicationStatus() {
        return communicationStatus;
    }

    public void setCommunicationStatus(Short communicationStatus) {
        this.communicationStatus = communicationStatus;
    }

    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLastFeedBackContent() {
        return lastFeedBackContent;
    }

    public void setLastFeedBackContent(String lastFeedBackContent) {
        this.lastFeedBackContent = lastFeedBackContent;
    }

    public Long getEngineUserId() {
        return engineUserId;
    }

    public void setEngineUserId(Long engineUserId) {
        this.engineUserId = engineUserId;
    }


    public String getEngineUserIdHash() {
        return engineUserIdHash;
    }

    public void setEngineUserIdHash(String engineUserIdHash) {
        this.engineUserIdHash = engineUserIdHash;
    }
}
