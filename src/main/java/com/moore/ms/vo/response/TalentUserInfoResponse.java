package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;
import com.moore.ms.domain.UserCompany;
import com.moore.ms.domain.UserEducation;

/**
 * Created by 汤云飞 on 2016/11/22.
 */
public class TalentUserInfoResponse {

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fullname;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobilePhone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String email;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String engineFullname;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String engineMobilePhone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String engineEmail;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short education;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentZipCode;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishSchoolYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short groupType;

    private UserEducation educationExp;

    private UserCompany workExp;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public String getCurrentZipCode() {
        return currentZipCode;
    }

    public void setCurrentZipCode(String currentZipCode) {
        this.currentZipCode = currentZipCode;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public UserEducation getEducationExp() {
        return educationExp;
    }

    public void setEducationExp(UserEducation educationExp) {
        this.educationExp = educationExp;
    }

    public UserCompany getWorkExp() {
        return workExp;
    }

    public void setWorkExp(UserCompany workExp) {
        this.workExp = workExp;
    }

    public String getEngineFullname() {
        return engineFullname;
    }

    public void setEngineFullname(String engineFullname) {
        this.engineFullname = engineFullname;
    }

    public String getEngineMobilePhone() {
        return engineMobilePhone;
    }

    public void setEngineMobilePhone(String engineMobilePhone) {
        this.engineMobilePhone = engineMobilePhone;
    }

    public String getEngineEmail() {
        return engineEmail;
    }

    public void setEngineEmail(String engineEmail) {
        this.engineEmail = engineEmail;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }
}
