package com.moore.ms.vo.response;

import java.util.List;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserIntensionResponse {

    private Long userId;

    private String airId;

    private List<Long> jobIds;

    private Long jobCount;

    private Long matchTypeMost;

    private Long matchTypePart;

    private Long matchTypeOther;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAirId() {
        return airId;
    }

    public void setAirId(String airId) {
        this.airId = airId;
    }

    public List<Long> getJobIds() {
        return jobIds;
    }

    public void setJobIds(List<Long> jobIds) {
        this.jobIds = jobIds;
    }

    public Long getJobCount() {
        return jobCount;
    }

    public void setJobCount(Long jobCount) {
        this.jobCount = jobCount;
    }

    public Long getMatchTypeMost() {
        return matchTypeMost;
    }

    public void setMatchTypeMost(Long matchTypeMost) {
        this.matchTypeMost = matchTypeMost;
    }

    public Long getMatchTypePart() {
        return matchTypePart;
    }

    public void setMatchTypePart(Long matchTypePart) {
        this.matchTypePart = matchTypePart;
    }

    public Long getMatchTypeOther() {
        return matchTypeOther;
    }

    public void setMatchTypeOther(Long matchTypeOther) {
        this.matchTypeOther = matchTypeOther;
    }
}
