package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;

/**
 * Created by 汤云飞 on 2016/11/17.
 */
public class JobListResponse {
    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;
    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long companyId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String title;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyLogo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String workingYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String degreeRequirement;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String createTime;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float commission;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float recommendedAward;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long jobSnapShotId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryStartYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryEndYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }

    public String getDegreeRequirement() {
        return degreeRequirement;
    }

    public void setDegreeRequirement(String degreeRequirement) {
        this.degreeRequirement = degreeRequirement;
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public Float getRecommendedAward() {
        return recommendedAward;
    }

    public void setRecommendedAward(Float recommendedAward) {
        this.recommendedAward = recommendedAward;
    }

    public Long getJobSnapShotId() {
        return jobSnapShotId;
    }

    public void setJobSnapShotId(Long jobSnapShotId) {
        this.jobSnapShotId = jobSnapShotId;
    }

    public String getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(String salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public String getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(String salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }
}
