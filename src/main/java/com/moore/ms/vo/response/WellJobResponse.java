package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullIntegerSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/29.
 */
public class WellJobResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;
    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long companyId;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityName;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryStart;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String salaryLimit;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String workingYear;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String edu;
    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer appJob;
    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer appCompany;
    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryStartYear;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Float salaryEndYear;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getSalaryStart() {
        return salaryStart;
    }

    public void setSalaryStart(String salaryStart) {
        this.salaryStart = salaryStart;
    }

    public String getSalaryLimit() {
        return salaryLimit;
    }

    public void setSalaryLimit(String salaryLimit) {
        this.salaryLimit = salaryLimit;
    }

    public String getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(String workingYear) {
        this.workingYear = workingYear;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public Integer getAppJob() {
        return appJob;
    }

    public void setAppJob(Integer appJob) {
        this.appJob = appJob;
    }

    public Integer getAppCompany() {
        return appCompany;
    }

    public void setAppCompany(Integer appCompany) {
        this.appCompany = appCompany;
    }

    public Float getSalaryStartYear() {
        return salaryStartYear;
    }

    public void setSalaryStartYear(Float salaryStartYear) {
        this.salaryStartYear = salaryStartYear;
    }

    public Float getSalaryEndYear() {
        return salaryEndYear;
    }

    public void setSalaryEndYear(Float salaryEndYear) {
        this.salaryEndYear = salaryEndYear;
    }
}
