package com.moore.ms.vo.response;

public class MTMessageResponse {

    private String utmMedium;
    private String utmSource;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;
    private Boolean isActivity;//是否是运营活动
    private String contentDesc;//内容描述
    private String entityId;//实体标识
    private String sessionId;//会话标识
    private String actionDesc;//动作描述
    private String additionalAction;//辅助动作
    private String productContent;//产生的内容(json字符串)
    private String engineIndustryChainIdMany;//产业链
    private String engineFunctionIdMany;//职能
    private String rank;//职级
    private String type;//活动类型
    private String key;//关键词
    private String tag;//标签
    private String ipAddr;//IP地址
    private String device;//设备
    private String url;//url或描述信息

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public Boolean getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(Boolean activity) {
        isActivity = activity;
    }

    public String getContentDesc() {
        return contentDesc;
    }

    public void setContentDesc(String contentDesc) {
        this.contentDesc = contentDesc;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getActionDesc() {
        return actionDesc;
    }

    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }

    public String getAdditionalAction() {
        return additionalAction;
    }

    public void setAdditionalAction(String additionalAction) {
        this.additionalAction = additionalAction;
    }

    public String getProductContent() {
        return productContent;
    }

    public void setProductContent(String productContent) {
        this.productContent = productContent;
    }

    public String getEngineIndustryChainIdMany() {
        return engineIndustryChainIdMany;
    }

    public void setEngineIndustryChainIdMany(String engineIndustryChainIdMany) {
        this.engineIndustryChainIdMany = engineIndustryChainIdMany;
    }

    public String getEngineFunctionIdMany() {
        return engineFunctionIdMany;
    }

    public void setEngineFunctionIdMany(String engineFunctionIdMany) {
        this.engineFunctionIdMany = engineFunctionIdMany;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
