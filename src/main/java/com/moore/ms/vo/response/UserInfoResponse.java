package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.StringUtils;
import com.moore.ms.common.jackson.*;
import com.moore.ms.domain.User;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class UserInfoResponse {



    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String openId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fullname;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String schoolName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fieldOfStudy;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short education;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishSchoolYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobile;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String email;
    @JsonSerialize(nullsUsing = NullBooleanSerializer.class)
    private Boolean joined;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long schoolOneFunction;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String schoolOneFunctionName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String researchDirection;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float currentSalaryYear;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectZipcode;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isCompleteIntension;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String functionName;

    public UserInfoResponse(User user,String openId) {
        this.openId = openId;
        this.fullname = user.getFullname();
        this.schoolName = user.getSchoolName();
        this.fieldOfStudy = user.getFieldOfStudy();
        this.education = user.getEducation();
        this.finishSchoolYear = user.getFinishSchoolYear();
        this.mobile = user.getMobilePhone();
        this.email = user.getContactEmail();
        this.researchDirection = user.getResearchDirection();
    }

    public UserInfoResponse(User user) {
        this.fullname = user.getFullname();
        this.schoolName = user.getSchoolName();
        this.fieldOfStudy = user.getFieldOfStudy();
        this.education = user.getEducation();
        this.finishSchoolYear = user.getFinishSchoolYear();
        this.mobile = StringUtils.isBlank(user.getMobilePhone()) ? user.getRegisterMobile() : user.getMobilePhone();
        this.email = user.getContactEmail();
        this.researchDirection = user.getResearchDirection();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getJoined() {
        return joined;
    }

    public void setJoined(Boolean joined) {
        this.joined = joined;
    }

    public Long getSchoolOneFunction() {
        return schoolOneFunction;
    }

    public void setSchoolOneFunction(Long schoolOneFunction) {
        this.schoolOneFunction = schoolOneFunction;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    public String getSchoolOneFunctionName() {
        return schoolOneFunctionName;
    }

    public void setSchoolOneFunctionName(String schoolOneFunctionName) {
        this.schoolOneFunctionName = schoolOneFunctionName;
    }

    public Float getCurrentSalaryYear() {
        return currentSalaryYear;
    }

    public void setCurrentSalaryYear(Float currentSalaryYear) {
        this.currentSalaryYear = currentSalaryYear;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Short getIsCompleteIntension() {
        return isCompleteIntension;
    }

    public void setIsCompleteIntension(Short isCompleteIntension) {
        this.isCompleteIntension = isCompleteIntension;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
}
