package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class CompanyDetailResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyFullName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String shortDescription;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String website;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<String> tags;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryChainId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryChainName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryScale;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String cityNames;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String description;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String shareTitle;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String shareDesc;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String sharePic;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String shareUrl;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String inviteCode;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer jobNums;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer collected;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String resumeProcessingRate;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String resumeProcessing;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short mediaType;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mediaVideoOrigin;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mediaImg;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private String fileUrlPrefix;
    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private String imgUrlPrefix;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<String> culturesImages;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isChecked;


    public String getIndustryChainId() {
        return industryChainId;
    }

    public void setIndustryChainId(String industryChainId) {
        this.industryChainId = industryChainId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getIndustryChainName() {
        return industryChainName;
    }

    public void setIndustryChainName(String industryChainName) {
        this.industryChainName = industryChainName;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(String industryScale) {
        this.industryScale = industryScale;
    }

    public String getCityNames() {
        return cityNames;
    }

    public void setCityNames(String cityNames) {
        this.cityNames = cityNames;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDesc() {
        return shareDesc;
    }

    public void setShareDesc(String shareDesc) {
        this.shareDesc = shareDesc;
    }

    public String getSharePic() {
        return sharePic;
    }

    public void setSharePic(String sharePic) {
        this.sharePic = sharePic;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public Integer getJobNums() {
        return jobNums;
    }

    public void setJobNums(Integer jobNums) {
        this.jobNums = jobNums;
    }

    public Integer getCollected() {
        return collected;
    }

    public void setCollected(Integer collected) {
        this.collected = collected;
    }

    public String getResumeProcessingRate() {
        return resumeProcessingRate;
    }

    public void setResumeProcessingRate(String resumeProcessingRate) {
        this.resumeProcessingRate = resumeProcessingRate;
    }

    public String getResumeProcessing() {
        return resumeProcessing;
    }

    public void setResumeProcessing(String resumeProcessing) {
        this.resumeProcessing = resumeProcessing;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Short getMediaType() {
        return mediaType;
    }

    public void setMediaType(Short mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaVideoOrigin() {
        return mediaVideoOrigin;
    }

    public void setMediaVideoOrigin(String mediaVideoOrigin) {
        this.mediaVideoOrigin = mediaVideoOrigin;
    }

    public String getMediaImg() {
        return mediaImg;
    }

    public void setMediaImg(String mediaImg) {
        this.mediaImg = mediaImg;
    }

    public String getFileUrlPrefix() {
        return fileUrlPrefix;
    }

    public void setFileUrlPrefix(String fileUrlPrefix) {
        this.fileUrlPrefix = fileUrlPrefix;
    }

    public String getImgUrlPrefix() {
        return imgUrlPrefix;
    }

    public void setImgUrlPrefix(String imgUrlPrefix) {
        this.imgUrlPrefix = imgUrlPrefix;
    }

    public List<String> getCulturesImages() {
        return culturesImages;
    }

    public void setCulturesImages(List<String> culturesImages) {
        this.culturesImages = culturesImages;
    }

    public Short getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Short isChecked) {
        this.isChecked = isChecked;
    }
}
