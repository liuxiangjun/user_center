package com.moore.ms.vo.response;

/**
 *
 *  add by 李登修
 *
 */
public class SchoolRecruitResponse {

    private Long activityId;

    private Short type;

    private String preImg;

    private String avatar;

    private String hrFullname;

    private String title;

    private Integer sequence;

    private String hrTitle;

    public String getHrTitle() {
        return hrTitle;
    }

    public void setHrTitle(String hrTitle) {
        this.hrTitle = hrTitle;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getPreImg() {
        return preImg;
    }

    public void setPreImg(String preImg) {
        this.preImg = preImg;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getHrFullname() {
        return hrFullname;
    }

    public void setHrFullname(String hrFullname) {
        this.hrFullname = hrFullname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
