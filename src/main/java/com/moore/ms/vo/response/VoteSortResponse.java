package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * @author 刘明新
 * @date 2017/12/8 下午4:46
 * 用于最佳雇主活动的投票活动
 */
public class VoteSortResponse implements Comparable<JobAccurateResponse>{
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Long employerCompanyId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private short VoteType;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Integer num;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyShortName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Integer average;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private short rewardId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Long sum;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private Integer answerNum;

    public Long getEmployerCompanyId() {
        return employerCompanyId;
    }

    public void setEmployerCompanyId(Long employerCompanyId) {
        this.employerCompanyId = employerCompanyId;
    }

    public short getVoteType() {
        return VoteType;
    }

    public void setVoteType(short voteType) {
        VoteType = voteType;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getAverage() {
        return average;
    }

    public void setAverage(Integer average) {
        this.average = average;
    }

    public short getRewardId() {
        return rewardId;
    }

    public void setRewardId(short rewardId) {
        this.rewardId = rewardId;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public Integer getAnswerNum() {
        return answerNum;
    }

    public void setAnswerNum(Integer answerNum) {
        this.answerNum = answerNum;
    }

    @Override
    public int compareTo(JobAccurateResponse o) {
        return 0;
    }

    @Override
    public String toString() {
        return "VoteSortResponse{" +
                "id=" + id +
                ", employerCompanyId=" + employerCompanyId +
                ", VoteType=" + VoteType +
                ", num=" + num +
                ", companyShortName='" + companyShortName + '\'' +
                ", logo='" + logo + '\'' +
                ", average=" + average +
                '}';
    }
}
