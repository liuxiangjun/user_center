package com.moore.ms.vo.response;



/**
 * Created by 李登修 on 2018/8/28
 */
public class DSPResponse {

    private Short isFinish;

    private Short groupType;

    private String CompanyName;

    private String jobTitle;

    private String finshSchoolYear;

    private String phone;

    public Short getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Short isFinish) {
        this.isFinish = isFinish;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getFinshSchoolYear() {
        return finshSchoolYear;
    }

    public void setFinshSchoolYear(String finshSchoolYear) {
        this.finshSchoolYear = finshSchoolYear;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

