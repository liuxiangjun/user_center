package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/18.
 */
public class DeliverHistoryResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long deliverId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String interviewTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String modifyTime;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short status;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String contactContact;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String contactPhone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String contactEmail;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String interviewPlace;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String interviewContact;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String interviewContactPhone;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short interviewWay;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String interviewContent;

    public Long getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Long deliverId) {
        this.deliverId = deliverId;
    }

    public String getInterviewTime() {
        return interviewTime;
    }

    public void setInterviewTime(String interviewTime) {
        this.interviewTime = interviewTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getContactContact() {
        return contactContact;
    }

    public void setContactContact(String contactContact) {
        this.contactContact = contactContact;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getInterviewPlace() {
        return interviewPlace;
    }

    public void setInterviewPlace(String interviewPlace) {
        this.interviewPlace = interviewPlace;
    }

    public String getInterviewContact() {
        return interviewContact;
    }

    public void setInterviewContact(String interviewContact) {
        this.interviewContact = interviewContact;
    }

    public String getInterviewContactPhone() {
        return interviewContactPhone;
    }

    public void setInterviewContactPhone(String interviewContactPhone) {
        this.interviewContactPhone = interviewContactPhone;
    }

    public Short getInterviewWay() {
        return interviewWay;
    }

    public void setInterviewWay(Short interviewWay) {
        this.interviewWay = interviewWay;
    }

    public String getInterviewContent() {
        return interviewContent;
    }

    public void setInterviewContent(String interviewContent) {
        this.interviewContent = interviewContent;
    }
}
