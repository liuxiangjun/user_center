package com.moore.ms.vo.response;

public class DSPresponseTwo {

        private String groupType;
        private String jobTitle;
        private String finshSchoolYear;
        private String phone;
        private String companyName;
        private String code;
        private String utmKey;
        private String utmSource;
        private String utmMedium;
        private String utmCampaign;
        private String utmContent;
        private String utmTerm;
        private String openId;

        public String getGroupType() {
            return groupType;
        }

        public void setGroupType(String groupType) {
            this.groupType = groupType;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getFinshSchoolYear() {
            return finshSchoolYear;
        }

        public void setFinshSchoolYear(String finshSchoolYear) {
            this.finshSchoolYear = finshSchoolYear;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getUtmKey() {
            return utmKey;
        }

        public void setUtmKey(String utmKey) {
            this.utmKey = utmKey;
        }

        public String getUtmSource() {
            return utmSource;
        }

        public void setUtmSource(String utmSource) {
            this.utmSource = utmSource;
        }

        public String getUtmMedium() {
            return utmMedium;
        }

        public void setUtmMedium(String utmMedium) {
            this.utmMedium = utmMedium;
        }

        public String getUtmCampaign() {
            return utmCampaign;
        }

        public void setUtmCampaign(String utmCampaign) {
            this.utmCampaign = utmCampaign;
        }

        public String getUtmContent() {
            return utmContent;
        }

        public void setUtmContent(String utmContent) {
            this.utmContent = utmContent;
        }

        public String getUtmTerm() {
            return utmTerm;
        }

        public void setUtmTerm(String utmTerm) {
            this.utmTerm = utmTerm;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }
}
