package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;

/**
 * add by 李登修
 */
public class UserJobIntensionResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private String expectCityName;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long oneFunction;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long function;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Integer rank;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private float currentSalary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public Long getOneFunction() {
        return oneFunction;
    }

    public void setOneFunction(Long oneFunction) {
        this.oneFunction = oneFunction;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public float getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(float currentSalary) {
        this.currentSalary = currentSalary;
    }
}
