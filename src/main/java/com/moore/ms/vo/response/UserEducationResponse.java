package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserEducationResponse {
    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String schoolName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fieldOfStudy;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short degree;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String finishYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getDegree() {
        return degree;
    }

    public void setDegree(Short degree) {
        this.degree = degree;
    }

    public String getFinishYear() {
        return finishYear;
    }

    public void setFinishYear(String finishYear) {
        this.finishYear = finishYear;
    }
}
