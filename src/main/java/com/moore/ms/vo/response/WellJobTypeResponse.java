package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullListSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/29.
 */
public class WellJobTypeResponse {
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String name;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<WellJobResponse> jobs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WellJobResponse> getJobs() {
        return jobs;
    }

    public void setJobs(List<WellJobResponse> jobs) {
        this.jobs = jobs;
    }
}
