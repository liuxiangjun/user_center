package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullListSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/29.
 */
public class HighSalaryResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String qiniuServerForImage;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mooreServerForWx;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String title;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String info;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String thumbnail;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String headImg;
    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String tailImg;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<WellJobTypeResponse> jobs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQiniuServerForImage() {
        return qiniuServerForImage;
    }

    public void setQiniuServerForImage(String qiniuServerForImage) {
        this.qiniuServerForImage = qiniuServerForImage;
    }

    public String getMooreServerForWx() {
        return mooreServerForWx;
    }

    public void setMooreServerForWx(String mooreServerForWx) {
        this.mooreServerForWx = mooreServerForWx;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getTailImg() {
        return tailImg;
    }

    public void setTailImg(String tailImg) {
        this.tailImg = tailImg;
    }

    public List<WellJobTypeResponse> getJobs() {
        return jobs;
    }

    public void setJobs(List<WellJobTypeResponse> jobs) {
        this.jobs = jobs;
    }
}
