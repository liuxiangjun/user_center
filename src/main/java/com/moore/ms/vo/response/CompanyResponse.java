package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullIntegerSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2017/6/29.
 */
public class CompanyResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyIndustryChainName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer jobNum;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryScale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyIndustryChainName() {
        return companyIndustryChainName;
    }

    public void setCompanyIndustryChainName(String companyIndustryChainName) {
        this.companyIndustryChainName = companyIndustryChainName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getJobNum() {
        return jobNum;
    }

    public void setJobNum(Integer jobNum) {
        this.jobNum = jobNum;
    }

    public String getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(String industryScale) {
        this.industryScale = industryScale;
    }
}
