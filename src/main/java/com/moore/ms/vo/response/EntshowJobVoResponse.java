package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class EntshowJobVoResponse {


    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<EntshowJobListVoResponse> jobs;

    @JsonSerialize(nullsUsing = NullListSerializer.class)
    private List<Map<String,Object>> functions;

    @JsonSerialize(nullsUsing = NullSetSerializer.class)
    private Set<String> cities;


    public List<EntshowJobListVoResponse> getJobs() {
        return jobs;
    }

    public void setJobs(List<EntshowJobListVoResponse> jobs) {
        this.jobs = jobs;
    }

    public Set<String> getCities() {
        return cities;
    }

    public void setCities(Set<String> cities) {
        this.cities = cities;
    }

    public List<Map<String, Object>> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Map<String, Object>> functions) {
        this.functions = functions;
    }
}
