package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/23.
 */
public class UserPublicationResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String title;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String publishTime;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String link;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String authors;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String description;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short publishStatus;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String patentNo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String patentOfficeLocation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Short publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getPatentNo() {
        return patentNo;
    }

    public void setPatentNo(String patentNo) {
        this.patentNo = patentNo;
    }

    public String getPatentOfficeLocation() {
        return patentOfficeLocation;
    }

    public void setPatentOfficeLocation(String patentOfficeLocation) {
        this.patentOfficeLocation = patentOfficeLocation;
    }
}
