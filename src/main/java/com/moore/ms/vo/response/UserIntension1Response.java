package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;
import com.moore.ms.common.jackson.NullBooleanSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullShortSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;
import com.moore.ms.domain.User;
import com.moore.ms.domain.UserJobIntension;

import java.util.List;

/**
 * Created by 汤云飞 on 2017/1/18.
 */
public class UserIntension1Response {

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String openId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String fullname;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectZipcode;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long functionId;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short rank;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String mobile;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String email;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String functionName;

    @JsonSerialize(nullsUsing = NullBooleanSerializer.class)
    private Boolean Joined;

    public UserIntension1Response(User user, String openId, List<UserJobIntension> intensions) {
        this.openId = openId;
        this.fullname = user.getFullname();
        this.companyName = user.getCurrentCompanyName();
        if (!CollectionUtils.isEmpty(intensions)) {
            UserJobIntension intension = intensions.get(0);
            this.expectCityName = intension.getExpectCityName();
            this.expectZipcode = intension.getExpectZipcode();
            this.functionId = intension.getFunction();
            if (intension.getRank() != null){
                this.rank = intension.getRank().shortValue();
            }
        }
        this.mobile = user.getMobilePhone();
        this.email = user.getContactEmail();



    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Short getRank() {
        return rank;
    }

    public void setRank(Short rank) {
        this.rank = rank;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Boolean getJoined() {
        return Joined;
    }

    public void setJoined(Boolean joined) {
        Joined = joined;
    }
}
