package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.*;


/**
 * Created by 汤云飞 on 2016/7/13.
 */
public class JobIntensionResponse {

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer industryChain;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryChainName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectCityName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String expectZipcode;

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long functionId;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String function;

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer rank;

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float currentSalary;
    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isCompleteIntension;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String phone;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String email;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String currentCompanyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String jobTitle;

    @JsonSerialize(nullsUsing = NullShortSerializer.class)
    private Short isCompleteStudentIntension;

    public Integer getIndustryChain() {
        return industryChain;
    }

    @JsonSerialize(nullsUsing = NullFloatSerializer.class)
    private Float currentSalaryYear;

    public void setIndustryChain(Integer industryChain) {
        this.industryChain = industryChain;
    }

    public String getIndustryChainName() {
        return industryChainName;
    }

    public void setIndustryChainName(String industryChainName) {
        this.industryChainName = industryChainName;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Float getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(Float currentSalary) {
        this.currentSalary = currentSalary;
    }

    public Short getIsCompleteIntension() {
        return isCompleteIntension;
    }

    public void setIsCompleteIntension(Short isCompleteIntension) {
        this.isCompleteIntension = isCompleteIntension;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Float getCurrentSalaryYear() {
        return currentSalaryYear;
    }

    public void setCurrentSalaryYear(Float currentSalaryYear) {
        this.currentSalaryYear = currentSalaryYear;
    }

    public Short getIsCompleteStudentIntension() {
        return isCompleteStudentIntension;
    }

    public void setIsCompleteStudentIntension(Short isCompleteStudentIntension) {
        this.isCompleteStudentIntension = isCompleteStudentIntension;
    }
}
