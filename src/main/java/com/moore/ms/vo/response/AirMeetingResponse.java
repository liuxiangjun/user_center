package com.moore.ms.vo.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 *
 *  add by 李登修 2018/9/11
 *
 */
public class AirMeetingResponse {

    private Long seminarId;

    private String lecturerName;

    private String lecturerAvatar;

    private String lecturerTitle;

    private Integer lecturerLevel;

    private String companyName;

    private String companyFullname;

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
    private Date seminarStartTime;

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
    private Date seminarEndTime;

    private Short liveStatus;

    private Short status;

    private String seminarTitle;

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getSeminarTitle() {
        return seminarTitle;
    }

    public void setSeminarTitle(String seminarTitle) {
        this.seminarTitle = seminarTitle;
    }

    public Long getSeminarId() {
        return seminarId;
    }

    public void setSeminarId(Long seminarId) {
        this.seminarId = seminarId;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getLecturerAvatar() {
        return lecturerAvatar;
    }

    public void setLecturerAvatar(String lecturerAvatar) {
        this.lecturerAvatar = lecturerAvatar;
    }

    public String getLecturerTitle() {
        return lecturerTitle;
    }

    public void setLecturerTitle(String lecturerTitle) {
        this.lecturerTitle = lecturerTitle;
    }

    public Integer getLecturerLevel() {
        return lecturerLevel;
    }

    public void setLecturerLevel(Integer lecturerLevel) {
        this.lecturerLevel = lecturerLevel;
    }

    public Short getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(Short liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyFullname() {
        return companyFullname;
    }

    public void setCompanyFullname(String companyFullname) {
        this.companyFullname = companyFullname;
    }

    public Date getSeminarStartTime() {
        return seminarStartTime;
    }

    public void setSeminarStartTime(Date seminarStartTime) {
        this.seminarStartTime = seminarStartTime;
    }

    public Date getSeminarEndTime() {
        return seminarEndTime;
    }

    public void setSeminarEndTime(Date seminarEndTime) {
        this.seminarEndTime = seminarEndTime;
    }
}
