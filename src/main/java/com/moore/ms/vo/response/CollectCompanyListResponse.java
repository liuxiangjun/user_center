package com.moore.ms.vo.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullIntegerSerializer;
import com.moore.ms.common.jackson.NullLongSerializer;
import com.moore.ms.common.jackson.NullStringSerializer;

/**
 * Created by 汤云飞 on 2016/11/14.
 */
public class CollectCompanyListResponse {

    @JsonSerialize(nullsUsing = NullLongSerializer.class)
    private Long id;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String companyName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String logo;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryChainName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryName;

    @JsonSerialize(nullsUsing = NullStringSerializer.class)
    private String industryScale;


    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer jobNums;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getIndustryChainName() {
        return industryChainName;
    }

    public void setIndustryChainName(String industryChainName) {
        this.industryChainName = industryChainName;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(String industryScale) {
        this.industryScale = industryScale;
    }


    public Integer getJobNums() {
        return jobNums;
    }

    public void setJobNums(Integer jobNums) {
        this.jobNums = jobNums;
    }

}
