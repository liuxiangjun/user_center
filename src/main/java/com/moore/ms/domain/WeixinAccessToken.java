package com.moore.ms.domain;

import javax.persistence.*;

/**
 * Created by 汤云飞 on 2016/8/24.
 */
@Entity
@Table(name="weixin_access_token",schema = "moore_core")
public class WeixinAccessToken {

    @Id
    @SequenceGenerator(name = "weixin_access_token_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_access_token_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_access_token_id_seq")
    private Long id;

    private String appId;

    private String accessToken;

    private long expiresTime;

    public WeixinAccessToken(String appId, String accessToken, long expiresTime) {
        this.appId = appId;
        this.accessToken = accessToken;
        this.expiresTime = expiresTime;
    }

    public WeixinAccessToken() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresTime() {
        return expiresTime;
    }

    public void setExpiresTime(long expiresTime) {
        this.expiresTime = expiresTime;
    }
}
