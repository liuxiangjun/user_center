package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="sign_in_log",schema = "moore_core")
public class SignInLog {
    @Id
    @SequenceGenerator(name = "sign_in_log_id_seq",schema = "moore_core" ,sequenceName = "moore_core.sign_in_log_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "sign_in_log_id_seq")
    private Long id;
    private Long userId;
    private String openId;
    private Date createTime;
    private Long signInId;
    private String dtxOpenId;

    public SignInLog() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getSignInId() {
        return signInId;
    }

    public void setSignInId(Long signInId) {
        this.signInId = signInId;
    }

    public String getDtxOpenId() {
        return dtxOpenId;
    }

    public void setDtxOpenId(String dtxOpenId) {
        this.dtxOpenId = dtxOpenId;
    }
}