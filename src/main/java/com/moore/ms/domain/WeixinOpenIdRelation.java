package com.moore.ms.domain;

import javax.persistence.*;

/**
 * Created by 汤云飞 on 2017/8/14.
 */
@Entity
@Table(name="weixin_unionid_relation",schema = "moore_core")
public class WeixinOpenIdRelation {

    @Id
    @SequenceGenerator(name = "weixin_unionid_relation_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_unionid_relation_id_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY ,generator = "weixin_unionid_relation_id_seq")
    private Long id;
    private String mooreeliteOpenid;
    private String icbankOpenid;
    private String mooreliveOpenid;
    private String appOpenid;
    private String unionId;
    private String nickName;
    private String headImgUrl;
    private String sex;
    private String country;
    private String province;
    private String city;
    private String language;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMooreeliteOpenid() {
        return mooreeliteOpenid;
    }

    public void setMooreeliteOpenid(String mooreeliteOpenid) {
        this.mooreeliteOpenid = mooreeliteOpenid;
    }

    public String getIcbankOpenid() {
        return icbankOpenid;
    }

    public void setIcbankOpenid(String icbankOpenid) {
        this.icbankOpenid = icbankOpenid;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getMooreliveOpenid() {
        return mooreliveOpenid;
    }

    public void setMooreliveOpenid(String mooreliveOpenid) {
        this.mooreliveOpenid = mooreliveOpenid;
    }

    public String getAppOpenid() {
        return appOpenid;
    }

    public void setAppOpenid(String appOpenid) {
        this.appOpenid = appOpenid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
