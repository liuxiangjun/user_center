package com.moore.ms.domain;

import javax.persistence.*;

@Entity
@Table(name="sign_in_custom_key",schema = "moore_core")
public class SignInCustomKey {
    @Id
    @SequenceGenerator(name = "sign_in_custom_key_id_seq",schema = "moore_core" ,sequenceName = "moore_core.sign_in_custom_key_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "sign_in_custom_key_id_seq")
    private Long id;

    private String keyTitle;

    private Long signInId;

    public SignInCustomKey(Long id, String keyTitle, Long signInId) {
        this.id = id;
        this.keyTitle = keyTitle;
        this.signInId = signInId;
    }

    public SignInCustomKey() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyTitle() {
        return keyTitle;
    }

    public void setKeyTitle(String keyTitle) {
        this.keyTitle = keyTitle;
    }

    public Long getSignInId() {
        return signInId;
    }

    public void setSignInId(Long signInId) {
        this.signInId = signInId;
    }
}