package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="weixin_scene_limit_record",schema = "moore_core")
public class WeixinSceneLimitRecord {

    @Id
    @SequenceGenerator(name = "weixin_scene_limit_record_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_scene_limit_record_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_scene_limit_record_id_seq")
    private Integer id;

    private Integer sceneId;

    private String type;

    private String params;

    private Date createTime;

    private String ticket;

    private String url;

    private String wxServiceName;
    private String utmSource;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;
    private String utmMedium;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWxServiceName() {
        return wxServiceName;
    }

    public void setWxServiceName(String wxServiceName) {
        this.wxServiceName = wxServiceName;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }
}
