package com.moore.ms.domain;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="talent_library_email",schema = "moore_core")
public class TalentLibraryState {

    @Id
    @SequenceGenerator(name = "talent_library_email_id_seq",schema = "moore_core" ,sequenceName = "moore_core.talent_library_email_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "talent_library_email_id_seq")
    private Long id;

    private String emailAddress;

    private Long mtId;

    private Short emailState;

    private String optimalChannel;

    private String nextChannel;

    private Date createTime;

    private Date latestOpen;

    private Date latestClick;

    private Short failureReason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Long getMtId() {
        return mtId;
    }

    public void setMtId(Long mtId) {
        this.mtId = mtId;
    }

    public Short getEmailState() {
        return emailState;
    }

    public void setEmailState(Short emailState) {
        this.emailState = emailState;
    }

    public String getOptimalChannel() {
        return optimalChannel;
    }

    public void setOptimalChannel(String optimalChannel) {
        this.optimalChannel = optimalChannel;
    }

    public String getNextChannel() {
        return nextChannel;
    }

    public void setNextChannel(String nextChannel) {
        this.nextChannel = nextChannel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLatestOpen() {
        return latestOpen;
    }

    public void setLatestOpen(Date latestOpen) {
        this.latestOpen = latestOpen;
    }

    public Date getLatestClick() {
        return latestClick;
    }

    public void setLatestClick(Date latestClick) {
        this.latestClick = latestClick;
    }

    public Short getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(Short failureReason) {
        this.failureReason = failureReason;
    }
}
