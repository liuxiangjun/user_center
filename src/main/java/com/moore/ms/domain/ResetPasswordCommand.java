package com.moore.ms.domain;

/**
 * Created by 汤云飞 on 2017/7/6.
 */
public class ResetPasswordCommand {

    private String email;

    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
