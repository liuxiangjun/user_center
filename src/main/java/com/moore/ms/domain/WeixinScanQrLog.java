package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/12/7.
 */
@Entity
@Table(name="weixin_scan_qr_log",schema = "moore_core")
public class WeixinScanQrLog {

    @Id
    @SequenceGenerator(name = "weixin_scan_qr_log_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_scan_qr_log_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_scan_qr_log_id_seq")
    private Long id;

    private Long userId;

    private String openId;

    private Date createTime;

    private Integer sceneId;

    private Short qrType;

    private String utmSource;

    private String utmCampaign;

    private String utmContent;

    private String utmTerm;

    private String utmMedium;

    private String wxServiceName;

    public WeixinScanQrLog(Integer sceneId,String utmSource, String utmMedium, String utmCampaign,String utmContent, String utmTerm,Short qrType, String fromUser) {
        this.openId = fromUser;
        this.createTime = new Date();
        this.sceneId = sceneId;
        this.qrType = qrType;
        this.utmSource = utmSource;
        this.utmMedium = utmMedium;
        this.utmCampaign = utmCampaign;
        this.utmContent = utmContent;
        this.wxServiceName = "mooreelite";
        this.utmTerm = utmTerm;
    }


    public WeixinScanQrLog() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public Short getQrType() {
        return qrType;
    }

    public void setQrType(Short qrType) {
        this.qrType = qrType;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getWxServiceName() {
        return wxServiceName;
    }

    public void setWxServiceName(String wxServiceName) {
        this.wxServiceName = wxServiceName;
    }
}
