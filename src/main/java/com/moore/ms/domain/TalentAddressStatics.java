package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="talent_address_statics",schema = "moore_core")
public class TalentAddressStatics {
    @Id
    @SequenceGenerator(name = "talent_address_statics_id_seq",schema = "moore_core" ,sequenceName = "moore_core.talent_address_statics_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "talent_address_statics_id_seq")
    private Long id;
    private String address;
    private Short type;
    private Long successNum;
    private Long failNum;
    private Long openNum;
    private Date lastSend;
    private Date lastOpen;
    private Short isUnsubscribe;
    private String appId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Long getSuccessNum() {
        return successNum;
    }

    public void setSuccessNum(Long successNum) {
        this.successNum = successNum;
    }

    public Long getFailNum() {
        return failNum;
    }

    public void setFailNum(Long failNum) {
        this.failNum = failNum;
    }

    public Long getOpenNum() {
        return openNum;
    }

    public void setOpenNum(Long openNum) {
        this.openNum = openNum;
    }

    public Date getLastSend() {
        return lastSend;
    }

    public void setLastSend(Date lastSend) {
        this.lastSend = lastSend;
    }

    public Date getLastOpen() {
        return lastOpen;
    }

    public void setLastOpen(Date lastOpen) {
        this.lastOpen = lastOpen;
    }

    public Short getIsUnsubscribe() {
        return isUnsubscribe;
    }

    public void setIsUnsubscribe(Short isUnsubscribe) {
        this.isUnsubscribe = isUnsubscribe;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
