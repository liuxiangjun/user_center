package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/12/21.
 */
@Entity
@Table(name="wechat_scan_qrcode",schema = "moore_core")
public class WechatScanQrcode {

    @Id
    @SequenceGenerator(name = "wechat_scan_qrcode_id_seq",schema = "moore_core" ,sequenceName = "moore_core.wechat_scan_qrcode_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "wechat_scan_qrcode_id_seq")
    private Long id;

    private String code;

    private String openId;

    private String unionId;

    private String nickName;

    private String headImg;

    private Date createTime;

    private Long jobId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
