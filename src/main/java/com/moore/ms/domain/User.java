package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="users",schema = "moore_core")
public class User {

    @Id
    @SequenceGenerator(name = "users_id_seq",schema = "moore_core" ,sequenceName = "moore_core.users_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "users_id_seq")
    private Long id;

    private String email;

    private Short isEmailValidate;

    private Date registerTime;

    private String password;

    private String fullname;

    private Short education;

    private Short workingYear;

    private String avatar;

    private String mobilePhone;

    private Short isMobilePhoneValidate;

    private Integer cityNo;

    private String selfIntroduction;

    private String currentCompanyName;

    private Long currentCompanyId;

    private String currentJobTitle;

    private Short currentJobStatus;

    private Short resumeCompletePercent;

    private Short isDelete;

    private Date lastLoginTime;

    private Date modifyTime;

    private Date resumeUpdateTime;

    private String expectJobTitle;

    private Short expectJobStatus;

    private Integer expectJobCity;

    private Short expectJobSalary;

    private String expectJobDescription;

    private String recommendNumber;

    private String recommendQrcode;

    private Date recommendCreateTime;

    private Short recommendIsValidate;

    private Integer bonus;

    private Short registerStep;

    private String uploadedResume;

    private String emailDomain;

    private Short sex;

    private Short birthdayYear;

    private Short birthdayMonth;

    private String contactEmail;

    private String source;

    private String schoolName;

    private String blockdomain;

    private Long referrerId;

    private String referrerOpenId;

    private String fieldOfStudy;

    private Short isResumePublic;

    private String registerIp;

    private String customModuleTitle;

    private String customModuleContent;

    private String currentCityName;

    private String expectCityName;

    private String currentZipcode;

    private String expectZipcode;

    private String channel;

    private String utmKey;

    private Short autoRegister;

    private String cropping;

    private String linkedinId;

    private String linkedinToken;

    private String linkedinEmail;

    private Long linkedinTokenExpireTime;

    private Short isFeedback;

    private String importId;

    private Short isPasswordModify;

    private Integer websiteSource;

    private String userWebsite;

    private Integer firstPublic;

    private Date validateTime;

    private Short searchEngineIgnore;

    private String registerMobile;

    private Integer industryChain;

    private Long oneFunction;

    private Long function;

    private Integer rank;

    private Float currentSalary;

    private Float currentSalaryRaise;

    private Short isCompleteIntension;

    private Short englishListenLevel;

    private Short englishReadLevel;

    private Long engineCompanyId;

    private Date intensionCreateTime;

    private String finishSchoolYear;

    private Short isRegisterPhoneValidate;

    private Short groupType;

    private String researchDirection;

    private Short isCompleteStudentIntension;

    private Integer fastDeliverScore;

    private Integer fastDeliverRewardNum;

    private String currentCompanyJoinTime;

    private String utmSource;

    private String utmMedium;

    private String utmCampaign;

    private String utmContent;

    private String utmTerm;

    public Short getIsCompleteIntension() {
        return isCompleteIntension;
    }

    public void setIsCompleteIntension(Short isCompleteIntension) {
        this.isCompleteIntension = isCompleteIntension;
    }

    public String getRegisterMobile() {
        return registerMobile;
    }

    public void setRegisterMobile(String registerMobile) {
        this.registerMobile = registerMobile;
    }

    public Short getSearchEngineIgnore() {
        return searchEngineIgnore;
    }

    public void setSearchEngineIgnore(Short searchEngineIgnore) {
        this.searchEngineIgnore = searchEngineIgnore;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getIsEmailValidate() {
        return isEmailValidate;
    }

    public void setIsEmailValidate(Short isEmailValidate) {
        this.isEmailValidate = isEmailValidate;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Short getIsMobilePhoneValidate() {
        return isMobilePhoneValidate;
    }

    public void setIsMobilePhoneValidate(Short isMobilePhoneValidate) {
        this.isMobilePhoneValidate = isMobilePhoneValidate;
    }

    public Integer getCityNo() {
        return cityNo;
    }

    public void setCityNo(Integer cityNo) {
        this.cityNo = cityNo;
    }

    public String getSelfIntroduction() {
        return selfIntroduction;
    }

    public void setSelfIntroduction(String selfIntroduction) {
        this.selfIntroduction = selfIntroduction;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public Long getCurrentCompanyId() {
        return currentCompanyId;
    }

    public void setCurrentCompanyId(Long currentCompanyId) {
        this.currentCompanyId = currentCompanyId;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Short getCurrentJobStatus() {
        return currentJobStatus;
    }

    public void setCurrentJobStatus(Short currentJobStatus) {
        this.currentJobStatus = currentJobStatus;
    }

    public Short getResumeCompletePercent() {
        return resumeCompletePercent;
    }

    public void setResumeCompletePercent(Short resumeCompletePercent) {
        this.resumeCompletePercent = resumeCompletePercent;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getResumeUpdateTime() {
        return resumeUpdateTime;
    }

    public void setResumeUpdateTime(Date resumeUpdateTime) {
        this.resumeUpdateTime = resumeUpdateTime;
    }

    public String getExpectJobTitle() {
        return expectJobTitle;
    }

    public void setExpectJobTitle(String expectJobTitle) {
        this.expectJobTitle = expectJobTitle;
    }

    public Short getExpectJobStatus() {
        return expectJobStatus;
    }

    public void setExpectJobStatus(Short expectJobStatus) {
        this.expectJobStatus = expectJobStatus;
    }

    public Integer getExpectJobCity() {
        return expectJobCity;
    }

    public void setExpectJobCity(Integer expectJobCity) {
        this.expectJobCity = expectJobCity;
    }

    public Short getExpectJobSalary() {
        return expectJobSalary;
    }

    public void setExpectJobSalary(Short expectJobSalary) {
        this.expectJobSalary = expectJobSalary;
    }

    public String getExpectJobDescription() {
        return expectJobDescription;
    }

    public void setExpectJobDescription(String expectJobDescription) {
        this.expectJobDescription = expectJobDescription;
    }

    public String getRecommendNumber() {
        return recommendNumber;
    }

    public void setRecommendNumber(String recommendNumber) {
        this.recommendNumber = recommendNumber;
    }

    public String getRecommendQrcode() {
        return recommendQrcode;
    }

    public void setRecommendQrcode(String recommendQrcode) {
        this.recommendQrcode = recommendQrcode;
    }

    public Date getRecommendCreateTime() {
        return recommendCreateTime;
    }

    public void setRecommendCreateTime(Date recommendCreateTime) {
        this.recommendCreateTime = recommendCreateTime;
    }

    public Short getRecommendIsValidate() {
        return recommendIsValidate;
    }

    public void setRecommendIsValidate(Short recommendIsValidate) {
        this.recommendIsValidate = recommendIsValidate;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public Short getRegisterStep() {
        return registerStep;
    }

    public void setRegisterStep(Short registerStep) {
        this.registerStep = registerStep;
    }

    public String getUploadedResume() {
        return uploadedResume;
    }

    public void setUploadedResume(String uploadedResume) {
        this.uploadedResume = uploadedResume;
    }

    public String getEmailDomain() {
        return emailDomain;
    }

    public void setEmailDomain(String emailDomain) {
        this.emailDomain = emailDomain;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public Short getBirthdayYear() {
        return birthdayYear;
    }

    public void setBirthdayYear(Short birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

    public Short getBirthdayMonth() {
        return birthdayMonth;
    }

    public void setBirthdayMonth(Short birthdayMonth) {
        this.birthdayMonth = birthdayMonth;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        if(contactEmail != null) contactEmail = contactEmail.toLowerCase();
        this.contactEmail = contactEmail;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getBlockdomain() {
        return blockdomain;
    }

    public void setBlockdomain(String blockdomain) {
        this.blockdomain = blockdomain;
    }

    public Long getReferrerId() {
        return referrerId;
    }

    public void setReferrerId(Long referrerId) {
        this.referrerId = referrerId;
    }

    public String getReferrerOpenId() {
        return referrerOpenId;
    }

    public void setReferrerOpenId(String referrerOpenId) {
        this.referrerOpenId = referrerOpenId;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getIsResumePublic() {
        return isResumePublic;
    }

    public void setIsResumePublic(Short isResumePublic) {
        this.isResumePublic = isResumePublic;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public String getCustomModuleTitle() {
        return customModuleTitle;
    }

    public void setCustomModuleTitle(String customModuleTitle) {
        this.customModuleTitle = customModuleTitle;
    }

    public String getCustomModuleContent() {
        return customModuleContent;
    }

    public void setCustomModuleContent(String customModuleContent) {
        this.customModuleContent = customModuleContent;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public String getCurrentZipcode() {
        return currentZipcode;
    }

    public void setCurrentZipcode(String currentZipcode) {
        this.currentZipcode = currentZipcode;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public Short getAutoRegister() {
        return autoRegister;
    }

    public void setAutoRegister(Short autoRegister) {
        this.autoRegister = autoRegister;
    }

    public String getCropping() {
        return cropping;
    }

    public void setCropping(String cropping) {
        this.cropping = cropping;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getLinkedinToken() {
        return linkedinToken;
    }

    public void setLinkedinToken(String linkedinToken) {
        this.linkedinToken = linkedinToken;
    }

    public String getLinkedinEmail() {
        return linkedinEmail;
    }

    public void setLinkedinEmail(String linkedinEmail) {
        this.linkedinEmail = linkedinEmail;
    }

    public Long getLinkedinTokenExpireTime() {
        return linkedinTokenExpireTime;
    }

    public void setLinkedinTokenExpireTime(Long linkedinTokenExpireTime) {
        this.linkedinTokenExpireTime = linkedinTokenExpireTime;
    }

    public Short getIsFeedback() {
        return isFeedback;
    }

    public void setIsFeedback(Short isFeedback) {
        this.isFeedback = isFeedback;
    }

    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public Short getIsPasswordModify() {
        return isPasswordModify;
    }

    public void setIsPasswordModify(Short isPasswordModify) {
        this.isPasswordModify = isPasswordModify;
    }

    public Integer getWebsiteSource() {
        return websiteSource;
    }

    public void setWebsiteSource(Integer websiteSource) {
        this.websiteSource = websiteSource;
    }

    public String getUserWebsite() {
        return userWebsite;
    }

    public void setUserWebsite(String userWebsite) {
        this.userWebsite = userWebsite;
    }

    public Integer getFirstPublic() {
        return firstPublic;
    }

    public void setFirstPublic(Integer firstPublic) {
        this.firstPublic = firstPublic;
    }

    public Date getValidateTime() {
        return validateTime;
    }

    public void setValidateTime(Date validateTime) {
        this.validateTime = validateTime;
    }

    public Integer getIndustryChain() {
        return industryChain;
    }

    public void setIndustryChain(Integer industryChain) {
        this.industryChain = industryChain;
    }

    public Long getOneFunction() {
        return oneFunction;
    }

    public void setOneFunction(Long oneFunction) {
        this.oneFunction = oneFunction;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Float getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(Float currentSalary) {
        this.currentSalary = currentSalary;
    }

    public Short getEnglishListenLevel() {
        return englishListenLevel;
    }

    public void setEnglishListenLevel(Short englishListenLevel) {
        this.englishListenLevel = englishListenLevel;
    }

    public Short getEnglishReadLevel() {
        return englishReadLevel;
    }

    public void setEnglishReadLevel(Short englishReadLevel) {
        this.englishReadLevel = englishReadLevel;
    }

    public Float getCurrentSalaryRaise() {
        return currentSalaryRaise;
    }

    public void setCurrentSalaryRaise(Float currentSalaryRaise) {
        this.currentSalaryRaise = currentSalaryRaise;
    }

    public Long getEngineCompanyId() {
        return engineCompanyId;
    }

    public void setEngineCompanyId(Long engineCompanyId) {
        this.engineCompanyId = engineCompanyId;
    }

    public Date getIntensionCreateTime() {
        return intensionCreateTime;
    }

    public void setIntensionCreateTime(Date intensionCreateTime) {
        this.intensionCreateTime = intensionCreateTime;
    }

    public String getFinishSchoolYear() {
        return finishSchoolYear;
    }

    public void setFinishSchoolYear(String finishSchoolYear) {
        this.finishSchoolYear = finishSchoolYear;
    }

    public Short getIsRegisterPhoneValidate() {
        return isRegisterPhoneValidate;
    }

    public void setIsRegisterPhoneValidate(Short isRegisterPhoneValidate) {
        this.isRegisterPhoneValidate = isRegisterPhoneValidate;
    }

    public Short getGroupType() {
        return groupType;
    }

    public void setGroupType(Short groupType) {
        this.groupType = groupType;
    }

    public String getResearchDirection() {
        return researchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        this.researchDirection = researchDirection;
    }

    public Short getIsCompleteStudentIntension() {
        return isCompleteStudentIntension;
    }

    public void setIsCompleteStudentIntension(Short isCompleteStudentIntension) {
        this.isCompleteStudentIntension = isCompleteStudentIntension;
    }

    public Integer getFastDeliverScore() {
        return fastDeliverScore;
    }

    public void setFastDeliverScore(Integer fastDeliverScore) {
        this.fastDeliverScore = fastDeliverScore;
    }

    public Integer getFastDeliverRewardNum() {
        return fastDeliverRewardNum;
    }

    public void setFastDeliverRewardNum(Integer fastDeliverRewardNum) {
        this.fastDeliverRewardNum = fastDeliverRewardNum;
    }

    public String getCurrentCompanyJoinTime() {
        return currentCompanyJoinTime;
    }

    public void setCurrentCompanyJoinTime(String currentCompanyJoinTime) {
        this.currentCompanyJoinTime = currentCompanyJoinTime;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }
}
