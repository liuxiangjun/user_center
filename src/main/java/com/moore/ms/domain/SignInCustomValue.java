package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="sign_in_custom_value",schema = "moore_core")
public class SignInCustomValue {
    @Id
    @SequenceGenerator(name = "sign_in_custom_value_id_seq",schema = "moore_core" ,sequenceName = "moore_core.sign_in_custom_value_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "sign_in_custom_value_id_seq")
    private Long id;

    private Long userId;

    private Long customKeyId;

    private String customValue;

    private Date createTime;

    public SignInCustomValue(Long id, Long userId, Long customKeyId, String customValue, Date createTime) {
        this.id = id;
        this.userId = userId;
        this.customKeyId = customKeyId;
        this.customValue = customValue;
        this.createTime = createTime;
    }

    public SignInCustomValue() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCustomKeyId() {
        return customKeyId;
    }

    public void setCustomKeyId(Long customKeyId) {
        this.customKeyId = customKeyId;
    }

    public String getCustomValue() {
        return customValue;
    }

    public void setCustomValue(String customValue) {
        this.customValue = customValue == null ? null : customValue.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}