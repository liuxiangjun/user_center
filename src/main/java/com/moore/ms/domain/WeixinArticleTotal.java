package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_article_total",schema = "moore_core")
public class WeixinArticleTotal {
    @Id
    @SequenceGenerator(name = "weixin_article_total_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_article_total_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_article_total_id_seq")
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date refDate;
    private Short type;
    private Short userSource;
    private String msgid;
    private String title;
    @Temporal(TemporalType.DATE)
    private Date statDate;
    private Integer targetUser;
    private Integer intPageReadUser;
    private Integer intPageReadCount;
    private Integer oriPageReadUser;
    private Integer oriPageReadCount;
    private Integer shareUser;
    private Integer shareCount;
    private Integer addToFavUser;
    private Integer addToFavCount;
    private Integer intPageFromSessionReadUser;
    private Integer intPageFromSessionReadCount;
    private Integer intPageFromHistMsgReadUser;
    private Integer intPageFromHistMsgReadCount;
    private Integer intPageFromFeedReadUser;
    private Integer intPageFromFeedReadCount;
    private Integer intPageFromFriendsReadUser;
    private Integer intPageFromFriendsReadCount;
    private Integer intPageFromOtherReadUser;
    private Integer intPageFromOtherReadCount;
    private Integer feedShareFromSessionUser;
    private Integer feedShareFromSessionCnt;
    private Integer feedShareFromFeedUser;
    private Integer feedShareFromFeedCnt;
    private Integer feedShareFromOtherUser;
    private Integer feedShareFromOtherCnt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Short getUserSource() {
        return userSource;
    }

    public void setUserSource(Short userSource) {
        this.userSource = userSource;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStatDate() {
        return statDate;
    }

    public void setStatDate(Date statDate) {
        this.statDate = statDate;
    }

    public Integer getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(Integer targetUser) {
        this.targetUser = targetUser;
    }

    public Integer getIntPageReadUser() {
        return intPageReadUser;
    }

    public void setIntPageReadUser(Integer intPageReadUser) {
        this.intPageReadUser = intPageReadUser;
    }

    public Integer getIntPageReadCount() {
        return intPageReadCount;
    }

    public void setIntPageReadCount(Integer intPageReadCount) {
        this.intPageReadCount = intPageReadCount;
    }

    public Integer getOriPageReadUser() {
        return oriPageReadUser;
    }

    public void setOriPageReadUser(Integer oriPageReadUser) {
        this.oriPageReadUser = oriPageReadUser;
    }

    public Integer getOriPageReadCount() {
        return oriPageReadCount;
    }

    public void setOriPageReadCount(Integer oriPageReadCount) {
        this.oriPageReadCount = oriPageReadCount;
    }

    public Integer getShareUser() {
        return shareUser;
    }

    public void setShareUser(Integer shareUser) {
        this.shareUser = shareUser;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    public Integer getAddToFavUser() {
        return addToFavUser;
    }

    public void setAddToFavUser(Integer addToFavUser) {
        this.addToFavUser = addToFavUser;
    }

    public Integer getAddToFavCount() {
        return addToFavCount;
    }

    public void setAddToFavCount(Integer addToFavCount) {
        this.addToFavCount = addToFavCount;
    }

    public Integer getIntPageFromSessionReadUser() {
        return intPageFromSessionReadUser;
    }

    public void setIntPageFromSessionReadUser(Integer intPageFromSessionReadUser) {
        this.intPageFromSessionReadUser = intPageFromSessionReadUser;
    }

    public Integer getIntPageFromSessionReadCount() {
        return intPageFromSessionReadCount;
    }

    public void setIntPageFromSessionReadCount(Integer intPageFromSessionReadCount) {
        this.intPageFromSessionReadCount = intPageFromSessionReadCount;
    }

    public Integer getIntPageFromHistMsgReadUser() {
        return intPageFromHistMsgReadUser;
    }

    public void setIntPageFromHistMsgReadUser(Integer intPageFromHistMsgReadUser) {
        this.intPageFromHistMsgReadUser = intPageFromHistMsgReadUser;
    }

    public Integer getIntPageFromHistMsgReadCount() {
        return intPageFromHistMsgReadCount;
    }

    public void setIntPageFromHistMsgReadCount(Integer intPageFromHistMsgReadCount) {
        this.intPageFromHistMsgReadCount = intPageFromHistMsgReadCount;
    }

    public Integer getIntPageFromFeedReadUser() {
        return intPageFromFeedReadUser;
    }

    public void setIntPageFromFeedReadUser(Integer intPageFromFeedReadUser) {
        this.intPageFromFeedReadUser = intPageFromFeedReadUser;
    }

    public Integer getIntPageFromFeedReadCount() {
        return intPageFromFeedReadCount;
    }

    public void setIntPageFromFeedReadCount(Integer intPageFromFeedReadCount) {
        this.intPageFromFeedReadCount = intPageFromFeedReadCount;
    }

    public Integer getIntPageFromFriendsReadUser() {
        return intPageFromFriendsReadUser;
    }

    public void setIntPageFromFriendsReadUser(Integer intPageFromFriendsReadUser) {
        this.intPageFromFriendsReadUser = intPageFromFriendsReadUser;
    }

    public Integer getIntPageFromFriendsReadCount() {
        return intPageFromFriendsReadCount;
    }

    public void setIntPageFromFriendsReadCount(Integer intPageFromFriendsReadCount) {
        this.intPageFromFriendsReadCount = intPageFromFriendsReadCount;
    }

    public Integer getIntPageFromOtherReadUser() {
        return intPageFromOtherReadUser;
    }

    public void setIntPageFromOtherReadUser(Integer intPageFromOtherReadUser) {
        this.intPageFromOtherReadUser = intPageFromOtherReadUser;
    }

    public Integer getIntPageFromOtherReadCount() {
        return intPageFromOtherReadCount;
    }

    public void setIntPageFromOtherReadCount(Integer intPageFromOtherReadCount) {
        this.intPageFromOtherReadCount = intPageFromOtherReadCount;
    }

    public Integer getFeedShareFromSessionUser() {
        return feedShareFromSessionUser;
    }

    public void setFeedShareFromSessionUser(Integer feedShareFromSessionUser) {
        this.feedShareFromSessionUser = feedShareFromSessionUser;
    }

    public Integer getFeedShareFromSessionCnt() {
        return feedShareFromSessionCnt;
    }

    public void setFeedShareFromSessionCnt(Integer feedShareFromSessionCnt) {
        this.feedShareFromSessionCnt = feedShareFromSessionCnt;
    }

    public Integer getFeedShareFromFeedUser() {
        return feedShareFromFeedUser;
    }

    public void setFeedShareFromFeedUser(Integer feedShareFromFeedUser) {
        this.feedShareFromFeedUser = feedShareFromFeedUser;
    }

    public Integer getFeedShareFromFeedCnt() {
        return feedShareFromFeedCnt;
    }

    public void setFeedShareFromFeedCnt(Integer feedShareFromFeedCnt) {
        this.feedShareFromFeedCnt = feedShareFromFeedCnt;
    }

    public Integer getFeedShareFromOtherUser() {
        return feedShareFromOtherUser;
    }

    public void setFeedShareFromOtherUser(Integer feedShareFromOtherUser) {
        this.feedShareFromOtherUser = feedShareFromOtherUser;
    }

    public Integer getFeedShareFromOtherCnt() {
        return feedShareFromOtherCnt;
    }

    public void setFeedShareFromOtherCnt(Integer feedShareFromOtherCnt) {
        this.feedShareFromOtherCnt = feedShareFromOtherCnt;
    }
}