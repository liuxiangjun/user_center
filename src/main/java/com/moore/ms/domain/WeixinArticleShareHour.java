package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_article_share_hour",schema = "moore_core")
public class WeixinArticleShareHour {
    @Id
    @SequenceGenerator(name = "weixin_article_share_hour_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_article_share_hour_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_article_share_hour_id_seq")
    private Long id;
    private Short type;
    @Temporal(TemporalType.DATE)
    private Date refDate;
    private Integer refHour;
    private Short userSource;
    private Short shareScene;
    private Integer shareCount;
    private Integer shareUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Integer getRefHour() {
        return refHour;
    }

    public void setRefHour(Integer refHour) {
        this.refHour = refHour;
    }

    public Short getUserSource() {
        return userSource;
    }

    public void setUserSource(Short userSource) {
        this.userSource = userSource;
    }

    public Short getShareScene() {
        return shareScene;
    }

    public void setShareScene(Short shareScene) {
        this.shareScene = shareScene;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    public Integer getShareUser() {
        return shareUser;
    }

    public void setShareUser(Integer shareUser) {
        this.shareUser = shareUser;
    }
}