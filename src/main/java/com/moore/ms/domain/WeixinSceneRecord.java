package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="weixin_scene_record",schema = "moore_core")
public class WeixinSceneRecord {

    @Id
    @SequenceGenerator(name = "weixin_scene_record_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_scene_record_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_scene_record_id_seq")
    private Long id;

    private Integer sceneId;

    private String type;

    private String params;

    private Date createTime;

    private Date expireTime;

    private Integer expireSeconds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public Integer getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(Integer expireSeconds) {
        this.expireSeconds = expireSeconds;
    }
}
