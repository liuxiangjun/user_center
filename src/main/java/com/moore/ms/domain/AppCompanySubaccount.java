package com.moore.ms.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="app_company_subaccount",schema = "moore_core")
public class AppCompanySubaccount implements Serializable {

    @Id
    @SequenceGenerator(name = "app_company_subaccount_id_seq",schema = "moore_core" ,sequenceName = "moore_core.app_company_subaccount_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "app_company_subaccount_id_seq")
    private Long id;

    private String hrEmail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    private Date registerIme;

    private String password;

    private String hrFullname;

    private String mobilePhone;

    private String emailForResume;

    private Short isDelete;

    private Date modifyTime;

    private String department;

    private String avatar;

    private String title;

    private String personalProfile;

    private Short isHrEmailValidate;

    private Short type;

    private Short isAdmin;

    private Date createTime;

    private Integer grade;

    private String publishUrl;

    private Long departmentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHrEmail() {
        return hrEmail;
    }

    public void setHrEmail(String hrEmail) {
        this.hrEmail = hrEmail == null ? null : hrEmail.trim();
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getRegisterIme() {
        return registerIme;
    }

    public void setRegisterIme(Date registerIme) {
        this.registerIme = registerIme;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getHrFullname() {
        return hrFullname;
    }

    public void setHrFullname(String hrFullname) {
        this.hrFullname = hrFullname == null ? null : hrFullname.trim();
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone == null ? null : mobilePhone.trim();
    }

    public String getEmailForResume() {
        return emailForResume;
    }

    public void setEmailForResume(String emailForResume) {
        this.emailForResume = emailForResume == null ? null : emailForResume.trim();
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getPersonalProfile() {
        return personalProfile;
    }

    public void setPersonalProfile(String personalProfile) {
        this.personalProfile = personalProfile == null ? null : personalProfile.trim();
    }

    public Short getIsHrEmailValidate() {
        return isHrEmailValidate;
    }

    public void setIsHrEmailValidate(Short isHrEmailValidate) {
        this.isHrEmailValidate = isHrEmailValidate;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Short getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Short isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getPublishUrl() {
        return publishUrl;
    }

    public void setPublishUrl(String publishUrl) {
        this.publishUrl = publishUrl;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
}