package com.moore.ms.domain;

import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="weixin_moore_binding",schema = "moore_core")
public class WeixinMooreBinding {

    @Id
    @SequenceGenerator(name = "weixin_moore_binding_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_moore_binding_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_moore_binding_id_seq")
    private Long id;

    private String openId;

    private Long userId;

    private String email;

    private Date createTime;

    private String unionId;

    private Integer type;

    private String nickname;

    private String headimgurl;

    private Integer step;

    private Integer collstellation;

    private Integer supplychain;

    private Long level;

    private Long oneLevel;

    private Integer jobLevel;

    private Float salary;

    private Integer ranking;

    private String character;

    private String advantage;

    private String disadvantaged;

    private Integer isDelete;

    public WeixinMooreBinding(String openId, Long uid, String email, String unionId, Integer type) {
        this.openId = openId;
        this.userId = uid;
        if (StringUtils.isNotBlank(email)){
            this.email = email.toLowerCase();
        }
        this.createTime = new Date();
        this.unionId = unionId;
        this.type = type;
    }

    public WeixinMooreBinding() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Integer getCollstellation() {
        return collstellation;
    }

    public void setCollstellation(Integer collstellation) {
        this.collstellation = collstellation;
    }

    public Integer getSupplychain() {
        return supplychain;
    }

    public void setSupplychain(Integer supplychain) {
        this.supplychain = supplychain;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getOneLevel() {
        return oneLevel;
    }

    public void setOneLevel(Long oneLevel) {
        this.oneLevel = oneLevel;
    }

    public Integer getJobLevel() {
        return jobLevel;
    }

    public void setJobLevel(Integer jobLevel) {
        this.jobLevel = jobLevel;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    public String getDisadvantaged() {
        return disadvantaged;
    }

    public void setDisadvantaged(String disadvantaged) {
        this.disadvantaged = disadvantaged;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
