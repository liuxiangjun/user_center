package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="sign_in",schema = "moore_core")
public class SignIn {
    @Id
    @SequenceGenerator(name = "sign_in_id_seq",schema = "moore_core" ,sequenceName = "moore_core.sign_in_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "sign_in_id_seq")
    private Long id;

    private String title;

    private String shareBrief;

    private String shareIcon;

    private String content;

    private String contentTwo;

    private Short isShowFlag;

    private String flagSuffix;

    private String formTitle;

    private String formBtnName;

    private Integer sceneId;

    private String sceneTitle;

    private String sceneScanTip;

    private Date createTime;

    private Date modifyTime;

    private Short isDeleted;

    private String sucContent;

    private String sucBtnName;

    private String sucTip;

    private String sucLink;

    private String sucGuideShareTip;

    private String btnColor;

    private String bgColor;

    private Short type;

    private String utmKey;

    public SignIn(Long id, String title, String shareBrief, String shareIcon, String content, String contentTwo, Short isShowFlag, String flagSuffix, String formTitle, String formBtnName, Integer sceneId, String sceneTitle, String sceneScanTip, Date createTime, Date modifyTime, Short isDeleted, String sucContent, String sucBtnName, String sucTip, String sucLink, String sucGuideShareTip, String btnColor, String bgColor, Short type, String utmKey) {
        this.id = id;
        this.title = title;
        this.shareBrief = shareBrief;
        this.shareIcon = shareIcon;
        this.content = content;
        this.contentTwo = contentTwo;
        this.isShowFlag = isShowFlag;
        this.flagSuffix = flagSuffix;
        this.formTitle = formTitle;
        this.formBtnName = formBtnName;
        this.sceneId = sceneId;
        this.sceneTitle = sceneTitle;
        this.sceneScanTip = sceneScanTip;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
        this.isDeleted = isDeleted;
        this.sucContent = sucContent;
        this.sucBtnName = sucBtnName;
        this.sucTip = sucTip;
        this.sucLink = sucLink;
        this.sucGuideShareTip = sucGuideShareTip;
        this.btnColor = btnColor;
        this.bgColor = bgColor;
        this.type = type;
        this.utmKey = utmKey;
    }

    public SignIn() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getShareBrief() {
        return shareBrief;
    }

    public void setShareBrief(String shareBrief) {
        this.shareBrief = shareBrief == null ? null : shareBrief.trim();
    }

    public String getShareIcon() {
        return shareIcon;
    }

    public void setShareIcon(String shareIcon) {
        this.shareIcon = shareIcon == null ? null : shareIcon.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getContentTwo() {
        return contentTwo;
    }

    public void setContentTwo(String contentTwo) {
        this.contentTwo = contentTwo == null ? null : contentTwo.trim();
    }

    public Short getIsShowFlag() {
        return isShowFlag;
    }

    public void setIsShowFlag(Short isShowFlag) {
        this.isShowFlag = isShowFlag;
    }

    public String getFlagSuffix() {
        return flagSuffix;
    }

    public void setFlagSuffix(String flagSuffix) {
        this.flagSuffix = flagSuffix == null ? null : flagSuffix.trim();
    }

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle == null ? null : formTitle.trim();
    }

    public String getFormBtnName() {
        return formBtnName;
    }

    public void setFormBtnName(String formBtnName) {
        this.formBtnName = formBtnName == null ? null : formBtnName.trim();
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public String getSceneTitle() {
        return sceneTitle;
    }

    public void setSceneTitle(String sceneTitle) {
        this.sceneTitle = sceneTitle == null ? null : sceneTitle.trim();
    }

    public String getSceneScanTip() {
        return sceneScanTip;
    }

    public void setSceneScanTip(String sceneScanTip) {
        this.sceneScanTip = sceneScanTip == null ? null : sceneScanTip.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Short getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Short isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getSucContent() {
        return sucContent;
    }

    public void setSucContent(String sucContent) {
        this.sucContent = sucContent == null ? null : sucContent.trim();
    }

    public String getSucBtnName() {
        return sucBtnName;
    }

    public void setSucBtnName(String sucBtnName) {
        this.sucBtnName = sucBtnName == null ? null : sucBtnName.trim();
    }

    public String getSucTip() {
        return sucTip;
    }

    public void setSucTip(String sucTip) {
        this.sucTip = sucTip == null ? null : sucTip.trim();
    }

    public String getSucLink() {
        return sucLink;
    }

    public void setSucLink(String sucLink) {
        this.sucLink = sucLink == null ? null : sucLink.trim();
    }

    public String getSucGuideShareTip() {
        return sucGuideShareTip;
    }

    public void setSucGuideShareTip(String sucGuideShareTip) {
        this.sucGuideShareTip = sucGuideShareTip == null ? null : sucGuideShareTip.trim();
    }

    public String getBtnColor() {
        return btnColor;
    }

    public void setBtnColor(String btnColor) {
        this.btnColor = btnColor == null ? null : btnColor.trim();
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor == null ? null : bgColor.trim();
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey == null ? null : utmKey.trim();
    }
}