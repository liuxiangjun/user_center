package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_user_cumulate",schema = "moore_core")
public class WeixinUserCumulate {
    @Id
    @SequenceGenerator(name = "weixin_user_cumulate_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_user_cumulate_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_user_cumulate_id_seq")
    private Long id;

    private Short type;

    @Temporal(TemporalType.DATE)
    private Date refDate;

    private Integer cumulateUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Integer getCumulateUser() {
        return cumulateUser;
    }

    public void setCumulateUser(Integer cumulateUser) {
        this.cumulateUser = cumulateUser;
    }
}