package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_article_read",schema = "moore_core")
public class WeixinArticleRead {
    @Id
    @SequenceGenerator(name = "weixin_article_read_id_seq", schema = "moore_core", sequenceName = "moore_core.weixin_article_read_id_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "weixin_article_read_id_seq")
    private Long id;
    private Short type;
    @Temporal(TemporalType.DATE)
    private Date refDate;
    private Short userSource;
    private Integer intPageReadUser;
    private Integer intPageReadCount;
    private Integer oriPageReadUser;
    private Integer oriPageReadCount;
    private Integer shareUser;
    private Integer shareCount;
    private Integer addToFavUser;
    private Integer addToFavCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Short getUserSource() {
        return userSource;
    }

    public void setUserSource(Short userSource) {
        this.userSource = userSource;
    }

    public Integer getIntPageReadUser() {
        return intPageReadUser;
    }

    public void setIntPageReadUser(Integer intPageReadUser) {
        this.intPageReadUser = intPageReadUser;
    }

    public Integer getIntPageReadCount() {
        return intPageReadCount;
    }

    public void setIntPageReadCount(Integer intPageReadCount) {
        this.intPageReadCount = intPageReadCount;
    }

    public Integer getOriPageReadUser() {
        return oriPageReadUser;
    }

    public void setOriPageReadUser(Integer oriPageReadUser) {
        this.oriPageReadUser = oriPageReadUser;
    }

    public Integer getOriPageReadCount() {
        return oriPageReadCount;
    }

    public void setOriPageReadCount(Integer oriPageReadCount) {
        this.oriPageReadCount = oriPageReadCount;
    }

    public Integer getShareUser() {
        return shareUser;
    }

    public void setShareUser(Integer shareUser) {
        this.shareUser = shareUser;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    public Integer getAddToFavUser() {
        return addToFavUser;
    }

    public void setAddToFavUser(Integer addToFavUser) {
        this.addToFavUser = addToFavUser;
    }

    public Integer getAddToFavCount() {
        return addToFavCount;
    }

    public void setAddToFavCount(Integer addToFavCount) {
        this.addToFavCount = addToFavCount;
    }
}