package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by pickj on 2017/8/17.
 */
@Entity
@Table(name="weixin_user_img",schema = "moore_core")
public class WeixinUserImg {
    @Id
    @SequenceGenerator(name = "weixin_user_img_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_user_img_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_user_img_id_seq")
    private Long id;
    private String openId;
    private String mediaId;
    private String cardId;
    private Long userId;
    private Integer cardOne;
    private Integer cardTwo;
    private Integer cardThree;
    private Integer cardFour;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getCardOne() {
        return cardOne;
    }

    public void setCardOne(Integer cardOne) {
        this.cardOne = cardOne;
    }

    public Integer getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(Integer cardTwo) {
        this.cardTwo = cardTwo;
    }

    public Integer getCardThree() {
        return cardThree;
    }

    public void setCardThree(Integer cardThree) {
        this.cardThree = cardThree;
    }

    public Integer getCardFour() {
        return cardFour;
    }

    public void setCardFour(Integer cardFour) {
        this.cardFour = cardFour;
    }
}
