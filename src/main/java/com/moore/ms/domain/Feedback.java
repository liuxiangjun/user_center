package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="feedback",schema = "moore_core")
public class Feedback {

    @Id
    @SequenceGenerator(name = "feedback_id_seq",schema = "moore_core" ,sequenceName = "moore_core.feedback_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "feedback_id_seq")
    private Integer id;

    private String content;

    private String email;

    private Date createTime;

    private String source;

    private String openId;

    private Long userId;

    private Character type;

    private String mobile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Character getType() {
        return type;
    }

    public void setType(Character type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
