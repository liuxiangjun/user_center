package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/9.
 */
@Entity
@Table(name="company",schema = "moore_core")
public class Company {

    @Id
    @SequenceGenerator(name = "company_id_seq",schema = "moore_core" ,sequenceName = "moore_core.company_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "company_id_seq")
    private Long id;

    private String hrEmail;

    private Short isHrEmailValidate;

    private Date registerTime;

    private String password;

    private String hrFullname;

    private String mobilePhone;

    private String emailForResume;

    private String companyFullname;

    private String companyShortname;

    private String logo;

    private String website;

    private String description;

    private String cityNames;

    private String industryId;

    private String industryName;

    private Short industryScale;

    private String shortDescription;

    private String tags;

    private Date modifyTime;

    private Date hrLastLoginTime;

    private Short isDelete;

    private Short isChecked;

    private Short isVerify;

    private Short registerStep;

    private String industryChainId;

    private String industryChainName;

    private String emailDomain;

    private String descriptionImg;

    private String tagsName;

    private Integer inviteTalentNum;

    private Integer surplusTalentNum;

    private Integer alreadyTalentNum;

    private String resumeProcessingRate;

    private String resumeProcessing;

    private Long engineCompanyId;

    private String mediaVideoOrigin;

    @Column(name = "media_video_240p")
    private String mediaVideo240p;

    @Column(name = "media_video_360p")
    private String mediaVideo360p;

    @Column(name = "media_video_720p")
    private String mediaVideo720p;

    private String mediaImg;

    private Short mediaType;

    private Short level;

    private String logoThumbnailSm;
    private String logoThumbnailLg;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHrEmail() {
        return hrEmail;
    }

    public void setHrEmail(String hrEmail) {
        this.hrEmail = hrEmail;
    }

    public Short getIsHrEmailValidate() {
        return isHrEmailValidate;
    }

    public void setIsHrEmailValidate(Short isHrEmailValidate) {
        this.isHrEmailValidate = isHrEmailValidate;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHrFullname() {
        return hrFullname;
    }

    public void setHrFullname(String hrFullname) {
        this.hrFullname = hrFullname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmailForResume() {
        return emailForResume;
    }

    public void setEmailForResume(String emailForResume) {
        this.emailForResume = emailForResume;
    }

    public String getCompanyFullname() {
        return companyFullname;
    }

    public void setCompanyFullname(String companyFullname) {
        this.companyFullname = companyFullname;
    }

    public String getCompanyShortname() {
        return companyShortname;
    }

    public void setCompanyShortname(String companyShortname) {
        this.companyShortname = companyShortname;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCityNames() {
        return cityNames;
    }

    public void setCityNames(String cityNames) {
        this.cityNames = cityNames;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public Short getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(Short industryScale) {
        this.industryScale = industryScale;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getHrLastLoginTime() {
        return hrLastLoginTime;
    }

    public void setHrLastLoginTime(Date hrLastLoginTime) {
        this.hrLastLoginTime = hrLastLoginTime;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    public Short getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Short isChecked) {
        this.isChecked = isChecked;
    }

    public Short getIsVerify() {
        return isVerify;
    }

    public void setIsVerify(Short isVerify) {
        this.isVerify = isVerify;
    }

    public Short getRegisterStep() {
        return registerStep;
    }

    public void setRegisterStep(Short registerStep) {
        this.registerStep = registerStep;
    }

    public String getIndustryChainId() {
        return industryChainId;
    }

    public void setIndustryChainId(String industryChainId) {
        this.industryChainId = industryChainId;
    }

    public String getIndustryChainName() {
        return industryChainName;
    }

    public void setIndustryChainName(String industryChainName) {
        this.industryChainName = industryChainName;
    }

    public String getEmailDomain() {
        return emailDomain;
    }

    public void setEmailDomain(String emailDomain) {
        this.emailDomain = emailDomain;
    }

    public String getDescriptionImg() {
        return descriptionImg;
    }

    public void setDescriptionImg(String descriptionImg) {
        this.descriptionImg = descriptionImg;
    }

    public String getTagsName() {
        return tagsName;
    }

    public void setTagsName(String tagsName) {
        this.tagsName = tagsName;
    }

    public Integer getInviteTalentNum() {
        return inviteTalentNum;
    }

    public void setInviteTalentNum(Integer inviteTalentNum) {
        this.inviteTalentNum = inviteTalentNum;
    }

    public Integer getSurplusTalentNum() {
        return surplusTalentNum;
    }

    public void setSurplusTalentNum(Integer surplusTalentNum) {
        this.surplusTalentNum = surplusTalentNum;
    }

    public Integer getAlreadyTalentNum() {
        return alreadyTalentNum;
    }

    public void setAlreadyTalentNum(Integer alreadyTalentNum) {
        this.alreadyTalentNum = alreadyTalentNum;
    }

    public String getResumeProcessingRate() {
        return resumeProcessingRate;
    }

    public void setResumeProcessingRate(String resumeProcessingRate) {
        this.resumeProcessingRate = resumeProcessingRate;
    }

    public String getResumeProcessing() {
        return resumeProcessing;
    }

    public void setResumeProcessing(String resumeProcessing) {
        this.resumeProcessing = resumeProcessing;
    }

    public Long getEngineCompanyId() {
        return engineCompanyId;
    }

    public void setEngineCompanyId(Long engineCompanyId) {
        this.engineCompanyId = engineCompanyId;
    }

    public String getMediaVideoOrigin() {
        return mediaVideoOrigin;
    }

    public void setMediaVideoOrigin(String mediaVideoOrigin) {
        this.mediaVideoOrigin = mediaVideoOrigin;
    }

    public String getMediaVideo240p() {
        return mediaVideo240p;
    }

    public void setMediaVideo240p(String mediaVideo240p) {
        this.mediaVideo240p = mediaVideo240p;
    }

    public String getMediaVideo360p() {
        return mediaVideo360p;
    }

    public void setMediaVideo360p(String mediaVideo360p) {
        this.mediaVideo360p = mediaVideo360p;
    }

    public String getMediaVideo720p() {
        return mediaVideo720p;
    }

    public void setMediaVideo720p(String mediaVideo720p) {
        this.mediaVideo720p = mediaVideo720p;
    }

    public String getMediaImg() {
        return mediaImg;
    }

    public void setMediaImg(String mediaImg) {
        this.mediaImg = mediaImg;
    }

    public Short getMediaType() {
        return mediaType;
    }

    public void setMediaType(Short mediaType) {
        this.mediaType = mediaType;
    }

    public Short getLevel() {
        return level;
    }

    public void setLevel(Short level) {
        this.level = level;
    }

    public String getLogoThumbnailSm() {
        return logoThumbnailSm;
    }

    public void setLogoThumbnailSm(String logoThumbnailSm) {
        this.logoThumbnailSm = logoThumbnailSm;
    }

    public String getLogoThumbnailLg() {
        return logoThumbnailLg;
    }

    public void setLogoThumbnailLg(String logoThumbnailLg) {
        this.logoThumbnailLg = logoThumbnailLg;
    }
}
