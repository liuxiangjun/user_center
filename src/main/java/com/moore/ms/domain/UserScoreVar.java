package com.moore.ms.domain;

import java.lang.annotation.*;

/**
 * User: Caixiaopig
 * Date: 15/8/23
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserScoreVar {
    String value() default "";
}
