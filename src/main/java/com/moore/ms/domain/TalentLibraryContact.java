package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="talent_library_contact",schema = "moore_core")
public class TalentLibraryContact {

    @Id
    @SequenceGenerator(name = "talent_library_contact_id_seq",schema = "moore_core" ,sequenceName = "moore_core.talent_library_contact_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "talent_library_contact_id_seq")
    private Long id;

    private Long talentLibraryId;

    private Short contactType;

    private Date createTime;

    private String contact;

    private Integer emailSendNum;

    private Date emailLastSendTime;

    private Integer emailReadNum;

    private Date emailLastReadTime;

    private Integer emailClickNum;

    private Date emailLastClickTime;

    private Integer smsSendNum;

    private Date smsLastSendTime;

    private Short isCompanyEmail;

    private String emailSuffix;

    private Short isInherit;

    private Long nextTalentLibraryId;

    public TalentLibraryContact(Long id, Long talentLibraryId, Short contactType, Date createTime, String contact, Integer emailSendNum, Date emailLastSendTime, Integer emailReadNum, Date emailLastReadTime, Integer emailClickNum, Date emailLastClickTime, Integer smsSendNum, Date smsLastSendTime, Short isCompanyEmail, String emailSuffix, Short isInherit, Long nextTalentLibraryId) {
        this.id = id;
        this.talentLibraryId = talentLibraryId;
        this.contactType = contactType;
        this.createTime = createTime;
        this.contact = contact;
        this.emailSendNum = emailSendNum;
        this.emailLastSendTime = emailLastSendTime;
        this.emailReadNum = emailReadNum;
        this.emailLastReadTime = emailLastReadTime;
        this.emailClickNum = emailClickNum;
        this.emailLastClickTime = emailLastClickTime;
        this.smsSendNum = smsSendNum;
        this.smsLastSendTime = smsLastSendTime;
        this.isCompanyEmail = isCompanyEmail;
        this.emailSuffix = emailSuffix;
        this.isInherit = isInherit;
        this.nextTalentLibraryId = nextTalentLibraryId;
    }

    public TalentLibraryContact() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTalentLibraryId() {
        return talentLibraryId;
    }

    public void setTalentLibraryId(Long talentLibraryId) {
        this.talentLibraryId = talentLibraryId;
    }

    public Short getContactType() {
        return contactType;
    }

    public void setContactType(Short contactType) {
        this.contactType = contactType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public Integer getEmailSendNum() {
        return emailSendNum;
    }

    public void setEmailSendNum(Integer emailSendNum) {
        this.emailSendNum = emailSendNum;
    }

    public Date getEmailLastSendTime() {
        return emailLastSendTime;
    }

    public void setEmailLastSendTime(Date emailLastSendTime) {
        this.emailLastSendTime = emailLastSendTime;
    }

    public Integer getEmailReadNum() {
        return emailReadNum;
    }

    public void setEmailReadNum(Integer emailReadNum) {
        this.emailReadNum = emailReadNum;
    }

    public Date getEmailLastReadTime() {
        return emailLastReadTime;
    }

    public void setEmailLastReadTime(Date emailLastReadTime) {
        this.emailLastReadTime = emailLastReadTime;
    }

    public Integer getEmailClickNum() {
        return emailClickNum;
    }

    public void setEmailClickNum(Integer emailClickNum) {
        this.emailClickNum = emailClickNum;
    }

    public Date getEmailLastClickTime() {
        return emailLastClickTime;
    }

    public void setEmailLastClickTime(Date emailLastClickTime) {
        this.emailLastClickTime = emailLastClickTime;
    }

    public Integer getSmsSendNum() {
        return smsSendNum;
    }

    public void setSmsSendNum(Integer smsSendNum) {
        this.smsSendNum = smsSendNum;
    }

    public Date getSmsLastSendTime() {
        return smsLastSendTime;
    }

    public void setSmsLastSendTime(Date smsLastSendTime) {
        this.smsLastSendTime = smsLastSendTime;
    }

    public Short getIsCompanyEmail() {
        return isCompanyEmail;
    }

    public void setIsCompanyEmail(Short isCompanyEmail) {
        this.isCompanyEmail = isCompanyEmail;
    }

    public String getEmailSuffix() {
        return emailSuffix;
    }

    public void setEmailSuffix(String emailSuffix) {
        this.emailSuffix = emailSuffix == null ? null : emailSuffix.trim();
    }

    public Short getIsInherit() {
        return isInherit;
    }

    public void setIsInherit(Short isInherit) {
        this.isInherit = isInherit;
    }

    public Long getNextTalentLibraryId() {
        return nextTalentLibraryId;
    }

    public void setNextTalentLibraryId(Long nextTalentLibraryId) {
        this.nextTalentLibraryId = nextTalentLibraryId;
    }
}