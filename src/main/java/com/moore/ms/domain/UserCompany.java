package com.moore.ms.domain;

import org.apache.commons.lang.StringUtils;
import com.moore.ms.common.utils.DateUtil;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="user_company",schema = "moore_core")
public class UserCompany implements  Comparable<UserCompany> {

    @Id
    @SequenceGenerator(name = "user_company_id_seq",schema = "moore_core" ,sequenceName = "moore_core.user_company_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "user_company_id_seq")
    private Long id;

    private Long userId;

    private String name;

    private Long companyId;

    private String jobTitle;

    private String joinTime;

    private String leaveTime;

    private String jobContent;

    private Long engineCompanyId;

    private Date createTime = new Date();

    private Date modifyTime = new Date();


    public UserCompany() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle == null ? null : jobTitle.trim();
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime == null ? null : joinTime.trim();
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime == null ? null : leaveTime.trim();
    }

    public String getJobContent() {
        return jobContent;
    }

    public void setJobContent(String jobContent) {
        this.jobContent = jobContent == null ? null : jobContent.trim();
    }

    public Long getEngineCompanyId() {
        return engineCompanyId;
    }

    public void setEngineCompanyId(Long engineCompanyId) {
        this.engineCompanyId = engineCompanyId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int compareTo(UserCompany o) {
        Date oD = DateUtil.StrToDate("1900-01-01","yyyy-MM-dd");
        if (StringUtils.isNotBlank(o.getJoinTime())){
            oD = DateUtil.StrToDate(o.getJoinTime(),"yyyy.MM");
            if (oD==null){
                oD = DateUtil.StrToDate(o.getJoinTime(),"yyyy.M");
                if (oD == null){
                    oD = DateUtil.StrToDate(o.getJoinTime(),"yyyy");
                    if (oD == null){
                        oD = new Date();
                    }
                }
            }
        }
        Date thiD = DateUtil.StrToDate("1900-01-01","yyyy-MM-dd");
        if (StringUtils.isNotBlank(joinTime)){
            thiD = DateUtil.StrToDate(joinTime,"yyyy.MM");
            if (thiD==null) {
                thiD = DateUtil.StrToDate(joinTime, "yyyy.M");
                if (thiD == null) {
                    thiD = DateUtil.StrToDate(joinTime, "yyyy");
                    if (thiD == null) {
                        thiD = new Date();
                    }
                }
            }
        }
        return oD.compareTo(thiD);
    }
}