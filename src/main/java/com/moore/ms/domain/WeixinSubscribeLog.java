package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Entity
@Table(name="weixin_subscribe_log",schema = "moore_core")
public class WeixinSubscribeLog {

    @Id
    @SequenceGenerator(name = "weixin_subscribe_log_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_subscribe_log_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_subscribe_log_id_seq")
    private Long id;

    private Short eventType;

    private String openid;

    private Date createTime;

    private Long userId;

    private String channel;

    private String utmSource;

    private String utmKey;

    private String wxServiceName;

    private String utmSourceNew;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;
    private String utmMedium;

    public WeixinSubscribeLog(Short eventType, String openid, Date createTime, String channel, String utmSource, String utmKey, String wxServiceName) {
        this.eventType = eventType;
        this.openid = openid;
        this.createTime = createTime;
        this.channel = channel;
        this.utmSource = utmSource;
        this.utmKey = utmKey;
        this.wxServiceName = wxServiceName;
    }

    public WeixinSubscribeLog() {
    }

    public WeixinSubscribeLog(Short eventType, String openid, Date date, String wxServiceName) {
        this.eventType = eventType;
        this.openid = openid;
        this.createTime = date;
        this.wxServiceName =wxServiceName;
    }

    public WeixinSubscribeLog(Short eventType, String openid, Date createTime, String channel, String utmSource, String utmKey, String wxServiceName, String utmSourceNew, String utmMedium, String utmCampaign, String utmContent, String utmTerm) {
        this.eventType = eventType;
        this.openid = openid;
        this.createTime = createTime;
        this.channel = channel;
        this.utmSource = utmSource;
        this.utmKey = utmKey;
        this.wxServiceName = wxServiceName;
        this.utmSourceNew = utmSourceNew;
        this.utmMedium = utmMedium;
        this.utmCampaign = utmCampaign;
        this.utmContent = utmContent;
        this.utmTerm = utmTerm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getEventType() {
        return eventType;
    }

    public void setEventType(Short eventType) {
        this.eventType = eventType;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public String getWxServiceName() {
        return wxServiceName;
    }

    public void setWxServiceName(String wxServiceName) {
        this.wxServiceName = wxServiceName;
    }

    public String getUtmSourceNew() {
        return utmSourceNew;
    }

    public void setUtmSourceNew(String utmSourceNew) {
        this.utmSourceNew = utmSourceNew;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }
}
