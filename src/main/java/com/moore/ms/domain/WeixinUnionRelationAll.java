package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_unionid_relation_all",schema = "moore_core")
public class WeixinUnionRelationAll {
    @Id
    @SequenceGenerator(name = "weixin_unionid_relation_all_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_unionid_relation_all_id_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY ,generator = "weixin_unionid_relation_all_id_seq")
    private Long id;
    private String mooreeliteOpenid;
    private String icbankOpenid;
    private String mooreliveOpenid;
    private String moorenewsOpenid;
    private String unionId;
    private Short subscribe;
    private String nickname;
    private String sex;
    private String city;
    private String country;
    private String province;
    private String language;
    private String headimgurl;
    private Date subscribeTime;
    private String remark;
    private Integer groupid;
    private String tagidList;
    private String subscribeScene;
    private String mpwOpenid;
    private Date mooreliveSubscribeTime;
    private Date mooreeliteSubscribeTime;
    private Date icbankSubscribeTime;
    private Date moorenewsSubscribeTime;
    private Date mpwSubscribeTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMooreeliteOpenid() {
        return mooreeliteOpenid;
    }

    public void setMooreeliteOpenid(String mooreeliteOpenid) {
        this.mooreeliteOpenid = mooreeliteOpenid;
    }

    public String getIcbankOpenid() {
        return icbankOpenid;
    }

    public void setIcbankOpenid(String icbankOpenid) {
        this.icbankOpenid = icbankOpenid;
    }

    public String getMooreliveOpenid() {
        return mooreliveOpenid;
    }

    public void setMooreliveOpenid(String mooreliveOpenid) {
        this.mooreliveOpenid = mooreliveOpenid;
    }

    public String getMoorenewsOpenid() {
        return moorenewsOpenid;
    }

    public void setMoorenewsOpenid(String moorenewsOpenid) {
        this.moorenewsOpenid = moorenewsOpenid;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public Short getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Short subscribe) {
        this.subscribe = subscribe;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getTagidList() {
        return tagidList;
    }

    public void setTagidList(String tagidList) {
        this.tagidList = tagidList;
    }

    public String getSubscribeScene() {
        return subscribeScene;
    }

    public void setSubscribeScene(String subscribeScene) {
        this.subscribeScene = subscribeScene;
    }

    public String getMpwOpenid() {
        return mpwOpenid;
    }

    public void setMpwOpenid(String mpwOpenid) {
        this.mpwOpenid = mpwOpenid;
    }

    public Date getMooreliveSubscribeTime() {
        return mooreliveSubscribeTime;
    }

    public void setMooreliveSubscribeTime(Date mooreliveSubscribeTime) {
        this.mooreliveSubscribeTime = mooreliveSubscribeTime;
    }

    public Date getMooreeliteSubscribeTime() {
        return mooreeliteSubscribeTime;
    }

    public void setMooreeliteSubscribeTime(Date mooreeliteSubscribeTime) {
        this.mooreeliteSubscribeTime = mooreeliteSubscribeTime;
    }

    public Date getIcbankSubscribeTime() {
        return icbankSubscribeTime;
    }

    public void setIcbankSubscribeTime(Date icbankSubscribeTime) {
        this.icbankSubscribeTime = icbankSubscribeTime;
    }

    public Date getMoorenewsSubscribeTime() {
        return moorenewsSubscribeTime;
    }

    public void setMoorenewsSubscribeTime(Date moorenewsSubscribeTime) {
        this.moorenewsSubscribeTime = moorenewsSubscribeTime;
    }

    public Date getMpwSubscribeTime() {
        return mpwSubscribeTime;
    }

    public void setMpwSubscribeTime(Date mpwSubscribeTime) {
        this.mpwSubscribeTime = mpwSubscribeTime;
    }
}
