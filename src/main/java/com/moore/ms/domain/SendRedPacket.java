package com.moore.ms.domain;

import com.moore.ms.common.utils.*;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by 汤云飞 on 2016/11/8.
 */
public class SendRedPacket {

    private String MCH_URI = "https://api.mch.weixin.qq.com";

    private Header xmlHeader = new BasicHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_XML.toString());

    private String mchKey;

    private String certFilePath;

    private String mchId;

    public String getMCH_URI() {
        return MCH_URI;
    }

    public void setMCH_URI(String MCH_URI) {
        this.MCH_URI = MCH_URI;
    }

    public Header getXmlHeader() {
        return xmlHeader;
    }

    public void setXmlHeader(Header xmlHeader) {
        this.xmlHeader = xmlHeader;
    }

    public String getMchKey() {
        return mchKey;
    }

    public void setMchKey(String mchKey) {
        this.mchKey = mchKey;
    }

    public String getCertFilePath() {
        return certFilePath;
    }

    public void setCertFilePath(String certFilePath) {
        this.certFilePath = certFilePath;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    /**
     * 现金红包
     *
     * 微信红包发送规则
     * 1. 发送频率规则
     　	 *	每分钟发送红包数量不得超过1800个；
     　	 *	北京时间0：00-8：00不触发红包赠送；（如果以上规则不满足您的需求，请发邮件至wxhongbao@tencent.com获取升级指引）
     *	2. 红包规则
     *	单个红包金额介于[1.00元，200.00元]之间；
     *	同一个红包只能发送给一个用户；（如果以上规则不满足您的需求，请发邮件至wxhongbao@tencent.com获取升级指引）
     * @param redPacketRequest
     * @return
     */
    public RedPacketResponse send(RedPacketRequest redPacketRequest){
        Map<String,String> map = WXMapUtil.objectToMap( redPacketRequest);
        String sign = SignatureUtil.generateSign(map, mchKey);
        redPacketRequest.setSign(sign);
        String secapiPayRefundXML = XMLConverUtil.convertToXML( redPacketRequest);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(xmlHeader)
                .setUri(MCH_URI + "/mmpaymkttransfers/sendredpack")
                .setEntity(new StringEntity(secapiPayRefundXML, Charset.forName("utf-8")))
                .build();
        LocalHttpClient.initMchKeyStore(mchId, certFilePath);
        return LocalHttpClient.keyStoreExecuteXmlResult(redPacketRequest.getMch_id(),httpUriRequest,RedPacketResponse.class);
    }
}
