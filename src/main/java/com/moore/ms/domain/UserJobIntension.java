package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="user_job_intension",schema = "moore_core")
public class UserJobIntension{

    @Id
    @SequenceGenerator(name = "user_job_intension_id_seq",schema = "moore_core" ,sequenceName = "moore_core.user_job_intension_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "user_job_intension_id_seq")
    private Long id;

    private Long userId;

    private String expectZipcode;

    private String expectCityName;

    private Integer industryChain;

    private Long oneFunction;

    private Long function;

    private Integer rank;

    private Float currentSalary;

    private Date createTime;

    private Date modifyTime;

    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;

    private String lastUtmSource;
    private String lastUtmMedium;
    private String lastUtmCampaign;
    private String lastUtmContent;
    private String lastUtmTerm;
    private Short channelByEmail;
    private Short channelByWechat;
    private Short channelBySms;
    private Short intention;

    private Long schoolOneFunction;

    private Float currentSalaryYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getExpectZipcode() {
        return expectZipcode;
    }

    public void setExpectZipcode(String expectZipcode) {
        this.expectZipcode = expectZipcode;
    }

    public String getExpectCityName() {
        return expectCityName;
    }

    public void setExpectCityName(String expectCityName) {
        this.expectCityName = expectCityName;
    }

    public Integer getIndustryChain() {
        return industryChain;
    }

    public void setIndustryChain(Integer industryChain) {
        this.industryChain = industryChain;
    }

    public Long getOneFunction() {
        return oneFunction;
    }

    public void setOneFunction(Long oneFunction) {
        this.oneFunction = oneFunction;
    }

    public Long getFunction() {
        return function;
    }

    public void setFunction(Long function) {
        this.function = function;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Float getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(Float currentSalary) {
        this.currentSalary = currentSalary;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    public Short getChannelByEmail() {
        return channelByEmail;
    }

    public void setChannelByEmail(Short channelByEmail) {
        this.channelByEmail = channelByEmail;
    }

    public Short getChannelByWechat() {
        return channelByWechat;
    }

    public void setChannelByWechat(Short channelByWechat) {
        this.channelByWechat = channelByWechat;
    }

    public Short getChannelBySms() {
        return channelBySms;
    }

    public void setChannelBySms(Short channelBySms) {
        this.channelBySms = channelBySms;
    }

    public Short getIntention() {
        return intention;
    }

    public void setIntention(Short intention) {
        this.intention = intention;
    }

    public Long getSchoolOneFunction() {
        return schoolOneFunction;
    }

    public void setSchoolOneFunction(Long schoolOneFunction) {
        this.schoolOneFunction = schoolOneFunction;
    }

    public Float getCurrentSalaryYear() {
        return currentSalaryYear;
    }

    public void setCurrentSalaryYear(Float currentSalaryYear) {
        this.currentSalaryYear = currentSalaryYear;
    }

    public String getLastUtmSource() {
        return lastUtmSource;
    }

    public void setLastUtmSource(String lastUtmSource) {
        this.lastUtmSource = lastUtmSource;
    }

    public String getLastUtmMedium() {
        return lastUtmMedium;
    }

    public void setLastUtmMedium(String lastUtmMedium) {
        this.lastUtmMedium = lastUtmMedium;
    }

    public String getLastUtmCampaign() {
        return lastUtmCampaign;
    }

    public void setLastUtmCampaign(String lastUtmCampaign) {
        this.lastUtmCampaign = lastUtmCampaign;
    }

    public String getLastUtmContent() {
        return lastUtmContent;
    }

    public void setLastUtmContent(String lastUtmContent) {
        this.lastUtmContent = lastUtmContent;
    }

    public String getLastUtmTerm() {
        return lastUtmTerm;
    }

    public void setLastUtmTerm(String lastUtmTerm) {
        this.lastUtmTerm = lastUtmTerm;
    }


}