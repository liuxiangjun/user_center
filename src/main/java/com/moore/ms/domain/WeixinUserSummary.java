package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weixin_user_summary",schema = "moore_core")
public class WeixinUserSummary {
    @Id
    @SequenceGenerator(name = "weixin_user_summary_id_seq",schema = "moore_core" ,sequenceName = "moore_core.weixin_user_summary_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "weixin_user_summary_id_seq")
    private Long id;

    private Short type;

    @Temporal(TemporalType.DATE)
    private Date refDate;

    private Short userSource;

    private Integer newUser;

    private Integer cancelUser;

    public WeixinUserSummary() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Short getUserSource() {
        return userSource;
    }

    public void setUserSource(Short userSource) {
        this.userSource = userSource;
    }

    public Integer getNewUser() {
        return newUser;
    }

    public void setNewUser(Integer newUser) {
        this.newUser = newUser;
    }

    public Integer getCancelUser() {
        return cancelUser;
    }

    public void setCancelUser(Integer cancelUser) {
        this.cancelUser = cancelUser;
    }

}