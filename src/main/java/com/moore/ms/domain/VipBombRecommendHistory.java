package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="vip_bomb_recommend_history",schema = "moore_core")
public class VipBombRecommendHistory {

    @Id
    @SequenceGenerator(name = "vip_bomb_recommend_history_id_seq",schema = "moore_core" ,sequenceName = "moore_core.vip_bomb_recommend_history_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "vip_bomb_recommend_history_id_seq")
    private Long id;

    private String recommendBatchId;

    private Long engineUserId;

    private Long mooreUserId;

    private Integer feedbackStatus;

    private Date createTime;

    private Long talentLibraryId;

    private String airId;

    private Long devlierId;

    private Date modifyTime;

    private Long companyJobId;

    private Long subaccountId;

    private Long companyId;

    private Short isRead;

    private Short isClick;

    private Short isDeliver;

    private Short isDeliverSuccess;

    private String jobTitle;

    private String userMobile;

    private String userEmail;

    private String currentCompanyName;

    private String currentJobTitle;

    private Short sex;

    private String rejectReason;

    private Short workingYear;

    private String school;

    private String fieldOfStudy;

    private Short education;

    private String city;

    private String zipcode;

    private String fullname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecommendBatchId() {
        return recommendBatchId;
    }

    public void setRecommendBatchId(String recommendBatchId) {
        this.recommendBatchId = recommendBatchId;
    }

    public Long getEngineUserId() {
        return engineUserId;
    }

    public void setEngineUserId(Long engineUserId) {
        this.engineUserId = engineUserId;
    }

    public Long getMooreUserId() {
        return mooreUserId;
    }

    public void setMooreUserId(Long mooreUserId) {
        this.mooreUserId = mooreUserId;
    }

    public Integer getFeedbackStatus() {
        return feedbackStatus;
    }

    public void setFeedbackStatus(Integer feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getTalentLibraryId() {
        return talentLibraryId;
    }

    public void setTalentLibraryId(Long talentLibraryId) {
        this.talentLibraryId = talentLibraryId;
    }

    public String getAirId() {
        return airId;
    }

    public void setAirId(String airId) {
        this.airId = airId;
    }

    public Long getDevlierId() {
        return devlierId;
    }

    public void setDevlierId(Long devlierId) {
        this.devlierId = devlierId;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getCompanyJobId() {
        return companyJobId;
    }

    public void setCompanyJobId(Long companyJobId) {
        this.companyJobId = companyJobId;
    }

    public Long getSubaccountId() {
        return subaccountId;
    }

    public void setSubaccountId(Long subaccountId) {
        this.subaccountId = subaccountId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Short getIsRead() {
        return isRead;
    }

    public void setIsRead(Short isRead) {
        this.isRead = isRead;
    }

    public Short getIsClick() {
        return isClick;
    }

    public void setIsClick(Short isClick) {
        this.isClick = isClick;
    }

    public Short getIsDeliver() {
        return isDeliver;
    }

    public void setIsDeliver(Short isDeliver) {
        this.isDeliver = isDeliver;
    }

    public Short getIsDeliverSuccess() {
        return isDeliverSuccess;
    }

    public void setIsDeliverSuccess(Short isDeliverSuccess) {
        this.isDeliverSuccess = isDeliverSuccess;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCurrentCompanyName() {
        return currentCompanyName;
    }

    public void setCurrentCompanyName(String currentCompanyName) {
        this.currentCompanyName = currentCompanyName;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Short getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Short workingYear) {
        this.workingYear = workingYear;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Short getEducation() {
        return education;
    }

    public void setEducation(Short education) {
        this.education = education;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        return "VipBombRecommendHistory{" +
                "id=" + id +
                ", recommendBatchId='" + recommendBatchId + '\'' +
                ", engineUserId=" + engineUserId +
                ", mooreUserId=" + mooreUserId +
                ", feedbackStatus=" + feedbackStatus +
                ", createTime=" + createTime +
                ", talentLibraryId=" + talentLibraryId +
                ", airId='" + airId + '\'' +
                ", devlierId=" + devlierId +
                ", modifyTime=" + modifyTime +
                ", companyJobId=" + companyJobId +
                ", subaccountId=" + subaccountId +
                ", companyId=" + companyId +
                ", isRead=" + isRead +
                ", isClick=" + isClick +
                ", isDeliver=" + isDeliver +
                ", isDeliverSuccess=" + isDeliverSuccess +
                ", jobTitle='" + jobTitle + '\'' +
                ", userMobile='" + userMobile + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", currentCompanyName='" + currentCompanyName + '\'' +
                ", currentJobTitle='" + currentJobTitle + '\'' +
                ", sex=" + sex +
                ", rejectReason='" + rejectReason + '\'' +
                ", workingYear=" + workingYear +
                ", school='" + school + '\'' +
                ", fieldOfStudy='" + fieldOfStudy + '\'' +
                ", education=" + education +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", fullname='" + fullname + '\'' +
                '}';
    }
}