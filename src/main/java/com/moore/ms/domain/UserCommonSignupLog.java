package com.moore.ms.domain;

import javax.persistence.*;

/**
 * Created by 汤云飞 on 2016/12/21.
 */
@Entity
@Table(name="user_common_signup_log",schema = "moore_core")
public class UserCommonSignupLog {

    @Id
    @SequenceGenerator(name = "user_common_signup_log_id_seq", schema = "moore_core", sequenceName = "moore_core.user_common_signup_log_id_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "user_common_signup_log_id_seq")
    private Long id;

    private Long userId;

    private String openId;

    private Long commonSignupId;

    private String dtxOpenId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getCommonSignupId() {
        return commonSignupId;
    }

    public void setCommonSignupId(Long commonSignupId) {
        this.commonSignupId = commonSignupId;
    }

    public String getDtxOpenId() {
        return dtxOpenId;
    }

    public void setDtxOpenId(String dtxOpenId) {
        this.dtxOpenId = dtxOpenId;
    }
}


