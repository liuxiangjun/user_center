package com.moore.ms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="email_validate",schema = "moore_core")
public class EmailValidate {

    @Id
    @SequenceGenerator(name = "email_validate_id_seq",schema = "moore_core" ,sequenceName = "moore_core.email_validate_id_seq")
    @GeneratedValue(strategy =GenerationType.IDENTITY ,generator = "email_validate_id_seq")
    private Long id;

    private Long entityId;

    private String code;

    private Short businessType;

    private Short userType;

    private Date createTime;

    private Date expireTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Short getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Short businessType) {
        this.businessType = businessType;
    }

    public Short getUserType() {
        return userType;
    }

    public void setUserType(Short userType) {
        this.userType = userType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }
}