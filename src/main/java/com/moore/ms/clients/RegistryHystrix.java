package com.moore.ms.clients;

import com.moore.ms.clients.params.RegisterHttpParam;
import com.moore.ms.clients.response.ResponseStatus;
import com.moore.ms.domain.User;
import org.springframework.stereotype.Component;


/**
 * Created by 汤云飞 on 2016/8/19.
 */
@Component
public class RegistryHystrix implements RegistryClient {



    @Override
    public ResponseStatus<User> wxRegister(RegisterHttpParam param) {
        return ResponseStatus.ERROR;
    }



}
