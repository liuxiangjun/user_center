package com.moore.ms.clients.response;

import java.io.Serializable;

public class ResponseStatus<T> implements Serializable {
    
    private static final long serialVersionUID = 8928141281315480770L;
    private int resultCode;
    private String resultMessage;
    private T data;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseStatus() {
        super();
    }

    public ResponseStatus(ResponseStatus reponseStatus, T data) {
        super();
        this.resultCode = reponseStatus.getResultCode();
        this.resultMessage = reponseStatus.getResultMessage();
        this.data = data;
    }

    public ResponseStatus(ResponseStatus reponseStatus) {
        super();
        this.resultCode = reponseStatus.getResultCode();
        this.resultMessage = reponseStatus.getResultMessage();
    }

    public ResponseStatus(int resultCode, String message) {
        super();
        this.resultCode = resultCode;
        this.resultMessage = message;
    }

    public ResponseStatus(int resultCode, String message, T data) {
        super();
        this.resultCode = resultCode;
        this.resultMessage = message;
        this.data = data;
    }

    public ResponseStatus(T data){
        super();
        this.resultCode = 1;
        this.resultMessage = "成功";
        this.data = data;
    }

    public ResponseStatus(ResponseStatus reponseStatus, T data, int total) {
        super();
        this.resultCode = reponseStatus.getResultCode();
        this.data = data;
    }

    // 统一返回公用编码
    public static final ResponseStatus SUCCESS = new ResponseStatus(200, "成功");
    public static final ResponseStatus ERROR = new ResponseStatus(10000000, "服务异常,请稍后重试");

    // 已定义返回编码
    public static final ResponseStatus REQUEST_PARAM_ERROR = new ResponseStatus(10000027, "请求参数异常");

    // 公用编码以11开头
    public static final ResponseStatus REQUEST_PARAM_EMPTY = new ResponseStatus(11000000, "请求参数为空");

    public static final ResponseStatus EMIAL_REGISTERED = new ResponseStatus(11000001, "邮箱已被注册");

    public static final ResponseStatus PHONE_REGISTERED = new ResponseStatus(11000002, "手机已被注册");

    public static final ResponseStatus PHONE_CODE_EXPIRED = new ResponseStatus(11000003, "手机验证码已过期");
}

