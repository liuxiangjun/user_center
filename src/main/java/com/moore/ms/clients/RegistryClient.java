package com.moore.ms.clients;

import com.moore.ms.clients.params.RegisterHttpParam;
import com.moore.ms.clients.response.ResponseStatus;
import com.moore.ms.config.FeignConfig;
import com.moore.ms.domain.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "registry-service",configuration = FeignConfig.class,fallback = RegistryHystrix.class)
public interface RegistryClient {



    @RequestMapping(method = RequestMethod.POST, value = "/register/user/wx")
    ResponseStatus<User> wxRegister(RegisterHttpParam param);




}