package com.moore.ms.clients;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpClientUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);
    /**
     *  POST方式
     * @param url
     * @param transJson
     * @return
     */
    public static void postMethod(String url,String transJson){

        try {
            HttpClient client = new HttpClient();
            HttpMethod method = new PostMethod(url);

            RequestEntity se = new StringRequestEntity(transJson, "application/json", "UTF-8");
            ((PostMethod) method).setRequestEntity(se);

            //使用系统提供的默认的恢复策略
            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
            //设置超时的时间
            method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 10000);
            client.executeMethod(method);
            method.getResponseBodyAsString();
            method.releaseConnection();
        } catch (IOException e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
    }

    /**
     *  GET方式
     * @param url
     * @return
     */
    public static String getMethod(String url){

        try {
            HttpClient client = new HttpClient();
            HttpMethod method = new GetMethod(url);

            //使用系统提供的默认的恢复策略
            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
            //设置超时的时间
            method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 10000);
            client.executeMethod(method);
            String responseBodyAsString = method.getResponseBodyAsString();
            method.releaseConnection();
            return responseBodyAsString;
        } catch (IOException e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return null;
    }
}
