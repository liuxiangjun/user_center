package com.moore.ms.clients.params;


/**
 * Created by maqi on 16/8/2.
 */
public class RegisterHttpParam {

    public RegisterHttpParam(){}

    //注册类型(如:用户邮箱注册、用户手机注册、一键投递简历注册、HR注册等等)
    private RegisterType registerType;

    //个人邮箱注册的参数
    private String email;
    private String password;
    private String inviteCode;
    private String ip;
    private String validUrl;
    private String importId;
    private String source;
    private String linkedinId;

    //手机号注册的参数(短信验证码)
    private String registerMobile;
    private Short isRegisterPhoneValidate;
    private String code;

    //一键投递用户注册的参数
    private String fullname;
    private Integer sex;
    private Integer education;
    private Integer workingYear;
    private String currentZipcode;
    private String currentCityName;
    private String uploadedResume;

    //HR注册时的参数
    private String title;
    private String contact;
    private String mobilePhone;

    //wx来源
    private String utmKey;

    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmContent;
    private String utmTerm;


    public RegisterType getRegisterType() {
        return registerType;
    }

    public void setRegisterType(RegisterType registerType) {
        this.registerType = registerType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getValidUrl() {
        return validUrl;
    }

    public void setValidUrl(String validUrl) {
        this.validUrl = validUrl;
    }

    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getEducation() {
        return education;
    }

    public void setEducation(Integer education) {
        this.education = education;
    }

    public Integer getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Integer workingYear) {
        this.workingYear = workingYear;
    }

    public String getCurrentZipcode() {
        return currentZipcode;
    }

    public void setCurrentZipcode(String currentZipcode) {
        this.currentZipcode = currentZipcode;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public String getUploadedResume() {
        return uploadedResume;
    }

    public void setUploadedResume(String uploadedResume) {
        this.uploadedResume = uploadedResume;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getRegisterMobile() {
        return registerMobile;
    }

    public void setRegisterMobile(String registerMobile) {
        this.registerMobile = registerMobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getUtmKey() {
        return utmKey;
    }

    public void setUtmKey(String utmKey) {
        this.utmKey = utmKey;
    }

    public Short getIsRegisterPhoneValidate() {
        return isRegisterPhoneValidate;
    }

    public void setIsRegisterPhoneValidate(Short isRegisterPhoneValidate) {
        this.isRegisterPhoneValidate = isRegisterPhoneValidate;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return utmContent;
    }

    public void setUtmContent(String utmContent) {
        this.utmContent = utmContent;
    }

    public String getUtmTerm() {
        return utmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.utmTerm = utmTerm;
    }

    @Override
    public String toString() {
        return "RegisterParamForService{" +
                "registerType=" + registerType +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", inviteCode='" + inviteCode + '\'' +
                ", ip='" + ip + '\'' +
                ", validUrl='" + validUrl + '\'' +
                ", importId='" + importId + '\'' +
                ", source='" + source + '\'' +
                ", linkedinId='" + linkedinId + '\'' +
                ", registerMobile='" + registerMobile + '\'' +
                ", code='" + code + '\'' +
                ", fullname='" + fullname + '\'' +
                ", sex='" + sex + '\'' +
                ", education='" + education + '\'' +
                ", workingYear='" + workingYear + '\'' +
                ", currentZipcode='" + currentZipcode + '\'' +
                ", currentCityName='" + currentCityName + '\'' +
                ", uploadedResume='" + uploadedResume + '\'' +
                ", title='" + title + '\'' +
                ", contact='" + contact + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                '}';
    }
}
