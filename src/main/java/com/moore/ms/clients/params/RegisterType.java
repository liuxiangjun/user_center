package com.moore.ms.clients.params;

/**
 * Created by maqi on 16/8/2.
 */
public enum RegisterType {
    //用户邮箱注册
    USER_EMAIL_REGISTER("user_register_by_email",1),

    //用户手机号注册
    USER_PHONE_REGISTER("user_register_by_phone",2),

    //一键投递注册
    ONEKEY_REGISTER("user_register_by_onekey",3),

    //linkedin 用户注册
    LINKEDIN_REGISTER("user_register_by_linkedin",4),

    // moore-wechat 用户注册
    WECHAT_REGISTER("user_register_by_wechat",5),

    //wx 公众号 用户注册
    WX_REGISTER("user_register_by_wx",6),

    //HR注册
    COMPAY_REGISTER("company_register",20);

    private String name;

    private int index;

    private RegisterType(String name, int index){
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static String getName(int index) {
        for (RegisterType c : RegisterType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "RegisterType{" +
                "name='" + name + '\'' +
                ", index=" + index +
                '}';
    }
}
