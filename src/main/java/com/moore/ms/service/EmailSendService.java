package com.moore.ms.service;

import cn.submail.sdk.config.AppConfig;
import com.google.common.collect.Lists;
import com.moore.ms.email.MAILSend;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by 汤云飞 on 2016/6/25.
 */
public class EmailSendService {

    private static Logger logger = LoggerFactory.getLogger(EmailSendService.class);

    private  String mailTemplateUrl;

    private TaskExecutor taskExecutor;

    private TemplateEngine templateEngine;

    private String from;

    private AppConfig appConfig;

    private String active;

    private List<NameValuePair> sysParams = Lists.newArrayList();

    public EmailSendService(String active, String mailTemplateUrl, String submailApiId, String submailApiSignatureOrSendcloudApiKey, String mailFrom, TemplateEngine templateEngine, TaskExecutor taskExecutor) {

        from = mailFrom;
        this.mailTemplateUrl = mailTemplateUrl;
        this.templateEngine = templateEngine;
        this.taskExecutor = taskExecutor;
        this.active = active;
        if(active.equals("submail")){
            appConfig = new AppConfig();
            appConfig.setAppId(submailApiId);
            appConfig.setAppKey(submailApiSignatureOrSendcloudApiKey);
            appConfig.setSignType(AppConfig.TYPE_MD5);
        } else {
            sysParams.add(new BasicNameValuePair("api_user",submailApiId));
            sysParams.add(new BasicNameValuePair("from", this.from));
            sysParams.add(new BasicNameValuePair("api_key", submailApiSignatureOrSendcloudApiKey));
        }

    }

    public String buildEmailContentByTemplate(Map<String,Object> map,String templateName){
        map.put("serviceSupportEmail","support@service.mooreelite.com");
        Context context = new Context();
        if (!CollectionUtils.isEmpty(map)){
            context.setVariables(map);
        }

        StringWriter writer = new StringWriter();
        templateEngine.process(templateName, context, writer);

        return writer.toString();
    }

    public boolean send(String subject,String fromName, String html,String to,String tag) {
        if(active.equals("submail")) {
            MAILSend mailSend = new MAILSend(appConfig);
            mailSend.setHtml(html);
            mailSend.addTo(to, to);
            mailSend.setSubject(subject);
            mailSend.setSender(from, fromName);
            mailSend.addTag(tag);
            mailSend.send();
        }else{
            sendSendCloud(subject,fromName,html,to);
        }
        return true;
    }

    public String sendSubmail(String subject,String fromName, String html,String to,String tag) {
        MAILSend mailSend = new MAILSend(appConfig);
        mailSend.setHtml(html);
        mailSend.addTo(to, to);
        mailSend.setSubject(subject);
        mailSend.setSender(from, fromName);
        mailSend.addTag(tag);
        return mailSend.send();
    }

    public boolean sendSendCloud(String subject,String fromName, String html,String to){
        List<NameValuePair>  params = Lists.newArrayList();
        params.addAll(sysParams);
        params.add(new BasicNameValuePair("subject", subject));
        params.add(new BasicNameValuePair("fromname", fromName));
        params.add(new BasicNameValuePair("to", to));
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(mailTemplateUrl);
        params.add(new BasicNameValuePair("html", html));
        boolean success = false;
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) { // 正常返回
                success = true;
            } else {
                logger.error("send fail:"+ EntityUtils.toString(response.getEntity()));
            }

        } catch (IOException e) {
            logger.error("send fail:"+ e);
        }
        httpPost.releaseConnection();
        return success;
    }

    public void subMailSendAsyn(Map<String, Object> params , String templateName, String subject, String fromName, String to,String tag) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                String content = buildEmailContentByTemplate(params,templateName);
                send(subject,fromName,content,to,tag);
            }
        });

    }

}
