package com.moore.ms.service;

import com.moore.ms.domain.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Service
public class FeedbackService {

    @Autowired
    private com.moore.ms.respository.FeedBackRepository FeedBackRepository;

    public void submitWxUserMessage(String openId, String content) {
        Feedback feedback = new Feedback();
        feedback.setOpenId(openId);
        feedback.setContent(content);
        feedback.setCreateTime(new Date());
        feedback.setSource("wx");
        FeedBackRepository.save(feedback);

    }

    public void addFeedBack(String email, String content,String source) {

        Feedback feedback = new Feedback();
        feedback.setContent(content);
        feedback.setCreateTime(new Date());
        feedback.setSource(source);
        feedback.setEmail(email);
        FeedBackRepository.save(feedback);
    }
    public void addFeedBack(String email, String content,String source,Long userId,String openId) {

        Feedback feedback = new Feedback();
        feedback.setContent(content);
        feedback.setCreateTime(new Date());
        feedback.setSource(source);
        feedback.setEmail(email);
        feedback.setOpenId(openId);
        feedback.setUserId(userId);
        char type= 0;
        feedback.setType(new Character(type));
        FeedBackRepository.save(feedback);
    }
    public void addFeedBack(String email, String content,String source,Long userId,String openId,String type,String mobile) {

        Feedback feedback = new Feedback();
        feedback.setContent(content);
        feedback.setCreateTime(new Date());
        feedback.setSource(source);
        feedback.setEmail(email);
        feedback.setOpenId(openId);
        feedback.setUserId(userId);
        feedback.setType(type.charAt(0));
        feedback.setMobile(mobile);
        FeedBackRepository.save(feedback);
    }

}
