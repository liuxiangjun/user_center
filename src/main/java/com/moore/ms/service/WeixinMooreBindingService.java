package com.moore.ms.service;

import com.moore.ms.domain.WeixinMooreBinding;
import com.moore.ms.domain.WeixinOpenIdRelation;
import com.moore.ms.respository.WeixinMooreBindingRepository;
import com.moore.ms.respository.WeixinOpenIdRelationRepository;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Service
public class WeixinMooreBindingService {
    private static Logger log = LoggerFactory.getLogger(WeixinMooreBindingService.class);

    @Autowired private WxMpService wxMpService;
    @Autowired private WeixinMooreBindingRepository weixinMooreBindingRepository;
    @Autowired private WeixinOpenIdRelationRepository weixinOpenIdRelationRepository;
    @Autowired private MTLogService mtLogService;

    public WeixinMooreBinding findByOpenId(String openId) {

        List<WeixinMooreBinding> binding = weixinMooreBindingRepository.findByOpenIdAndTypeAndIsDelete(openId);
        if (binding.size() > 1) {
            log.info("绑定关系存在多条，openId为" + openId);
            WeixinMooreBinding weixinMooreBinding = weixinMooreBindingRepository.findFirstByOpenIdAndTypeAndIsDeleteOrderByCreateTimeDesc(openId);
            return weixinMooreBinding;
        }
        if (binding.size() == 0){
            return null;
        }
        return binding.get(0);
    }

    public List<WeixinMooreBinding> findByUnionId(String unionId) {
        return weixinMooreBindingRepository.findByUnionId(unionId);
    }

    public List<WeixinMooreBinding> findByUnionIdNotType(String unionId) {
        return weixinMooreBindingRepository.findByUnionIdAndTypeNot(unionId, 1);
    }

    public Long findUserIdByOpenId(String openId) {
        WeixinMooreBinding binding = weixinMooreBindingRepository.findByOpenId(openId);
        if (binding != null ){
            return binding.getUserId();
        }
        return null;
    }

    public void save(String openId, Long userId, String email) {
        WeixinMooreBinding bind = findByOpenId(openId);
        if (bind != null) return;
        try {
            WxMpUser wxMpUser = this.wxMpService.getUserService().userInfo(openId,"zh_CN");
            bind = findByUserId(userId);
            if (bind != null){
                bind.setOpenId(openId);
                bind.setUnionId(wxMpUser.getUnionId());
                bind.setEmail(email);
            }else{
                bind = new WeixinMooreBinding(openId,userId,email,wxMpUser.getUnionId(),1);
            }
            bind.setNickname(wxMpUser.getNickname());
            bind.setHeadimgurl(wxMpUser.getHeadImgUrl());
            if (bind.getIsDelete() == null) {
                bind.setIsDelete(0);
            }
            this.weixinMooreBindingRepository.save(bind);
            mtLogService.sendWeixinBindingMtLog(bind.getOpenId(), bind.getUnionId(), bind.getUserId());
//            String hideUserId = hideUserId(bind.getUserId());
//            String header = "mr_mw_wx||mooreelite_weixin||" + openId + "||" + hideUserId + "||-||-";
//            mtLogService.mtMessageCollect("subscribe,register","auto_binding","","follow_wechat","","","","","","","",header);
        } catch (WxErrorException e) {
            e.printStackTrace();
            log.error("save( " + openId +", "+ userId +", "+ email  +")：", e);
        }
    }

    public void save(WeixinMooreBinding bind ) {
        if (bind.getIsDelete() == null) {
            bind.setIsDelete(0);
        }
        this.weixinMooreBindingRepository.save(bind);
        mtLogService.sendWeixinBindingMtLog(bind.getOpenId(), bind.getUnionId(), bind.getUserId());
//        String hideUserId = hideUserId(bind.getUserId());
//        String header = "mr_mw_wx||mooreelite_weixin||" + bind.getOpenId() + "||" + hideUserId + "||-||-";
//        mtLogService.mtMessageCollect("subscribe,register","auto_binding","","follow_wechat","","","","","","","",header);
    }

    public Long getUserIdByOpenId(String openId) {
        WeixinMooreBinding binding = weixinMooreBindingRepository.findByOpenId(openId);
        if (binding != null ){
            return binding.getUserId();
        }else{
            try {
                WxMpUser wxMpUser = this.wxMpService.getUserService().userInfo(openId,"zh_CN");
                if (StringUtils.isNotBlank(wxMpUser.getUnionId())){
                    binding = findUserIdByUionId(wxMpUser.getUnionId());
                    if (binding != null && binding.getUserId() != null){
                        save(openId,binding.getUserId(),"");
                        return  binding.getUserId();
                    }
                }

            } catch (WxErrorException e) {
                e.printStackTrace();
                log.error("更新绑定信息错误getUserIdByOpenId()：", e);
            }
        }
        return null;
    }

    private WeixinMooreBinding findUserIdByUionId(String unionId){
        if(StringUtils.isBlank(unionId)) unionId = "-1";
        List<WeixinMooreBinding> bindings = this.weixinMooreBindingRepository.findByUnionId(unionId);
        if (!CollectionUtils.isEmpty(bindings)){
            return bindings.get(0);
        }
        return  null;
    }

    public WeixinMooreBinding findByUserId(Long userId) {
        if(userId == null) return null;
        WeixinMooreBinding binding = weixinMooreBindingRepository.findByUserIdAndTypeAndIsDelete(userId,1, 0);
        return binding;
    }

    public WeixinMooreBinding findByUserIdGetFirst(Long userId) {
        if(userId == null) return null;
        WeixinMooreBinding binding = weixinMooreBindingRepository.findFirstByUserIdAndTypeAndIsDeleteOrderByCreateTimeDesc(userId,1, 0);
        return binding;
    }

    public boolean findIsUserBinded(Long userId) {
        if(userId == null) return false;
        List<WeixinMooreBinding> bindings = weixinMooreBindingRepository.findByUserId(userId);
        if(bindings != null && bindings.size() > 0) return true;
        return false;
    }

    public Long initWxBindingByWxMpUserAndUionId(WxMpUser wxMpUser,String unionId) {
        if (StringUtils.isBlank(unionId)){
            return null;
        }
        WeixinMooreBinding bind = findUserIdByUionId(unionId);
        if (bind != null){
            WeixinMooreBinding  wxbinding = new WeixinMooreBinding(wxMpUser.getOpenId(),bind.getUserId(),bind.getEmail(),unionId,1);
            wxbinding.setNickname(wxMpUser.getNickname());
            wxbinding.setHeadimgurl(wxMpUser.getHeadImgUrl());
            this.weixinMooreBindingRepository.save(wxbinding);
            mtLogService.sendWeixinBindingMtLog(wxbinding.getOpenId(), wxbinding.getUnionId(), wxbinding.getUserId());
            return bind.getUserId();
        }

        return null;
    }

    public Map checkUserInfo(String openId){
        Map result = new HashMap();
        try{
            WeixinMooreBinding binding = findByOpenId(openId);
            if(binding != null){
                result.put("isNeedAuthen",false);
                result.put("unionId",binding.getUnionId());
                return result;
            }
            List<WeixinOpenIdRelation> relations = weixinOpenIdRelationRepository.findByMooreeliteOpenid(openId);
            for(WeixinOpenIdRelation relation:relations){
                if(StringUtils.isNotBlank(relation.getUnionId())){
                    result.put("isNeedAuthen",false);
                    result.put("unionId",relation.getUnionId());
                    WeixinMooreBinding bind = findUserIdByUionId(relation.getUnionId());
                    if(bind != null){
                        WeixinMooreBinding moorBinding = new WeixinMooreBinding();
                        moorBinding.setOpenId(openId);
                        moorBinding.setCreateTime(new Date());
                        moorBinding.setUnionId(bind.getUnionId());
                        moorBinding.setUserId(bind.getUserId());
                        moorBinding.setEmail(bind.getEmail());
                        moorBinding.setHeadimgurl(bind.getHeadimgurl());
                        moorBinding.setNickname(bind.getNickname());
                        moorBinding.setType(1);
                        save(moorBinding);
                    }
                    return result;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        result.put("isNeedAuthen",true);
        return result;
    }

    public List<WeixinMooreBinding> findByUnionIsNull(){
        return weixinMooreBindingRepository.findByTypeAndUnionIdIsNull(1);
    }

    public WeixinMooreBinding findByOpenIdOrUnionId(String openId){
        WeixinMooreBinding bind = this.findByOpenId(openId);
        if (bind != null) return bind;
        List<WeixinOpenIdRelation> relations = weixinOpenIdRelationRepository.findByMooreeliteOpenid(openId);
        if(relations != null && relations.size() > 0){
            String unionId = null;
            for(WeixinOpenIdRelation relation:relations){
                if(StringUtils.isNotBlank(relation.getUnionId())) unionId = relation.getUnionId();
            }
            if(StringUtils.isNotBlank(unionId)) {
                bind = findUserIdByUionId(unionId);
                if(bind != null){
                    Long userId = bind.getUserId();
                    if(findByUserId(userId) != null) return null;
                    WeixinMooreBinding liteBinding = new WeixinMooreBinding(openId,bind.getUserId(),bind.getEmail(),bind.getUnionId(),1);
                    this.save(liteBinding);
                    return  liteBinding;
                }
            }
        }
        return bind;
    }

    public void saveWithUnionId(String openId, Long userId, String email) {
        WeixinMooreBinding bind = findByOpenId(openId);
        if (bind != null) return;
        List<WeixinOpenIdRelation> relations = weixinOpenIdRelationRepository.findByMooreeliteOpenid(openId);
        String unionId = null;
        if(relations != null && relations.size() > 0){
            for(WeixinOpenIdRelation relation:relations){
                if(StringUtils.isNotBlank(relation.getUnionId())) unionId = relation.getUnionId();
            }
        }
        if(StringUtils.isNotBlank(unionId)) {
            WeixinMooreBinding liteBinding = new WeixinMooreBinding(openId,userId,email,unionId,1);
            this.save(liteBinding);
        }
    }

    public void saveWithUnionId_careerReport(String openId, Long userId, String email) {
        WeixinMooreBinding bind = findByOpenId(openId);
        if (bind != null) return;
        List<WeixinOpenIdRelation> relations = weixinOpenIdRelationRepository.findByMooreeliteOpenid(openId);
        String unionId = null;
        if(relations != null && relations.size() > 0){
            for(WeixinOpenIdRelation relation:relations){
                if(StringUtils.isNotBlank(relation.getUnionId())) unionId = relation.getUnionId();
            }
        }
        if(StringUtils.isNotBlank(unionId)) {
            WeixinMooreBinding liteBinding = new WeixinMooreBinding(openId,userId,email,unionId,1);
            this.save(liteBinding);
        } else {
            save(openId, userId, email);
        }
    }

//    private String hideUserId(Long userId){
//        Hashids hashids = new Hashids("Moore.135",16,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
//        String hash = hashids.encode(userId);
//        return hash;
//    }
}
