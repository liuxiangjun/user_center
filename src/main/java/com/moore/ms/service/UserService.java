package com.moore.ms.service;

import com.google.common.collect.Lists;
import com.moore.ms.clients.RegistryClient;
import com.moore.ms.clients.params.RegisterHttpParam;
import com.moore.ms.clients.response.ResponseStatus;
import com.moore.ms.common.utils.Hashids;
import com.moore.ms.common.utils.PasswordUtils;
import com.moore.ms.domain.*;
import com.moore.ms.respository.*;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Service
public class UserService {

    @Autowired
    private WeixinMooreBindingService weixinMooreBindingService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WeixinMooreBindingRepository weixinMooreBindingRepository;
    @Autowired
    private UserProjectRepository userProjectRepository;
    @Autowired
    private UserPublicationRepository userPublicationRepository;
    @Autowired
    private UserPublishRepository userPublishRepository;
    @Autowired
    private MTLogService mtLogService;

    @Autowired
    private RegistryClient registryClient;

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    public Long wxAutoRegisterProcess(WxMpUser wxMpUser, String registerPhone,String utmKey,String ip,String utmCampaign, String utmContent, String utmMedium, String utmSource, String utmTerm) {

        String openId = wxMpUser.getOpenId();
        String unionId = wxMpUser.getUnionId();

        Long userId = weixinMooreBindingService.findUserIdByOpenId(openId);
        if(userId != null){
            return userId;
        }
        //从开放平台绑定数据
        if (userId == null){
            userId = weixinMooreBindingService.initWxBindingByWxMpUserAndUionId(wxMpUser,unionId);
        }
        if(userId != null){
            return userId;
        }
        User user = createUser("wx","111111","",registerPhone,utmKey,ip,wxMpUser.getOpenId(),new Short("1"),null,null,new Short("0"), utmCampaign, utmContent, utmMedium, utmSource, utmTerm);

        WeixinMooreBinding binding = new WeixinMooreBinding(openId,user.getId(),user.getEmail(),unionId,1);
        binding.setNickname(wxMpUser.getNickname());
        binding.setHeadimgurl(wxMpUser.getHeadImgUrl());

        this.weixinMooreBindingRepository.save(binding);
        mtLogService.sendWeixinBindingMtLog(openId, unionId, userId);
        return user.getId();
    }

    public User createUser(String source, String password, String email, String registerPhone, String utmKey, String ip, String openId, Short searchEngineIgnore, String inviteCode, String importId, Short isValidata,
                           String utmCampaign, String utmContent, String utmMedium, String utmSource, String utmTerm){



        if (StringUtils.isNotBlank(email)){
            User users = findByEmail(email.toLowerCase());
            if (users != null && users.getAutoRegister() != null && users.getAutoRegister().intValue() == 1) {
                users.setPassword(PasswordUtils.encodePassword(password));
                userRepository.save(users);
                return users;
            }
        }

        RegisterHttpParam param = new RegisterHttpParam();

        param.setPassword(password);
        param.setSource(source);
        param.setUtmKey(utmKey);
        param.setIp(ip);
        param.setInviteCode(inviteCode);
        param.setEmail(email.toLowerCase());
        param.setRegisterMobile(registerPhone);
        param.setIsRegisterPhoneValidate(isValidata);
        param.setImportId(importId);
        param.setUtmCampaign(StringUtils.isBlank(utmCampaign) ? null : utmCampaign);
        param.setUtmContent(StringUtils.isBlank(utmContent) ? null : utmContent);
        param.setUtmMedium(StringUtils.isBlank(utmMedium) ? null : utmMedium);
        param.setUtmSource(StringUtils.isBlank(utmSource) ? null : utmSource);
        param.setUtmTerm(StringUtils.isBlank(utmTerm) ? null : utmTerm);
        ResponseStatus<User> data = registryClient.wxRegister(param);
        if (data.getResultCode() != 200){
            throw  new RuntimeException("远程链接错误");
        }
        User user = data.getData();

//        用户分数计算
//        userScoreService.saveUserScore(user);

        String header = "";
        if (source.equals("wx")) {
            String hash = hideUserId(user.getId());
            header = "mr_mw_wx||mooreelite_weixin||-||" + hash + "||-||-";
        }else{
            String hash = hideUserId(user.getId());
            header = "mr_mw||browser||-||" + hash + "||-||-";
        }
        logger.info("user register mt_header: " + header);
        mtLogService.mtMessageCollect("register","","","register","","",utmCampaign,utmContent,utmMedium,utmSource,utmTerm,header);
        return user;

    }

    public User findByEmailOrPhone(String username) {

        User user = userRepository.findByEmailOrRegisterMobile(username,username);
        return user;
    }

    public User findByPhone(String phone) {
        List<User> user = userRepository.findByRegisterMobileAndIsDeleteOrderByRegisterTimeDesc(phone,new Short("0"));
        if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(user)){
            return user.get(0);
        }
        return null;
    }

    public  List<User> findByMobilePhone(String phone) {
        return userRepository.findAllByMobilePhone(phone);
    }


    public User findByEmail(String email) {
        return userRepository.findByEmailAndIsDelete(email,new Short("0"));
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public User findByUserId(Long userId) {
        return userRepository.findOne(userId);
    }

    public  final static long EXPIRES =  30 * 60 * 1000l;

    public void recordLastLoginTime(Long userId) {
        if (userId == null){
            return;
        }
        User user = userRepository.findOne(userId);
        if (user == null){
            return;
        }
        Date lastLoginTime = user.getLastLoginTime();

        long lastTime = 0l;
        long current = System.currentTimeMillis();
        if (lastLoginTime != null){
            lastTime = lastLoginTime.getTime();
        }

        if (current > lastTime + EXPIRES){
            user.setLastLoginTime(new Date());
            userRepository.save(user);
        }


    }


    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findAllByIsDelete(Short isDelete) {
         return this.userRepository.findAllByIsDelete(isDelete);
    }

    public User findByImportId(String importId) {
        return this.userRepository.findByImportId(importId);
    }


    public List<UserProject> listUserProjectByUserId(Long userId) {
        List<UserProject> list = this.userProjectRepository.findByUserId(userId);
        return list;
    }

    public UserProject findUserProjectById(Long id) {
        return this.userProjectRepository.findOne(id);
    }

    public void saveUserProject(UserProject userProject) {
        userProjectRepository.save(userProject);
    }

    public void deleteUserProjectById(Long id) {
        userProjectRepository.delete(id);
    }
    public List<UserPublication> listUserPublicationByUserId(Long userId) {
        List<UserPublication> list = this.userPublicationRepository.findByUserId(userId);
        return list;
    }

    public UserPublication findUserPublicationById(Long id) {
        return this.userPublicationRepository.findOne(id);
    }

    public void saveUserPublication(UserPublication userPublication) {
        userPublicationRepository.save(userPublication);
    }

    public void deleteUserPublicationById(Long id) {
        userPublicationRepository.delete(id);
    }

    public List<UserPublish> listUserPublishByUserId(Long userId) {
        List<UserPublish> list = this.userPublishRepository.findByUserId(userId);
        return list;
    }

    public UserPublish findUserPublishById(Long id) {
        return this.userPublishRepository.findOne(id);
    }

    public void saveUserPublish(UserPublish userPublish) {
        userPublishRepository.save(userPublish);
    }

    public void deleteUserPublishById(Long id) {
        userPublishRepository.delete(id);
    }


    public List<User> findByFastDeliverScore() {
        Sort sort = new Sort(Sort.Direction.DESC,"fastDeliverScore");
        Pageable pageable = new PageRequest(0,10,sort);
        Page<User> list = this.userRepository.findAllByIsDeleteAndFastDeliverScoreIsNotNull(new Short("0"),pageable);
        for (User user : list.getContent()) {
            if (user.getFastDeliverScore() == null){
                user.setFastDeliverScore(0);
            }

            if (StringUtils.isBlank(user.getFullname())){
                user.setFullname("未知");
            }

        }
        return list.getContent();
    }

    public User getUser(String phone){
        return userRepository.findUserByPhone(phone);
    }

    public List<User> getUserByIds(List<Long> userIds){
        return userRepository.findByIdIn(userIds);
    }

    public List<User> getUserByIdsAndFinishSchoolYearIn(List<Long> userIds){
        List<String> years = Lists.newArrayList();
        years.add("2019");
        years.add("2018");
        return userRepository.findByIdInAndFinishSchoolYearIn(userIds, years);
    }

    private String hideUserId(Long userId){
        Hashids hashids = new Hashids("Moore.135",16,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
        String hash = hashids.encode(userId);
        return hash;
    }
}
