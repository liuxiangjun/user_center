package com.moore.ms.service;

import com.google.common.collect.Maps;
import com.moore.ms.config.CommonConfig;
import com.moore.ms.domain.EmailValidate;
import com.moore.ms.domain.User;
import com.moore.ms.respository.EmailValidateRepository;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Service
public class AuthenticationService {

    @Autowired
    private WeixinMooreBindingService weixinMooreBindingService;
    @Autowired
    private EmailValidateRepository emailValidateRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CommonConfig commonConfig;

    public String processWxLogin(WxMpUser wxMpUser, Integer autoRegister,String utmKey,String ip,String utmCampaign, String utmContent, String utmMedium, String utmSource, String utmTerm) {

        String openId = wxMpUser.getOpenId();
        String unionId = wxMpUser.getUnionId();

        Long userId = weixinMooreBindingService.findUserIdByOpenId(openId);
        if(userId != null){
            return createToken(userId,openId);
        }
        //从开放平台绑定数据
        if (userId == null){
            userId = weixinMooreBindingService.initWxBindingByWxMpUserAndUionId(wxMpUser,unionId);
        }
        if(userId != null){
            return createToken(userId,openId);
        }

        if (autoRegister == 0){
            return null;
        }

        //自动注册逻辑
        userId = userService.wxAutoRegisterProcess(wxMpUser,"",utmKey,ip, utmCampaign, utmContent, utmMedium, utmSource, utmTerm);
        return createToken(userId,openId);
    }


    public String createToken(Long userId,String openId){
        //jwt设置
        JwtBuilder jwt = Jwts.builder();
        //jwt.setExpiration(DateUtil.getPerDate(new Date(), "d", 30));
        jwt.claim("userId",userId).claim("openId",openId);
        jwt.setSubject("ACCONUT_" + userId);
        String token = jwt.signWith(SignatureAlgorithm.HS256, commonConfig.getJwtSecretkey()).compact();
        //LocalAccountHelper.putAccount2Session(httpSession,vo);

        return token;
    }

    public Map<String,Object> checkEmailValidataByCode(String code) {
        Map<String,Object> data = Maps.newHashMap();
        boolean hasNotExpired = true;
        EmailValidate entity = emailValidateRepository.findByCode(code);
        if (entity == null){
            data.put("hasNotExpired",false);
            return data;
        }
        if(entity.getExpireTime() == null || entity.getExpireTime().compareTo(new Date()) < 0){
            hasNotExpired = false;
        }
        data.put("hasNotExpired",hasNotExpired);
        User user = this.userService.findByUserId(entity.getEntityId());
        data.put("email",user.getEmail());
        return data;
    }
}
