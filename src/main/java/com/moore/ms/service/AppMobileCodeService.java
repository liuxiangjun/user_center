package com.moore.ms.service;

import com.moore.ms.common.utils.DateUtil;
import com.moore.ms.domain.AppMobileCode;
import com.moore.ms.respository.AppMobileCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Service
public class AppMobileCodeService {

    @Autowired
    private AppMobileCodeRepository appMobileCodeRepository;

    public String findByPhoneAndType(String phone,Short type) {

        Sort.Direction direction = Sort.Direction.DESC;
        Pageable pageable = new PageRequest(0,1,direction,"createTime");
        Page<AppMobileCode> list = appMobileCodeRepository.findByMobileAndType(phone,type,pageable);

        if (list == null || CollectionUtils.isEmpty(list.getContent())){
            return "";
        }
        return list.getContent().get(0).getCode();

    }


    public List<AppMobileCode> getCodesByMobileToday(String mobile){
        Date afterDate = DateUtil.getDayStart();
        Date beforeDate = DateUtil.getDayEnd();
        return appMobileCodeRepository.findByMobileAndCreateTimeAfterAndCreateTimeBeforeAndStatusCode(mobile,afterDate,beforeDate,"200");
    }


}
