package com.moore.ms.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.moore.ms.config.CommonConfig;
import com.moore.ms.config.ResourcesConfig;
import com.moore.ms.vo.response.MTMessageResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

@Service
public class MTLogService {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired private CommonConfig commonConfig;
    @Autowired private TaskExecutor taskExecutor;
    @Autowired private ObjectMapper objectMapper;

    public void sendSubscribeMtLog(String openId, String unionId, String utmSource,
                                   String utmContent, String utmMedium, String utmCampaign, String utmTerm){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setConsume(3);
            requestBody.setAction("subscribe");
            requestBody.setMooreeliteOpenId(openId);
            requestBody.setUnionId(unionId);
            requestBody.setUtmCampaign(utmCampaign);
            requestBody.setUtmContent(utmContent);
            requestBody.setUtmSource(utmSource);
            requestBody.setUtmTerm(utmTerm);
            requestBody.setUtmMedium(utmMedium);
            sendMtLog(requestBody);
        });
    }

    public void sendUnsubscribeMtLog(String openId){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setConsume(4);
            requestBody.setAction("unsubscribe");
            requestBody.setMooreeliteOpenId(openId);
            sendMtLog(requestBody);
        });
    }

    public void sendWeixinBindingMtLog(String openId, String unionId, Long userId){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setConsume(15);
            requestBody.setAction("binding");
            requestBody.setMooreeliteOpenId(openId);
            requestBody.setUnionId(unionId);
            requestBody.setMooreUserId(userId);
            sendMtLog(requestBody);
        });
    }

    public void sendResumeUpdateMtLog(String url, Long id, Object param){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setConsume(9);
            requestBody.setMooreUserId(id);
            requestBody.setAction("update");
            requestBody.setRequestUrl(ResourcesConfig.WEIXIN_SERVER_API_URL + url);
            requestBody.setJsonExt(String.valueOf(param));
            sendMtLog(requestBody);
        });
    }

    public void sendCustomerJobMtLog(String url, String userId, String openId, Object param){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setConsume(10);
            requestBody.setAction("update");
            requestBody.setMooreUserId(StringUtils.isBlank(userId) ? null:Long.valueOf(userId));
            requestBody.setMooreliveOpenId(openId);
            requestBody.setRequestUrl(ResourcesConfig.WEIXIN_SERVER_API_URL + url);
            requestBody.setJsonExt(String.valueOf(param));
            sendMtLog(requestBody);
        });
    }

    public void sendUnacceptMtLog(String url, Long userId, Object param){
        taskExecutor.execute(()-> {
            RequestBody requestBody = new RequestBody();
            requestBody.setProductLine(2);
            requestBody.setMooreUserId(userId);
            requestBody.setConsume(14);
            requestBody.setAction("unaccept");
            requestBody.setRequestUrl(ResourcesConfig.WEIXIN_SERVER_API_URL + url);
            requestBody.setJsonExt(String.valueOf(param));
            sendMtLog(requestBody);
        });
    }

    public void sendSearchMtLog(String url, Long userId, String key, Object param){
        taskExecutor.execute(()->{
            RequestBody requestBody = new RequestBody();
            requestBody.setMooreUserId(userId);
            requestBody.setProductLine(2);
            requestBody.setConsume(12);
            requestBody.setAction("search");
            requestBody.setRequestUrl(ResourcesConfig.WEIXIN_SERVER_API_URL + url);
            requestBody.setJsonExt(String.valueOf(param));
            sendMtLog(requestBody);
        });
    }

    private void sendMtLog(RequestBody requestBody) {
        /*
        旧版MT收集停止
        try {
            log.info(requestBody.toString());
            HttpResponse<String> response = Unirest.post(commonConfig.getMtLogServer() + "/api/mt/log")
                    .header("Content-Type", "application/json")
                    .body(objectMapper.writeValueAsString(requestBody))
                    .asString();
            log.info(response.getBody());
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        */
    }

    /**
     *
     *  MT二期简历数据收集通用方法
     *
     */
    public void mtMessageCollect(String actionDesc,String additionalAction,String content,String contentDesc,String key,String url,String utmCampaign,String utmContent,String utmMedium,String utmSource,String utmTerm,String header){

        MTMessageResponse mtMessageResponse = new MTMessageResponse();
        mtMessageResponse.setUtmCampaign(utmCampaign);
        mtMessageResponse.setUtmContent(utmContent);
        mtMessageResponse.setUtmMedium(utmMedium);
        mtMessageResponse.setUtmSource(utmSource);
        mtMessageResponse.setUtmTerm(utmTerm);
        mtMessageResponse.setIsActivity(false);
        mtMessageResponse.setActionDesc(actionDesc);
        mtMessageResponse.setAdditionalAction(additionalAction);
        mtMessageResponse.setProductContent(content);
        mtMessageResponse.setContentDesc(contentDesc);
        mtMessageResponse.setKey(key);
        mtMessageResponse.setUrl(url);

//        HttpClientUtils.postMethod(commonConfig.getMtLogServer() + "/api/data/collect",mtMessageResponse.toString());
        try {
            log.info(header);
            log.info(mtMessageResponse.toString());
            HttpResponse<String> response = Unirest.post(commonConfig.getMtLogServer() + "/api/data/collect")
                                                   .header("Content-Type", "application/json")
                                                   .header("mt_header",header)
                                                   .body(objectMapper.writeValueAsString(mtMessageResponse))
                                                   .asString();
            log.info(response.getBody());
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static class RequestBody{
        private Integer productLine;
        private Integer consume;
        private String action;
        private Long consumeEntityId;
        private Long mooreUserId;
        private String mooreeliteOpenId;
        private String mooreliveOpenId;
        private String unionId;
        private String utmMedium;
        private String utmSource;
        private String utmCampaign;
        private String utmContent;
        private String utmTerm;
        private String params1;
        private String params2;
        private String params3;
        private String jsonExt;
        private String requestUrl;
        private String companyId;
        private Long jobId;
        private String keyword;
        private String tag;

        public Integer getProductLine() {
            return productLine;
        }

        public void setProductLine(Integer productLine) {
            this.productLine = productLine;
        }

        public Integer getConsume() {
            return consume;
        }

        public void setConsume(Integer consume) {
            this.consume = consume;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public Long getConsumeEntityId() {
            return consumeEntityId;
        }

        public void setConsumeEntityId(Long consumeEntityId) {
            this.consumeEntityId = consumeEntityId;
        }

        public Long getMooreUserId() {
            return mooreUserId;
        }

        public void setMooreUserId(Long mooreUserId) {
            this.mooreUserId = mooreUserId;
        }

        public String getMooreeliteOpenId() {
            return mooreeliteOpenId;
        }

        public void setMooreeliteOpenId(String mooreeliteOpenId) {
            this.mooreeliteOpenId = mooreeliteOpenId;
        }

        public String getMooreliveOpenId() {
            return mooreliveOpenId;
        }

        public void setMooreliveOpenId(String mooreliveOpenId) {
            this.mooreliveOpenId = mooreliveOpenId;
        }

        public String getUnionId() {
            return unionId;
        }

        public void setUnionId(String unionId) {
            this.unionId = unionId;
        }

        public String getUtmMedium() {
            return utmMedium;
        }

        public void setUtmMedium(String utmMedium) {
            this.utmMedium = utmMedium;
        }

        public String getUtmSource() {
            return utmSource;
        }

        public void setUtmSource(String utmSource) {
            this.utmSource = utmSource;
        }

        public String getUtmCampaign() {
            return utmCampaign;
        }

        public void setUtmCampaign(String utmCampaign) {
            this.utmCampaign = utmCampaign;
        }

        public String getUtmContent() {
            return utmContent;
        }

        public void setUtmContent(String utmContent) {
            this.utmContent = utmContent;
        }

        public String getUtmTerm() {
            return utmTerm;
        }

        public void setUtmTerm(String utmTerm) {
            this.utmTerm = utmTerm;
        }

        public String getParams1() {
            return params1;
        }

        public void setParams1(String params1) {
            this.params1 = params1;
        }

        public String getParams2() {
            return params2;
        }

        public void setParams2(String params2) {
            this.params2 = params2;
        }

        public String getParams3() {
            return params3;
        }

        public void setParams3(String params3) {
            this.params3 = params3;
        }

        public String getJsonExt() {
            return jsonExt;
        }

        public void setJsonExt(String jsonExt) {
            this.jsonExt = jsonExt;
        }

        public String getRequestUrl() {
            return requestUrl;
        }

        public void setRequestUrl(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public Long getJobId() {
            return jobId;
        }

        public void setJobId(Long jobId) {
            this.jobId = jobId;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        @Override
        public String toString() {
            return "RequestBody{" +
                    "productLine=" + productLine +
                    ", consume=" + consume +
                    ", action='" + action + '\'' +
                    ", consumeEntityId=" + consumeEntityId +
                    ", mooreUserId=" + mooreUserId +
                    ", mooreeliteOpenId='" + mooreeliteOpenId + '\'' +
                    ", mooreliveOpenId='" + mooreliveOpenId + '\'' +
                    ", unionId='" + unionId + '\'' +
                    ", utmMedium='" + utmMedium + '\'' +
                    ", utmSource='" + utmSource + '\'' +
                    ", utmCampaign='" + utmCampaign + '\'' +
                    ", utmContent='" + utmContent + '\'' +
                    ", utmTerm='" + utmTerm + '\'' +
                    ", params1='" + params1 + '\'' +
                    ", params2='" + params2 + '\'' +
                    ", params3='" + params3 + '\'' +
                    ", jsonExt='" + jsonExt + '\'' +
                    ", requestUrl='" + requestUrl + '\'' +
                    ", companyId='" + companyId + '\'' +
                    ", jobId=" + jobId +
                    ", keyword='" + keyword + '\'' +
                    ", tag='" + tag + '\'' +
                    '}';
        }
    }
}