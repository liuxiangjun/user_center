package com.moore.ms.common.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moore.ms.common.jackson.NullIntegerSerializer;

/**
 * Created by 汤云飞 on 2016/7/5.
 */
public class Pagination {

    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer current;
    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer next;
    @JsonSerialize(nullsUsing = NullIntegerSerializer.class)
    private Integer totalPages;


    public Pagination(Integer current, Integer next,Integer totalPages ) {
        this.current = current;
        this.next = next;
        this.totalPages = totalPages;

    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getNext() {
        return next;
    }

    public void setNext(Integer next) {
        this.next = next;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
