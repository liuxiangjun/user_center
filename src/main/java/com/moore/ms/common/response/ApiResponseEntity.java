package com.moore.ms.common.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by word on 2016/6/17.
 */
public class ApiResponseEntity {
    private HttpStatus code = HttpStatus.OK;
    private Object data;
    private ErrorEntity errorEntity;
    private Pagination pagination;



    public boolean hasError(){
        if(code.equals(HttpStatus.OK) || code.equals(HttpStatus.CREATED)){
            return false;
        } else {
            return  true;
        }
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    public void setData(Object data,HttpStatus code) {
        this.data = data;
        this.code = code;
    }

    public ErrorEntity getErrorEntity() {
        return errorEntity;
    }

    public void setErrorEntity(ErrorEntity errorEntity) {
        this.errorEntity = errorEntity;
    }


    public ApiResponseEntity() {

    }

    /**
     * 添加详细错误信息,并且设置第一级的报错信息
     */
    public void addError(HttpStatus code, ErrorDetail errorDetail, String allMessage){
        addError(code, errorDetail);
        errorEntity.setMessage(allMessage);
    }


    public void addError(HttpStatus code, ErrorDetail errorDetail){
        setCode(code);
        if(errorEntity == null){
            errorEntity = new ErrorEntity(errorDetail.getMessage());
            errorEntity.setMessage(errorDetail.getMessage());
        }
        errorEntity.getErrors().add(errorDetail);
    }

    /**
     * 添加详细错误信息
     */

    public void  addInfoError(String field, String code, String message){
        addInfoError(new ErrorDetail(field, code, message));
    }

    /**
     * 添加详细错误信息
     */

    public void  addInfoError(ErrorDetail errorDetail){
        addError(HttpStatus.BAD_REQUEST, errorDetail);
    }

    /**
     * 添加验证错误信息
     * @param errorDetail
     */
    public void  addTokenError(ErrorDetail errorDetail){
        addError(HttpStatus.UNAUTHORIZED, errorDetail);
    }

    /**
     * 如果成功返回{"code":200, "data":object}
     * example
              {
                 "code":200,
                 "data":object
              }
     * 失败返回
     * example
     *          {
                    "code": 400,
                    "error": {
                        "errors": [
                            {
                            "field": "deliver",
                            "code": "deliver.record.exist",
                            "message": "已经投递过该职位"
                            },
                            {
                            "field": "user",
                            "code": "user.incomplete",
                            "message": "用户信息不完善"
                            }
                        ],
                        "message": "不符合投递条件"
                        }
                    }
     *
     * 最后需要返回的JSON数据
     * @return
     */
    public Map<String, Object> getResponseData(){
        Map<String, Object> map = new HashMap<String, Object>();
        int codeValue = getCode().value();
        map.put("code", codeValue);
        if(codeValue >= 400 && codeValue < 500){
            map.put("error", errorEntity);
            if (errorEntity == null){
                map.put("error", data);
            }
        } else {
            map.put("data", data);
            if (pagination != null){
                map.put("pagination", pagination);
            }
        }
        return map;
    }

    public ResponseEntity<Map<String,Object>> createResponseEntity(){
        return  new ResponseEntity<Map<String, Object>>(getResponseData(), code);
    }


    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
