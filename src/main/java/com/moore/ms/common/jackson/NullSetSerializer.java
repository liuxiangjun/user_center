package com.moore.ms.common.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.collect.Sets;

import java.io.IOException;
import java.util.Set;

/**
 * Created by word on 2016/7/4.
 */
public class NullSetSerializer extends JsonSerializer<Set> {
    @Override
    public void serialize(Set value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if(value == null){
            gen.writeObject(Sets.newHashSet());
        } else {
            gen.writeObject(value);
        }
    }
}
