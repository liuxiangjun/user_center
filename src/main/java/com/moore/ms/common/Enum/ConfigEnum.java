package com.moore.ms.common.Enum;

/**
 * Created by Administrator on 2015/7/15.
 */
public class ConfigEnum {

    /**
     * 期望工作性质
     */
    public enum  JobTime {
        QUANZHI(1, "全职"), JIANZHI(2, "兼职"), XISHI(3, "实习");
        private final int index;
        private final String name;

        private JobTime(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }

        public static String getName(int index) {
            for (JobTime c : JobTime.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }

        public static int getIndex(String name) {
            for (JobTime c : JobTime.values()) {
                if (name.equals(c.getName())) {
                    return c.index;
                }
            }
            return 0;
        }
    }


    /**
     * 工作年限要求
     */
    public enum  JobWorkingYear {
        BUXIAN(0, "工作经验不限"), YINGJIE(1, "应届毕业生"), YINIAN(2, "1年以下"), YIDAOSAN(3, "1-3年"), SANDAOWU(4, "3年以上"), WUDAOSHI(5, "5年以上"), SHI(6, "10年以上");
        private final int index;
        private final String name;

        private JobWorkingYear(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (JobWorkingYear c : JobWorkingYear.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    public enum  UserWorkingYear {
        BUXIAN(0, null), YINGJIE(1, "应届毕业生"), YINIAN(2, "1年以下"), YIDAOSAN(3, "1-3年"), SANDAOWU(4, "3年以上"), WUDAOSHI(5, "5年以上"), SHI(6, "10年以上");
        private final int index;
        private final String name;

        private UserWorkingYear(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (UserWorkingYear c : UserWorkingYear.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 简历工作年限
     */
    public enum  ResumeJobWorkingYear {
        BUXIAN(0, ""), YINGJIE(1, "应届毕业生"), YINIAN(2, "1年以下"), YIDAOSAN(3, "1-3年"), SANDAOWU(4, "3年以上"), WUDAOSHI(5, "5年以上"), SHI(6, "10年以上");
        private final int index;
        private final String name;

        private ResumeJobWorkingYear(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (ResumeJobWorkingYear c : ResumeJobWorkingYear.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 学历要求
     */
    public enum  JobDegreeRequirement{
        BUXIAN(0, "学历不限"), DAZHUAN(1, "大专及以上"), BENKE(2, "本科及以上"), SHUOSHI(3, "硕士及以上"), BOSHI(4, "博士及以上");
        private final int index;
        private final String name;

        private JobDegreeRequirement(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (JobDegreeRequirement c : JobDegreeRequirement.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    public enum  UserDegree{
        BUXIAN(0, null), DAZHUAN(1, "大专"), BENKE(2, "本科"), SHUOSHI(3, "硕士"), BOSHI(4, "博士及以上");
        private final int index;
        private final String name;

        private UserDegree(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (UserDegree c : UserDegree.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 学历
     */
    public enum  FieldOfStudy{
        BUXIAN(0, ""), DAZHUAN(1, "大专"), BENKE(2, "本科"), SHUOSHI(3, "硕士"), BOSHI(4, "博士及以上");
        private final int index;
        private final String name;

        private FieldOfStudy(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (FieldOfStudy c : FieldOfStudy.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }

        public static Integer getValue(String name) {
            for (FieldOfStudy c : FieldOfStudy.values()) {
                if (c.name.equals(name)) {
                    return c.getIndex();
                }
            }
            return null;
        }
    }

    /**
     * 公司规模
     */
    public enum  CompanyScale{
        one(1, "0-50人"), two(2, "50-100人"), three(3, "100-200人"), four(4, "200-500人"), five(5, "500-1000人"), six(6, "1000人以上");
        private final int index;
        private final String name;

        private CompanyScale(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (CompanyScale c : CompanyScale.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     *
     */
    public enum  HistoryType{
        product(1, "产品"), fiance(2, "资本"), data(3, "数据"), person(4, "人员"), others(5, "其他");
        private final int index;
        private final String name;

        private HistoryType(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (HistoryType c : HistoryType.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 性别
     */
    public enum  Sex{
        women(1, "女"), man(2, "男"), no(0, null);
        private final int index;
        private final String name;

        private Sex(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (Sex c : Sex.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 个人当前工作状态
     */
    public enum  CurrentJobStatus{
        LEAVE(0, "我目前正在找工作"), ON(1, "有好的机会我会考虑"), NO(2, "我暂时不考虑换工作"), STUDENT(3, "我是应届毕业生");
        private final int index;
        private final String name;

        private CurrentJobStatus(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (CurrentJobStatus c : CurrentJobStatus.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /**
     * 期望薪资范围
     */
    public enum  ExpectSalay{
        ONE(1, "小于5k"), TWO(2, "5-10K"), THREE(3, "10-15K"), FOUR(4, "15-20K"), FIV(5, "20-30K"), SIX(6, "30-40K"), SEVEN(7, "40-50K"), EIGHT(8, "50K以上");
        private final int index;
        private final String name;

        private ExpectSalay(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (ExpectSalay c : ExpectSalay.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }



    public enum  QiniuPipelineRecord{
        ACTIVITYAUDIO(0, "活动音频"), ACTIVITYFILE(1, "活动文件"), ACTIVITYVIDEO(2, "活动视频");
        private final int index;
        private final String name;

        QiniuPipelineRecord(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return this.index;
        }

        public static String getName(int index) {
            for (QiniuPipelineRecord c : QiniuPipelineRecord.values()) {
                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }
    }

    public enum RankName{
        Rank1(1,"辅助性人才"),Rank2(2,"专业人才"),Rank3(3,"经理级"),Rank4(4,"总监及以上");
        private int idx;
        private String name;

        RankName(int idx,String name){
            this.idx = idx;
            this.name = name;
        }

        public int getIndex() {
            return this.idx;
        }

        public static String getName(int idx) {
            for (RankName c : RankName.values()) {
                if (c.getIndex() == idx) {
                    return c.name;
                }
            }
            return null;
        }

    }

    /**
     * 首字母图片
     */
    public enum  AppHeaderImg {
        A("a", "app/chat_user/app_header_a.png"),
        B("b", "app/chat_user/app_header_b.png"),
        C("c", "app/chat_user/app_header_c.png"),
        D("d", "app/chat_user/app_header_d.png"),
        E("e", "app/chat_user/app_header_e.png"),
        F("f", "app/chat_user/app_header_f.png"),
        G("g", "app/chat_user/app_header_g.png"),
        H("h", "app/chat_user/app_header_h.png"),
        I("i", "app/chat_user/app_header_i.png"),
        J("j", "app/chat_user/app_header_j.png"),
        K("k", "app/chat_user/app_header_k.png"),
        L("l", "app/chat_user/app_header_l.png"),
        M("m", "app/chat_user/app_header_m.png"),
        N("n", "app/chat_user/app_header_n.png"),
        O("o", "app/chat_user/app_header_o.png"),
        P("p", "app/chat_user/app_header_p.png"),
        Q("q", "app/chat_user/app_header_q.png"),
        R("r", "app/chat_user/app_header_r.png"),
        S("s", "app/chat_user/app_header_s.png"),
        T("t", "app/chat_user/app_header_t.png"),
        U("u", "app/chat_user/app_header_u.png"),
        V("v", "app/chat_user/app_header_v.png"),
        W("w", "app/chat_user/app_header_w.png"),
        X("x", "app/chat_user/app_header_x.png"),
        Y("y", "app/chat_user/app_header_y.png"),
        Z("z", "app/chat_user/app_header_z.png");

        private final String index;
        private final String name;

        private AppHeaderImg(String index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }

        public static String getName(String index) {
            for (AppHeaderImg c : AppHeaderImg.values()) {
                if (c.getIndex().equals(index)) {
                    return c.name;
                }
            }
            return null;
        }

        public static String getIndex(String name) {
            for (AppHeaderImg c : AppHeaderImg.values()) {
                if (name.equals(c.getIndex())) {
                    return c.name;
                }
            }
            return "";
        }
    }

}
