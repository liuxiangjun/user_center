package com.moore.ms.common.repository;


import com.moore.ms.domain.AppCompanySubaccount;

/**
 * Created by 汤云飞 on 2016/7/4.
 */
public interface AppCompanySubaccountRepository extends BaseRepository<AppCompanySubaccount,Long>  {
    AppCompanySubaccount findByHrEmail(String hrEmail);
}
