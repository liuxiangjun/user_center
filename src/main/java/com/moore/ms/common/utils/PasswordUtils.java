package com.moore.ms.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by 汤云飞 on 2016/7/4.
 */
public class PasswordUtils {

    private final static String salt = "Moore.Ren+13579";

    private final static String catSalt = "vc6d9%s1d3c79";

    public static String encodePassword(String password){

        return DigestUtils.md5Hex(password + salt);

    }

    public static String encodeCat(String cat){
        return DigestUtils.md5Hex(String.format(catSalt,cat));

    }

}
