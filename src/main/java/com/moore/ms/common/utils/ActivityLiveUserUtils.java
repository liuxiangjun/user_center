package com.moore.ms.common.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by pickj on 2017/11/7.
 */
public class ActivityLiveUserUtils {
    private static Long _section;
    private static List<String> list = Arrays.asList(
//            "o2WUt0tT9GT3bSWz9C4AufmteVAI",
//                "ooV7awnitpDhDNSojiiNycHSEvEE",
//                "oiNPAvjASgElQSwzHFRqEMW0WC-s",
//                "ooV7awlePX4loSADIHvvOS-t7F8I",
//                "ooV7awvON9vz9N862K-oD_1V9i-A",
//                "ooV7awjbPZjsNJc4edd0k0GVqsCw",
//                "ooV7awk3U9zNRM9m6G02oZlUw2_4"
            );

    public static Long getSection(){
        return _section;
    }

    public static void setSection(Long section){
        _section = section;
    }

    public static boolean isUser(String openId){
        return list.contains(openId);
    }
}
