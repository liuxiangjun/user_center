package com.moore.ms.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/10/22.
 */
public class ImageCropping {

    public static  String getImageCropping(String cropping) {
            String[] parts = cropping.split(";");
            List<String> tags = new ArrayList<>();
            String logo = "";

            for (int i = 0; i < parts.length; i++) {
                    String part = parts[i];
                    if(!StringUtils.isBlank(part)){
                            tags.add(part.substring(part.indexOf(":") + 1, part.length()));
                    }
            }
            if(tags != null) {
                    logo = "?imageMogr2/crop/!";
                    int j = 0;
                    for (String key : tags) {
                            logo  += key;
                            j++;
                            if(j ==1){
                                    logo +="x";
                            }else if(j  == 2){
                                    logo +="a";
                            }else if(j  == 3){
                                    logo +="a";
                            }
                    }
            }
            return logo;
    };
}
