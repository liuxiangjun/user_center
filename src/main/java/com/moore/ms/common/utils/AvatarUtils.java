package com.moore.ms.common.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class AvatarUtils {
    public static String getAvatar(String avatar, String imgSuffix){
        if(StringUtil.isEmpty(avatar)){
            return "";
        }
        if(avatar.indexOf("images/users/app/") > -1){
            int index = avatar.indexOf("_mooreroom_");
            if(index > -1){
                return imgSuffix + avatar.substring(0, index);
            }
            return imgSuffix + avatar;
        }

        int index = avatar.indexOf("_mooreroom_");
        if(index > -1){
            return imgSuffix + avatar.substring(0, index) + getImageCropping(avatar.substring(index+11));
        }

        if (avatar.contains("http://") || avatar.contains("https://")){
            return avatar;
        }

        String baseLogo = base64EncodeForUrl(avatar + "-200", imgSuffix);
        return imgSuffix + "images/base/logo_bg_200.png?watermark/1/gravity/Center/dx/0/dy/0/image/" + baseLogo;
    }

    private static String base64EncodeForUrl(String imagePath, String imageUrl){
        try {
            String urlEncoded = Base64.getUrlEncoder().encodeToString((imageUrl+imagePath).getBytes("utf-8"));
            return urlEncoded;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getImageCropping(String cropping) {
        String[] parts = cropping.split(";");
        List<String> tags = new ArrayList<>();
        String logo = "";

        for (int i = 0; i < parts.length; i++) {
            String part = parts[i];
            if(!org.apache.commons.lang3.StringUtils.isBlank(part)){
                tags.add(part.substring(part.indexOf(":") + 1, part.length()));
            }
        }
        if(tags != null) {
            logo = "?imageMogr2/crop/!";
            int j = 0;
            for (String key : tags) {
                logo  += key;
                j++;
                if(j ==1){
                    logo +="x";
                }else if(j  == 2){
                    logo +="a";
                }else if(j  == 3){
                    logo +="a";
                }
            }
        }
        return logo;
    }

}
