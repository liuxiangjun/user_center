package com.moore.ms.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by word on 2016/12/5.
 */
public class JarResourceUtil {

    private InputStream is;
    public JarResourceUtil(String path) {
        is = getClass().getClassLoader().getResourceAsStream(path);
        if(is == null){
            is = getClass().getClassLoader().getResourceAsStream("/" + path);
        }
    }

    public InputStream getWebResourceStream() {
        return  this.is;
    }

    public static String readFile(String jarFilePath){
        BufferedReader reader = new BufferedReader(new InputStreamReader(new JarResourceUtil(jarFilePath).getWebResourceStream()));
        String laststr = "";
        try {
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                laststr = laststr + tempString;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
        return laststr;
    }
}
