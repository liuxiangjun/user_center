package com.moore.ms.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;

/**
 * Created by 汤云飞 on 2016/7/11.
 */
public class FirstLetterUtil {
    /**
     *  返回中文字符的首字母
     */
    public static String getPinYinHeadChar(String str) {
        if(StringUtil.isEmpty(str)){
            return "m";
        }
        if (str.equals("长沙") || str.equals("长春") || str.equals("重庆")){
            return "c";
        }
        if (str.equals("厦门")){
            return "x";
        }
        String convert = "";
        char word = str.charAt(0);
        String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
        if (pinyinArray != null) {
            convert += pinyinArray[0].charAt(0);
        } else {
            convert += word;
        }
        return convert.toLowerCase();
    }
}
