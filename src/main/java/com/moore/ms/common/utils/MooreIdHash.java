package com.moore.ms.common.utils;

/**
 * User: Caixiaopig
 * Date: 15/9/6
 */
public class MooreIdHash {

    private String salt;

    private Hashids hashids;

//    String userId;

    void setSalt(String salt) {
        if(hashids!=null){
            hashids = new Hashids(salt, 16);
        }
        this.salt = salt;
    }

    public MooreIdHash(String salt) {
        hashids = new Hashids(salt, 16);
        this.salt = salt;
    }

    public String encodeForUser(Long id){
        String hashCode = hashids.encode(id.longValue());
        return "u"+hashCode;
    }

    public String encodeForUser(Integer id){
        return encodeForUser(id.longValue());
    }

    public Long decodeForUser(String hashCode){
        String realHashCode = hashCode.substring(1,hashCode.length());
        return hashids.decode(realHashCode)[0];
    }

    public String encodeForCompany(Long id){
        String hashCode = hashids.encode(id.longValue());
        return "c"+hashCode;
    }

    public String encodeForCompany(Integer id){
        return encodeForCompany(id.longValue());
    }

    public Long decodeForCompany(String hashCode){
        String realHashCode = hashCode.substring(1,hashCode.length());
        return hashids.decode(realHashCode)[0];
    }

    public String encode(Long time){
        return hashids.encode(time);
    }
    public Long decode(String code){
        return hashids.decode(code)[0];
    }


}
