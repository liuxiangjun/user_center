package com.moore.ms.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * http接口调用工具类
 * 整个web数据交互的核心类
 */
public class HttpClientUtils {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(HttpClientUtils.class);

    private static final String keyStr = "A45DSAF7EWQ1DSA88QWE";

    private static Map<String, String> headers = new HashMap<String, String>();  
    static {  
        headers.put("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.2)");  
        headers.put("Accept-Language", "zh-cn,zh;q=0.5");  
        headers.put("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");  
        headers.put("Accept"," image/gif, image/x-xbitmap, image/jpeg,"+"image/pjpeg,application/x-silverlight, application/vnd.ms-excel,"+"application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, */*");  
        headers.put("Content-Type", "application/x-www-form-urlencoded");  
        headers.put("Accept-Encoding", "gzip, deflate");  
    }


    /**
     * 正常的HTTP请求
     * @param url
     * @return 返回JSON格式数据字符串
     * @throws Exception
     */
	public static String normalPost(String url,Map<String, Object> data) throws Exception{
        LOGGER.debug("request:------------------------------------"+ url);
        LOGGER.debug("params:------------------------------------"+ data);
        String responseStr = "";
    	HttpClient client = new HttpClient();
		HttpMethod method = new PostMethod(url);
		method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		HttpMethodParams p = new HttpMethodParams();  
        for(Map.Entry<String, Object> entry : data.entrySet()) {
            p.setParameter(entry.getKey(), entry.getValue());   
        }
        method.setParams(p);
        method.getParams().setContentCharset("utf-8");
		client.executeMethod(method);
		if (method.getStatusCode() == HttpStatus.SC_OK) {
				responseStr = method.getResponseBodyAsString();
		}else{
			 throw new Exception("http statusCode:" + String.valueOf(method.getStatusCode()) + "\n" + method.getStatusText() + "\n" + "url:" +  method.getURI());
		}
        LOGGER.debug("response:------------------------------------"+ responseStr + "");
		return responseStr;
    }

    public static String normalPost1(String url,Map<String, String> data) throws Exception{
        LOGGER.debug("request:------------------------------------"+ url);
        LOGGER.debug("params:------------------------------------"+ data);
        String responseStr = "";
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(url);
        post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        for(Map.Entry<String, String> entry : data.entrySet()) {
            post.setParameter(entry.getKey(), entry.getValue());
        }

        post.getParams().setContentCharset("utf-8");
        client.executeMethod(post);
        if (post.getStatusCode() == HttpStatus.SC_OK) {
            responseStr = post.getResponseBodyAsString();
        }else{
            throw new Exception("http statusCode:" + String.valueOf(post.getStatusCode()) + "\n" + post.getStatusText() + "\n" + "url:" +  post.getURI());
        }
        LOGGER.debug("response:------------------------------------"+ responseStr + "");
        return responseStr;
    }

	public static String appServicePost(String url,Map<String, Object> data) throws Exception{
        LOGGER.debug("request:------------------------------------"+ url);
        LOGGER.debug("params:------------------------------------"+ data);
        String responseStr = "";
    	HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
		method.setRequestHeader("Content-Type", "application/json");
		method.setRequestHeader("X-Request-Id","79abb24e-d79a-470b-bb20-33bc9fbb2e2a");
		method.setRequestHeader("Accept", "application/vnd.mooreelite+json");

        RequestEntity entity = new StringRequestEntity(JSONObject.toJSONString(data),"application/json","UTF-8");
        method.setRequestEntity(entity);

		client.executeMethod(method);
		if (method.getStatusCode() == HttpStatus.SC_OK) {
				responseStr = method.getResponseBodyAsString();
		}else{
			 throw new Exception("http statusCode:" + String.valueOf(method.getStatusCode()) + "\n" + method.getStatusText() + "\n" + "url:" +  method.getURI());
		}
        LOGGER.debug("response:------------------------------------"+ responseStr + "");
		return responseStr;
    }

    /**
     * 正常的HTTP请求
     * @param url
     * @return 返回JSON格式数据字符串
     * @throws Exception
     */
    public static String normalGet(String url)throws Exception{
        LOGGER.debug("response-normalGet:------------------------------------"+ url + "");
        String resp = "";
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(url);
        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        client.executeMethod(method);
        if (method.getStatusCode() == HttpStatus.SC_OK) {
            resp = method.getResponseBodyAsString();
        }else{
             throw new Exception("http statusCode:" + String.valueOf(method.getStatusCode()) + "\n" + method.getStatusText() + "\n" + "url:" +  method.getURI());
        }
        LOGGER.debug("response:------------------------------------"+ resp + "");
        return resp;
    }


    /***
     * 调用官网发送EMAIL接口
     * @param url
     * @param queryString
     * @return
     * @throws Exception
     */
    public static  String sendEmailWithParams(String url,String queryString)throws Exception{
        LOGGER.debug("response-调用邮件接口:------------------------------------"+ url + "\n" +"----------------------queryString:"  + queryString);
        String resp = "";
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(url);
        method.setQueryString(queryString);
        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        method.setRequestHeader("key", keyStr);
        client.executeMethod(method);
        if (method.getStatusCode() == HttpStatus.SC_OK || method.getStatusCode() == HttpStatus.SC_CREATED) {
            resp = method.getResponseBodyAsString();
        }else{
            throw new Exception("http statusCode:" + String.valueOf(method.getStatusCode()) + "\n" + method.getStatusText() + "\n" + "url:" +  method.getURI());
        }
        LOGGER.debug("response:------------------------------------"+ resp + "");
        return resp;
    }


    /**
     * 正常的HTTP请求
     * @param url
     * @return 返回JSON格式数据字符串
     * @throws Exception
     */
    public static String normalGetWithParams(String url,String queryString)throws Exception{
        LOGGER.debug("response-normalGet:------------------------------------"+ url + "\n" +"----------------------queryString:"  + queryString);
        String resp = "";
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(url);
        method.setQueryString(queryString);
        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        client.executeMethod(method);
        if (method.getStatusCode() == HttpStatus.SC_OK) {
            resp = method.getResponseBodyAsString();
        }else{
             throw new Exception("http statusCode:" + String.valueOf(method.getStatusCode()) + "\n" + method.getStatusText() + "\n" + "url:" +  method.getURI());
        }
        LOGGER.debug("response:------------------------------------"+ resp + "");
        return resp;
    }
    

    public static Map<String, Object> normalGettoMap(String url)throws Exception{  
        String resp = normalGet(url);
        return parseJSON2Map(resp.toString());
    }

    /**
     * json字符串解析成map
     * @param jsonStr
     * @return
     */
    public static Map<String, Object> parseJSON2Map(String jsonStr){  
        Map<String, Object> map = new HashMap<String, Object>();  
        //最外层解析  
        JSONObject json = JSONObject.parseObject(jsonStr);  
        for(Object k : json.keySet()){  
            Object v = json.get(k);   
            //如果内层还是数组的话，继续解析  
            if(v instanceof JSONArray){  
                List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();  
                Iterator<Object> it = ((JSONArray) v).iterator();  
                while(it.hasNext()){  
                    JSONObject json2 = (JSONObject) it.next();  
                    list.add(parseJSON2Map(json2.toString()));  
                }  
                map.put(k.toString(), list);  
            } else {  
                map.put(k.toString(), v);  
            }  
        }  
        return map;  
    } 

    public static String parseMap2JSON(Map<String, Object> map){
        return JSONObject.toJSONString(map);
    }

}
