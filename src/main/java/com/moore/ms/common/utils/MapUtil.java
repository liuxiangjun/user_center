package com.moore.ms.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by pickj on 2017/9/14.
 */
public class MapUtil {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static short covertShort(Short orginal){
        if(orginal == null){
            return -1;
        }
        return orginal.shortValue();
    }

    public static int covertInteger(Integer orginal){
        if(orginal == null){
            return -1;
        }
        return orginal.intValue();
    }

    public static long covertLong(Long orginal){
        if(orginal == null){
            return -1;
        }
        return orginal.longValue();
    }

    public static float covertFloat(Float orginal){
        if(orginal == null){
            return -1;
        }
        return orginal.floatValue();
    }

    public static String covertString(String orginal){
        if(orginal == null){
            return "";
        }
        return orginal;
    }

    public static String covertDate(Date date){
        if(date == null){
            return "";
        }
        return sdf.format(date);
    }

    public static Object covertObject(Object orginal){
        if(orginal == null){
            return new HashMap();
        }
        return orginal;
    }
}
