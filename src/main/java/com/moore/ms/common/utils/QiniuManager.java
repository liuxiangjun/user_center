package com.moore.ms.common.utils;

import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.util.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.moore.ms.config.EnvConfig;

/**
 * Created by maqi on 2016/11/13.
 */
@Component
public class QiniuManager {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EnvConfig envConfig;
    /**
     * 复制七牛服务器上的文件资源
     * @param sourceKey 待复制文件
     * @param destKey 目标文件
     */
    public void copy(String sourceKey, String destKey){
        Zone z = Zone.zone0();
        Configuration c = new Configuration(z);
        //实例化一个BucketManager对象
        BucketManager bucketManager = new BucketManager(Auth.create(envConfig.getQiniuAccessKey(), envConfig.getQiniuSecretKey()), c);
        //要测试的空间和key，并且这个key在你空间中存在
        String bucket = "Bucket_Name";
        String key = "Bucket_key";
        //将文件从文件key 复制到文件key2。 可以在不同bucket复制
        String key2 = "yourjavakey";
        try {
            //调用copy方法移动文件
            bucketManager.copy(envConfig.getUploadFileBucket(), sourceKey, envConfig.getUploadFileBucket(), destKey);
        } catch (QiniuException e) {
            Response r = e.response;
            logger.error(r.toString(), e);
        }
    }
}
