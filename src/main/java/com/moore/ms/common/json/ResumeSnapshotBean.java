package com.moore.ms.common.json;


import com.moore.ms.domain.*;
import com.moore.ms.vo.UserEducationVo;

import java.util.List;

/**
 * Created by maqi on 2016/11/8.
 */
public class ResumeSnapshotBean {
    private User user;
    List<UserCompany> userCompanyList;//预览简历查询用户工作经历
    List<UserHonor> userHonorList;//预览简历查询用户荣誉
    List<UserProject> userProjectList;//预览简历查询用户项目经验
    List<UserPublication> userPublicationList;//预览简历查询用户专利
    List<UserPublish> userPublishList;//预览简历查询用户出版
    List<UserEducationVo> userEducationList;//用户教育经历

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<UserCompany> getUserCompanyList() {
        return userCompanyList;
    }

    public void setUserCompanyList(List<UserCompany> userCompanyList) {
        this.userCompanyList = userCompanyList;
    }

    public List<UserHonor> getUserHonorList() {
        return userHonorList;
    }

    public void setUserHonorList(List<UserHonor> userHonorList) {
        this.userHonorList = userHonorList;
    }

    public List<UserProject> getUserProjectList() {
        return userProjectList;
    }

    public void setUserProjectList(List<UserProject> userProjectList) {
        this.userProjectList = userProjectList;
    }

    public List<UserPublication> getUserPublicationList() {
        return userPublicationList;
    }

    public void setUserPublicationList(List<UserPublication> userPublicationList) {
        this.userPublicationList = userPublicationList;
    }

    public List<UserPublish> getUserPublishList() {
        return userPublishList;
    }

    public void setUserPublishList(List<UserPublish> userPublishList) {
        this.userPublishList = userPublishList;
    }

    public List<UserEducationVo> getUserEducationList() {
        return userEducationList;
    }

    public void setUserEducationList(List<UserEducationVo> userEducationList) {
        this.userEducationList = userEducationList;
    }
}
