package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.Feedback;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface FeedBackRepository extends BaseRepository<Feedback,Integer> {


}
