package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.EmailValidate;

/**
 * Created by 汤云飞 on 2017/7/5.
 */
public interface EmailValidateRepository extends BaseRepository<EmailValidate,Long> {

    EmailValidate findByCode(String code);

}
