package com.moore.ms.respository;



import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.UserScore;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/10/20.
 */
public interface UserScoreRepository extends BaseRepository<UserScore,Long> {
    List<UserScore> findByUserId(Long userId);

    List<UserScore> findByUserIdIn(List<Long> userIds);
}
