package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinUnionRelationAll;

import java.util.List;

public interface WeixinUnionRelationAllRepository extends BaseRepository<WeixinUnionRelationAll, Long> {
    WeixinUnionRelationAll findByUnionId(String unionId);

    List<WeixinUnionRelationAll> findByUnionIdIn(List<String> unionId);

    WeixinUnionRelationAll findByMooreeliteOpenid(String mooreelite_openid);
}
