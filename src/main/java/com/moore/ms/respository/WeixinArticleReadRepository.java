package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinArticleRead;

import java.util.Date;

public interface WeixinArticleReadRepository extends BaseRepository<WeixinArticleRead, Long> {
    WeixinArticleRead findByTypeAndRefDateAndUserSource(Short type, Date refDate, Short userSource);
}
