package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WechatScanQrcode;

/**
 * Created by 汤云飞 on 2017/5/19.
 */
public interface WechatScanQrcodeRepository extends BaseRepository<WechatScanQrcode,Long> {
    WechatScanQrcode findByCode(String code);
}
