package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface UserRepository extends BaseRepository<User,Long> {


    User findByEmailOrRegisterMobile(String username, String username1);



    List<User> findAllByMobilePhone(String phone);

    List<User> findAllByIsDelete(Short isDelete);

    User findByImportId(String importId);

    List<User> findByRegisterMobileAndIsDeleteOrderByRegisterTimeDesc(String phone, Short aShort);

    User findFirstByRegisterMobileAndIsDeleteOrderByRegisterTimeDesc(String phone, Short aShort);

    User findByEmailAndIsDelete(String email, Short aShort);

    Page<User> findByFastDeliverScore(Pageable pageable);

    Page<User> findAllByIsDelete(Short isDelete, Pageable pageable);

    Page<User> findAllByIsDeleteAndFastDeliverScoreIsNotNull(Short isDelete, Pageable pageable);

    @Query(value=" select * from moore_core.users where register_mobile = ?1 and is_delete != 1 order by modify_time desc LIMIT 1",nativeQuery = true)
    User findUserByPhone(@Param("register_mobile") String register_mobile);

    List<User> findByIdIn(List<Long> userIds);

    List<User> findByIdInAndFinishSchoolYearIn(List<Long> userIds, List<String> finishYear);
}
