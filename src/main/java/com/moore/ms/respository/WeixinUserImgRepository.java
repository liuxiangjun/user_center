package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinUserImg;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface WeixinUserImgRepository extends BaseRepository<WeixinUserImg,Long> {
    WeixinUserImg findByOpenId(String openId);

    WeixinUserImg findByUserId(Long userId);
}
