package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.AppMobileCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface AppMobileCodeRepository extends BaseRepository<AppMobileCode,Long> {


    Page<AppMobileCode> findByMobileAndType(String phone, Short type, Pageable pageable);

    List<AppMobileCode> findByMobileAndCreateTimeAfterAndCreateTimeBeforeAndStatusCode(String phone, Date after, Date before, String statusCode);
}
