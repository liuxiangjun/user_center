package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinUserCumulate;

import java.util.Date;

public interface WeixinUserCumulateRepository extends BaseRepository<WeixinUserCumulate, Long> {
    WeixinUserCumulate findByTypeAndRefDate(Short type, Date refDate);
}
