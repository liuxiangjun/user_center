package com.moore.ms.respository;



import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.UserProject;

import java.util.List;

public interface UserProjectRepository extends BaseRepository<UserProject,Long> {

    void deleteByUserId(Long userId);

    List<UserProject> findByUserId(Long userId);
}
