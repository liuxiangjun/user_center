package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinArticleTotal;

import java.util.Date;

public interface WeixinArticleTotalRepository extends BaseRepository<WeixinArticleTotal, Long> {
    WeixinArticleTotal findByTypeAndRefDateAndMsgidAndStatDate(Short type, Date refDate, String msgid, Date statDate);
}
