package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinUserSummary;

import java.util.Date;

public interface WeixinUserSummaryRepository extends BaseRepository<WeixinUserSummary, Long> {
    WeixinUserSummary findByTypeAndUserSourceAndRefDate(Short type, Short userSource, Date refDate);
}
