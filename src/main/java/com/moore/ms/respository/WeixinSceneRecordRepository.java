package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinSceneRecord;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface WeixinSceneRecordRepository extends BaseRepository<WeixinSceneRecord,Integer> {


    WeixinSceneRecord findBySceneId(Integer sceneId);

    WeixinSceneRecord findFirstBySceneIdOrderByIdDesc(Integer sceneId);
}
