package com.moore.ms.respository;



import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinArticleSummary;

import java.util.Date;

public interface WeixinArticleSummaryRepository extends BaseRepository<WeixinArticleSummary, Long> {
    WeixinArticleSummary findByTypeAndRefDateAndMsgid(Short type, Date refDate, String msgid);
}
