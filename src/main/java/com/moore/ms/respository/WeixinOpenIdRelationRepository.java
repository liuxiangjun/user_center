package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinOpenIdRelation;

import java.util.List;

/**
 * Created by 汤云飞 on 2017/8/14.
 */
public interface WeixinOpenIdRelationRepository extends BaseRepository<WeixinOpenIdRelation,Long> {
    List<WeixinOpenIdRelation> findByUnionId(String unionId);

    List<WeixinOpenIdRelation> findByMooreeliteOpenid(String openId);
}
