package com.moore.ms.respository;


import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinArticleReadHour;

import java.util.Date;

public interface WeixinArticleReadHourRepository extends BaseRepository<WeixinArticleReadHour, Long> {
    WeixinArticleReadHour findByTypeAndRefDateAndRefHourAndUserSource(Short type, Date refDate, Integer refHour, Short userSource);
}
