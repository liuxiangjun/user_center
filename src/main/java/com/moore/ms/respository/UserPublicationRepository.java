package com.moore.ms.respository;



import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.UserPublication;

import java.util.List;

public interface UserPublicationRepository extends BaseRepository<UserPublication,Long> {

    void deleteByUserId(Long userId);

    List<UserPublication> findByUserId(Long userId);
}
