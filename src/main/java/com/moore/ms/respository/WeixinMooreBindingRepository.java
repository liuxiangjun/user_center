package com.moore.ms.respository;

import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.WeixinMooreBinding;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
public interface WeixinMooreBindingRepository extends BaseRepository<WeixinMooreBinding,Long> {

    @Query(value = "select * from moore_core.weixin_moore_binding where open_id = cast(?1 AS varchar) and type = 1 and (is_delete = 0 or is_delete is null)", nativeQuery = true)
    List<WeixinMooreBinding> findByOpenIdAndTypeAndIsDelete(@Param("open_id") String openId);

    @Query(value = "select * from moore_core.weixin_moore_binding where open_id = cast(?1 AS varchar) and type = 1 and (is_delete = 0 or is_delete is null) order by create_time desc LIMIT 1", nativeQuery = true)
    WeixinMooreBinding findFirstByOpenIdAndTypeAndIsDeleteOrderByCreateTimeDesc(@Param("open_id") String openId);

    List<WeixinMooreBinding> findByUnionIdAndIsDeleteOrderByCreateTimeDesc(String unionId, Integer isDelete);

    WeixinMooreBinding findByOpenId(String openId);

    List<WeixinMooreBinding> findByUnionId(String unionId);

    List<WeixinMooreBinding> findByUnionIdAndTypeNot(String unionId, Integer type);

    WeixinMooreBinding findByUserIdAndType(Long userId, Integer Type);

    WeixinMooreBinding findByUserIdAndTypeAndIsDelete(Long userId, Integer Type, Integer isDelete);

    WeixinMooreBinding findFirstByUserIdAndTypeAndIsDeleteOrderByCreateTimeDesc(Long userId, Integer Type, Integer isDelete);

    List<WeixinMooreBinding> findByTypeAndUnionIdIsNull(Integer type);

    List<WeixinMooreBinding> findByUserId(Long userId);

    WeixinMooreBinding findFirstByUnionIdAndIsDeleteOrderByCreateTimeDesc(String unionId, Integer isDelete);

    WeixinMooreBinding findFirstByUnionIdAndTypeAndIsDeleteOrderByCreateTimeDesc(String unionId, Integer type, Integer isDelete);

    @Modifying
    @Transactional
    Integer deleteByUserId(Long userId);
}
