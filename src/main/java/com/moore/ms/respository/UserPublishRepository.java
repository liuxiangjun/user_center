package com.moore.ms.respository;



import com.moore.ms.common.repository.BaseRepository;
import com.moore.ms.domain.UserPublish;

import java.util.List;

public interface UserPublishRepository extends BaseRepository<UserPublish,Long> {

    void deleteByUserId(Long userId);

    List<UserPublish> findByUserId(Long userId);
}
