package com.moore.ms.email;

import cn.submail.sdk.config.AppConfig;
import com.moore.ms.sms.Sender;

import java.util.Map;

/**
 * A Sender class define the mail mode to send HTTP request.
 * 
 * @see Sender
 * @version 1.0 at 2014/10/28
 * */
public class Mail extends Sender {

	private static final String API_SEND = "http://api.submail.cn/mail/send.json";
	private static final String API_XSEND = "http://api.submail.cn/mail/xsend.json";
	private static final String API_SUBSCRIBE = "http://api.submail.cn/addressbook/mail/subscribe.json";
	private static final String API_UNSUBSCRIBE = "http://api.submail.cn/addressbook/mail/unsubscribe.json";

	public Mail(AppConfig config) {
		this.config = config;
	}


	@Override
	public boolean send(Map<String, Object> data) {
		return request(API_SEND, data);
	}

	@Override
	public String sendEmail(Map<String, Object> data) {
		return requestEmail(API_SEND, data);
	}



	@Override
	public boolean xsend(Map<String, Object> data) {
		return request(API_XSEND, data);
	}

	@Override
	public boolean subscribe(Map<String, Object> data) {
		// TODO Auto-generated method stub
		return request(API_SUBSCRIBE, data);
	}

	@Override
	public boolean unsubscribe(Map<String, Object> data) {
		// TODO Auto-generated method stub
		return request(API_UNSUBSCRIBE, data);
	}
	
}
