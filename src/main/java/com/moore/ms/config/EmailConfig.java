package com.moore.ms.config;

import com.moore.ms.service.EmailSendService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 * User: tangyunfei
 * Date: 16/7/08
 */
@Configuration
public class EmailConfig {


    @Value("${email.active}")
    private String emailActive;

    @Value("${email.sendcloud.api.template.url}")
    private String sendCloudTemplateUrl;

    @Value("${email.sendcloud.api.user}")
    private String sendCloudApiUser;

    @Value("${email.sendcloud.api.key}")
    private String sendCloudApiKey;

    @Value("${email.sendcloud.api.batchuser}")
    private String sendCloudApiUserForBatch;

    @Value("${email.sendcloud.api.key}")
    private String sendCloudApiKeyForBatch;

    @Value("${email.sendcloud.from}")
    private String sendcloudMailFrom;


    @Value("${email.submail.api.template.url}")
    private String submailTemplateUrl;

    @Value("${email.submail.api.id}")
    private String submailApiId;

    @Value("${email.submail.api.signature}")
    private String submailApiSignature;

    @Value("${email.submail.from}")
    private String submailmailFrom;


    @Bean
    EmailSendService emailSendService(){
        EmailSendService emailSendService = null;
        if(!StringUtils.isBlank(emailActive) && emailActive.equals("submail")) {
            emailSendService = new EmailSendService(emailActive,submailTemplateUrl,submailApiId,submailApiSignature,submailmailFrom,templateEngine(),taskExecutor());
        }else{
            emailSendService = new EmailSendService(emailActive, sendCloudTemplateUrl,sendCloudApiUser,sendCloudApiKey,sendcloudMailFrom,templateEngine(),taskExecutor());
        }
        return emailSendService;
    }

    /**
     * thymeleafViewResolver()
     * @return
     */
    @Bean(name = "templateResolver")
    ClassLoaderTemplateResolver thymeleafViewResolver(){
        ClassLoaderTemplateResolver viewResolver = new ClassLoaderTemplateResolver();
        viewResolver.setPrefix("template/");
        viewResolver.setSuffix(".html");
        viewResolver.setCacheable(true);
        viewResolver.setCharacterEncoding("UTF-8");
        viewResolver.setTemplateMode("LEGACYHTML5");
        return viewResolver;
    }

    @Bean
    TemplateEngine templateEngine(){
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(thymeleafViewResolver());
        return templateEngine;
    }

    @Bean
    ThreadPoolTaskExecutor taskExecutor(){
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(30);
        taskExecutor.setCorePoolSize(10);
        return taskExecutor;
    }




}
