package com.moore.ms.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.moore.ms.common.utils.MooreIdHash;
import com.moore.ms.domain.SendRedPacket;
import com.moore.ms.domain.WxMpInDataSourceConfigStorage;
import com.moore.ms.domain.WxMpServiceIm;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ren.moore.search.client.CompanySearcher;
import ren.moore.search.client.JobSearcher;

import javax.annotation.PostConstruct;
import java.util.Properties;

/**
 * Created by 汤云飞 on 2016/11/7.
 */
@Configuration
public class ResourcesConfig {

    private static Logger log = LoggerFactory.getLogger(ResourcesConfig.class);

    @Value("${spring.profiles.active}")
    private String env;

    @Value("${solr.master.url}")
    private String solrMasterUrl;

    @Value("${weixin.mooreren.mchKey}")
    private String mchKey;

    @Value("${weixin.mooreren.mchCertFilePath}")
    private String certFilePath;

    @Value("${weixin.mooreren.mchId}")
    private String mchId;

    @Value("${qiniu.uploaded.image.urls}")
    private String qiniuServerForImage;

    @Value("${qiniu.uploaded.file.urls}")
    private String qiniuServerForFile;

    @Value("${qiniu.account.accesskey}")
    private String qiniuAccessKey;

    @Value("${qiniu.account.secretkey}")
    private String qiniuSecretKey;

    @Value("${qiniu.upload.image.bucket}")
    private String uploadImageBucket;

    @Value("${qiniu.upload.file.bucket}")
    private String uploadFileBucket;


    @Value("${kaptcha.textproducer.font.color}")
    private String fcolor;

    @Value("${kaptcha.textproducer.font.size}")
    private String fsize;

    @Value("${kaptcha.obscurificator.impl}")
    private String obscurificator;

    @Value("${kaptcha.noise.impl}")
    private String noise;

    @Value("${kaptcha.image.width}")
    private String width;

    @Value("${kaptcha.image.height}")
    private String height;

    @Value("${kaptcha.textproducer.char.length}")
    private String clength;

    @Value("${kaptcha.textproducer.char.space}")
    private String cspace;

    @Value("${kaptcha.background.clear.from}")
    private String from;

    @Value("${kaptcha.background.clear.to}")
    private String to;

    @Value("${kaptcha.background.white.from}")
    private String backgroundWhiteFrom;

    @Value("${kaptcha.background.white.to}")
    private String backgroundWhiteTo;

    @Value("${kaptcha.textproducer.font.white}")
    private String fontWhite;

    @Bean
    public DefaultKaptcha defaultKaptcha(){
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        properties.put("kaptcha.border", "no"/*kborder*/);//无边框
        properties.put("kaptcha.textproducer.font.color", fcolor);
        properties.put("kaptcha.textproducer.font.size", fsize);
        properties.put("kaptcha.obscurificator.impl", obscurificator);
        properties.put("kaptcha.noise.impl", noise);
        properties.put("kaptcha.image.width", width);
        properties.put("kaptcha.image.height", height);
        properties.put("kaptcha.textproducer.char.length", clength);
        properties.put("kaptcha.textproducer.char.space", cspace);
        properties.put("kaptcha.background.clear.from", from); //和登录框背景颜色一致
        properties.put("kaptcha.background.clear.to", to);
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }

    @Bean
    public DefaultKaptcha whiteKaptcha(){
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        properties.put("kaptcha.border", "no"/*kborder*/);//无边框
        properties.put("kaptcha.textproducer.font.color", fontWhite);
        properties.put("kaptcha.textproducer.font.size", fsize);
        properties.put("kaptcha.obscurificator.impl", obscurificator);
        properties.put("kaptcha.noise.impl", noise);
        properties.put("kaptcha.image.width", width);
        properties.put("kaptcha.image.height", height);
        properties.put("kaptcha.textproducer.char.length", clength);
        properties.put("kaptcha.textproducer.char.space", cspace);
        properties.put("kaptcha.background.clear.from", backgroundWhiteFrom); //和登录框背景颜色一致
        properties.put("kaptcha.background.clear.to", backgroundWhiteTo);
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }


    @Bean
    EnvConfig envConfig(){
        EnvConfig envConfig = new EnvConfig();
        envConfig.setMooreServerForWx(WEIXIN_SERVER_URL);
        envConfig.setMooreServerAPIForWx(WEIXIN_SERVER_API_URL);
        envConfig.setQiniuServerForImage(qiniuServerForImage);
        envConfig.setQiniuServerForFile(qiniuServerForFile);
        envConfig.setQiniuAccessKey(qiniuAccessKey);
        envConfig.setQiniuSecretKey(qiniuSecretKey);
        envConfig.setUploadImageBucket(uploadImageBucket);
        envConfig.setUploadFileBucket(uploadFileBucket);
        envConfig.setWeixinAppId(WEIXIN_MOOREREN_APPID);
        envConfig.setTemplateMessageForInterview(WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW);
        envConfig.setMooreServerForEmail(EMAIL_SERVER_URL);
        envConfig.setMooreServerForSms(SMS_SERVER_URL);
        envConfig.setMooreServer(MOORE_SERVER_URL);
        envConfig.setAdminServer(ADMIN_SERVER_URL);
        envConfig.setMooreEngineServer(MOORE_ENGINE_SERVER);
        envConfig.setS_accountId(A_ACCOUNT_ID);
        envConfig.setEnv(env);
        return envConfig;
    }
    @Bean
    WxMpConfigStorage wxMpConfigStorage(){
        WxMpConfigStorage storage = new WxMpInDataSourceConfigStorage(WEIXIN_MOOREREN_APPID,WEIXIN_MOOREREN_APPSECRET,
                WEIXIN_MOOREREN_TOKEN,WEIXIN_MOOREREN_ENCODINGAESKEY);

        return storage;
    }
//    @Bean
//     WxMpConfigStorage configStorage() {
//        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
//        configStorage.setAppId(WEIXIN_MOOREREN_APPID);
//        configStorage.setSecret(WEIXIN_MOOREREN_APPSECRET);
//        configStorage.setToken(WEIXIN_MOOREREN_TOKEN);
//        configStorage.setAesKey(WEIXIN_MOOREREN_ENCODINGAESKEY);
//    return configStorage;
//}

    @Bean
    WxMpService wxMpService(){
        WxMpService wxMpService = new WxMpServiceIm(ACCESS_TOKEN_URL,JS_TICKEY_URL);
        //WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

    @Bean
    WxMpMessageRouter wxMpMessageRouter(){
        WxMpMessageRouter wxMpMessageRouter = new WxMpMessageRouter(wxMpService());
        return wxMpMessageRouter;
    }

    @Bean
    public WxPayService wxPayService(){
        WxPayConfig config = new WxPayConfig();
        config.setAppId(WEIXIN_MOOREREN_APPID);
        config.setMchId(WEIXIN_MCH_ID);
        config.setMchKey(WEIXIN_MCH_KEY);
        config.setKeyPath(WEIXIN_KEY_PATH);
        config.setNotifyUrl(WEIXIN_SERVER_URL+"activity/wechat/notify");
        WxPayService wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(config);
        return wxPayService;
    }

    @Bean
    MooreIdHash mooreIdHash(){
        MooreIdHash mooreIdHash = new MooreIdHash("Moore.135");
        return mooreIdHash;
    }

    @Bean
    SolrClient solrClient(){
        SolrClient solrClient = new HttpSolrClient(solrMasterUrl);
        return solrClient;
    }

    @Bean
    JobSearcher jobSearcher(){
        JobSearcher jobSearcher = new JobSearcher(solrClient());
        return jobSearcher;
    }

    @Bean
    public CompanySearcher companySearcher() {
        CompanySearcher companySearcher = new CompanySearcher(solrClient());
        return companySearcher;
    }

    @Bean
    SendRedPacket sendRedPacket(){
        SendRedPacket sendRedPacket = new SendRedPacket();
        sendRedPacket.setMchKey(mchKey);
        sendRedPacket.setCertFilePath(certFilePath);
        sendRedPacket.setMchId(mchId);

        return sendRedPacket;

    }

    @Bean
    public CloseableHttpClient httpClient(){
        CloseableHttpClient client = HttpClients.createDefault();
        return client;
    }


    public static String WEIXIN_MOOREREN_APPID;

    public static String WEIXIN_MOOREREN_APPSECRET;

    public static String WEIXIN_MOOREREN_TOKEN;

    public static String WEIXIN_MOOREREN_ENCODINGAESKEY;

    public static String WEIXIN_SERVER_URL;

    public static String WEIXIN_SERVER_API_URL;

    public static String MOORE_SERVER_URL;

    public static String EMAIL_SERVER_URL;

    public static String SMS_SERVER_URL;

    public static String ADMIN_SERVER_URL;

    public static String MOORE_ENGINE_SERVER;

    public static String WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW;

    public static String ACCESS_TOKEN_URL;
    public static String JS_TICKEY_URL;

    public static String A_ACCOUNT_ID;

    public static String WEIXIN_MCH_ID;

    public static String WEIXIN_MCH_KEY;

    public static String WEIXIN_KEY_PATH;


    @PostConstruct
    private void init(){
        if (StringUtils.isNotBlank(System.getProperty("wx.developer"))) {
            switch (System.getProperty("wx.developer")) {
                case "lilei"://lilei的测试号
                    WEIXIN_MOOREREN_APPID = "wxdcfeae2a05b771ce";
                    WEIXIN_MOOREREN_APPSECRET = "d4624c36b6795d1d99dcf0547af5443d";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
                    WEIXIN_SERVER_URL = "http://wx.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "";
                    break;
                case "liuzhi"://刘智的测试号
                    WEIXIN_MOOREREN_APPID = "wx06e1b69e6f6096f2";
                    WEIXIN_MOOREREN_APPSECRET = "81bd331f3954a668b00965fdfd6049a9";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
                    WEIXIN_SERVER_URL = "http://tyf.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://tyf.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "liuzhizf"://支付测试号
                    WEIXIN_MOOREREN_APPID = "wxca3c1b393a64a4c8";
                    WEIXIN_MOOREREN_APPSECRET = "113ecac7954934e7a86618f13a1a04ac";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "QEJd7G6h0lkFAAcsqIlMCimv1VlmhnYgYdd7tXATqXC";
                    WEIXIN_SERVER_URL = "http://tyf.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://tyf.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "5:0VVmb5QzRLyU4ndlmBLNLpo5SKv7JGh8qVJX2Kcb2Yg," +
                            "6:WTm6n4S8LeL_YStv3wv5IXT_IV8QW1tvDfgNRKlmRd8";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    WEIXIN_MCH_ID = "1480180072";
                    WEIXIN_MCH_KEY = "niNpeEseyCKaXMgVdJYfH2JmM8iXVdBD";
                    WEIXIN_KEY_PATH = "classpath:paykey/dev";
                    break;
                case "tangyunfei"://汤云飞的测试号
                    WEIXIN_MOOREREN_APPID = "wx0364e5a4452c402c";
                    WEIXIN_MOOREREN_APPSECRET = "77fef657478323d283a37d1cf9663bb5";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
                    WEIXIN_SERVER_URL = "http://tyf.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://tyf.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "5:0VVmb5QzRLyU4ndlmBLNLpo5SKv7JGh8qVJX2Kcb2Yg,6:M3bNvg5lFqgc_RO3tZzh6PAeVHwf3ptnoZLJNBPz0Qk";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=tyf-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=tyf-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "zsg"://正式的MooreElite服务号
                    WEIXIN_MOOREREN_APPID = "wx37dea5eab3646964";
                    WEIXIN_MOOREREN_APPSECRET = "9c4450e15d89a4d33342a4abfff167ed";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "qpGRrrkgyxypTFzHl35i0FrHYUmXlda8hTCG3I6p0al";
                    WEIXIN_SERVER_URL = "http://wx.dev.moorelive.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://wx.dev.moorelive.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=zsg-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=zsg-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "idc": //正式的MooreElite服务号
                    WEIXIN_MOOREREN_APPID = "wxf267a5dab5a918b0";
                    WEIXIN_MOOREREN_APPSECRET = "846c1133c48d9a5aaf37f48861db16a0";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "qpGRrrkgyxypTFzHl35i0FrHYUmXlda8hTCG3I6p0al";
                    WEIXIN_SERVER_URL = "http://wxidc.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://wxidc.server.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "1:sCcdvhrAuxOjXI1kz0cm0vUNmJkVGdtmpYEgZWId4hg," +
                            "2:dO5taAOsH-zLxjfKE4Nj_6j56_LFBH-cRxmEB7Yqtf8," +
                            "3:EFZsSE6vMk8kEb8jD5ZOv08p7NluREsKgxiGW4hYNoA," +
                            "4:z449mHlf8D29SuwJMGsdKnaPpvnbEUppYDFDFaH-wao,"+//1:面试提醒，2:明日面试提醒，3:重新安排面试
                            "5:f_eF-YntW8tb8e0ITzlkVV0axw1f4RUrs1C7ErPPxrI";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=mooreelitefortest-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=mooreelitefortest-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "ic": //正式的MooreElite服务号
                    WEIXIN_MOOREREN_APPID = "wx1af216ed3949859e";
                    WEIXIN_MOOREREN_APPSECRET = "896d397d37a9c4883eca493e6e200630";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "qpGRrrkgyxypTFzHl35i0FrHYUmXlda8hTCG3I6p0al";
                    WEIXIN_SERVER_URL = "http://wxidc.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "1:sCcdvhrAuxOjXI1kz0cm0vUNmJkVGdtmpYEgZWId4hg," +
                            "2:dO5taAOsH-zLxjfKE4Nj_6j56_LFBH-cRxmEB7Yqtf8," +
                            "3:EFZsSE6vMk8kEb8jD5ZOv08p7NluREsKgxiGW4hYNoA," +
                            "4:z449mHlf8D29SuwJMGsdKnaPpvnbEUppYDFDFaH-wao,"+//1:面试提醒，2:明日面试提醒，3:重新安排面试
                            "5:f_eF-YntW8tb8e0ITzlkVV0axw1f4RUrs1C7ErPPxrI";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=icbank-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=mooreelitefortest-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "lileiidc"://正式的MooreElite服务号
                    WEIXIN_MOOREREN_APPID = "wxf267a5dab5a918b0";
                    WEIXIN_MOOREREN_APPSECRET = "846c1133c48d9a5aaf37f48861db16a0";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "qpGRrrkgyxypTFzHl35i0FrHYUmXlda8hTCG3I6p0al";
                    WEIXIN_SERVER_URL = "http://wx.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "1:sCcdvhrAuxOjXI1kz0cm0vUNmJkVGdtmpYEgZWId4hg," +
                            "2:dO5taAOsH-zLxjfKE4Nj_6j56_LFBH-cRxmEB7Yqtf8," +
                            "3:EFZsSE6vMk8kEb8jD5ZOv08p7NluREsKgxiGW4hYNoA," +
                            "4:z449mHlf8D29SuwJMGsdKnaPpvnbEUppYDFDFaH-wao,"+//1:面试提醒，2:明日面试提醒，3:重新安排面试
                            "5:f_eF-YntW8tb8e0ITzlkVV0axw1f4RUrs1C7ErPPxrI";
                    break;
                case "wusheng"://吴胜测试号
                    WEIXIN_MOOREREN_APPID = "wxd74c56f37118408c";
                    WEIXIN_MOOREREN_APPSECRET = "672f3605d0e60a593f012202daf725a1";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
                    WEIXIN_SERVER_URL = "http://tyf.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://tyf.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "5:0VVmb5QzRLyU4ndlmBLNLpo5SKv7JGh8qVJX2Kcb2Yg," +
                        "6:M3bNvg5lFqgc_RO3tZzh6PAeVHwf3ptnoZLJNBPz0Qk";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=wusheng-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=wusheng-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "lidengxiu"://李登修测试号
                    WEIXIN_MOOREREN_APPID = "wx6b44da4e08194473";
                    WEIXIN_MOOREREN_APPSECRET = "c3af6f33738ed8d92b28b48abeab726e";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
                    WEIXIN_SERVER_URL = "http://wx1.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://wx1.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "5:0VVmb5QzRLyU4ndlmBLNLpo5SKv7JGh8qVJX2Kcb2Yg," +
                            "6:M3bNvg5lFqgc_RO3tZzh6PAeVHwf3ptnoZLJNBPz0Qk";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=lidengxiu-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=wusheng-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    break;
                case "rentech"://支付测试号
                    WEIXIN_MOOREREN_APPID = "wxca3c1b393a64a4c8";
                    WEIXIN_MOOREREN_APPSECRET = "113ecac7954934e7a86618f13a1a04ac";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "QEJd7G6h0lkFAAcsqIlMCimv1VlmhnYgYdd7tXATqXC";
                    WEIXIN_SERVER_URL = "http://wxidc.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://wxidc.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "5:0VVmb5QzRLyU4ndlmBLNLpo5SKv7JGh8qVJX2Kcb2Yg," +
                        "6:WTm6n4S8LeL_YStv3wv5IXT_IV8QW1tvDfgNRKlmRd8";
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=moorerentech-weixin_jsticket";
                    A_ACCOUNT_ID = "a4a8e96ab58c383d";
                    WEIXIN_MCH_ID = "1480180072";
                    WEIXIN_MCH_KEY = "niNpeEseyCKaXMgVdJYfH2JmM8iXVdBD";
                    WEIXIN_KEY_PATH = "classpath:paykey/dev";
                    break;
                case "hr"://正式的MooreElite服务号
                    WEIXIN_MOOREREN_APPID = "wx1061b84237e7d0cc";
                    WEIXIN_MOOREREN_APPSECRET = "9bdc3d7edb95a7710c0a5653dbb6db35";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "08Q7j8Ntb6mWcbltz6EYkhMQ1AbqUbhrMTuiRjGWQxU";
                    WEIXIN_SERVER_URL = "http://wx.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "1:sCcdvhrAuxOjXI1kz0cm0vUNmJkVGdtmpYEgZWId4hg," +
                            "2:dO5taAOsH-zLxjfKE4Nj_6j56_LFBH-cRxmEB7Yqtf8," +
                            "3:EFZsSE6vMk8kEb8jD5ZOv08p7NluREsKgxiGW4hYNoA," +
                            "4:z449mHlf8D29SuwJMGsdKnaPpvnbEUppYDFDFaH-wao,"+//1:面试提醒，2:明日面试提醒，3:重新安排面试
                            "5:f_eF-YntW8tb8e0ITzlkVV0axw1f4RUrs1C7ErPPxrI";
                    break;
                case "liumingxin": //刘明新的测试号
                    WEIXIN_MOOREREN_APPID = "wx973e1b12d9c7b5bf";
                    WEIXIN_MOOREREN_APPSECRET = "49e59f72b32943d448f0c7fd9e300c64";
                    WEIXIN_MOOREREN_TOKEN = "weixinMoore";
                    WEIXIN_MOOREREN_ENCODINGAESKEY = "QEJd7G6h0lkFAAcsqIlMCimv1VlmhnYgYdd7tXATqXC";
                    WEIXIN_SERVER_URL = "http://lilei.dev.moore.ren/";
                    WEIXIN_SERVER_API_URL = "http://wxidc.server.dev.moore.ren/";
                    MOORE_SERVER_URL = "http://pc.dev.moore.ren/";
                    ADMIN_SERVER_URL = "http://172.16.20.5:8484/";
                    EMAIL_SERVER_URL = "http://172.16.20.5:8181/weixinEmail/";
                    SMS_SERVER_URL = "http://172.16.20.5:8181/weixin/";
                    MOORE_ENGINE_SERVER = "http://172.16.20.5:8290/";
                    WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "" +
                            "1:LcWFAE5w3_uRmRhNefsG2jiRgiPJrBqR2qTkoztH3d0," + //今日面试提醒           即时 ()
                            "2:RFJWnHQ5fKxxBvuyCasECqQb6hCyLmkZ-bKShmQlvo8," + //校招用户投递       定时 (网站端)
                            "3:TksLUsf8WK3RIBVQ2BoFlvnb5GofiuSnOTagzRmJPV4," + //社招用户投递 即时 （网站端或者微信端）
                            "4:fMUJGPq1rtUFX6DMwFxVrj-YOaco8NG7w3oDOt0JoCo," ; //新简历通知       定时（网站端）
                    ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=lmx-weixin";
                    JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=lmx-weixin_jsticket";
                    break;
            }
        } else {//摩尔精英正式公众号
            WEIXIN_MOOREREN_APPID = "wx961c9127b803bd00";
            WEIXIN_MOOREREN_APPSECRET = "b6bc16d154cd7ef9a604f352886c01f7";
            WEIXIN_MOOREREN_TOKEN = "weixinMoore";
            WEIXIN_MOOREREN_ENCODINGAESKEY = "CaTcTjaZqZIhZSMCnmJojhiAAwaBAjz3guaFU1BsHRg";
            WEIXIN_SERVER_URL = "http://wx.moore.ren/";
            WEIXIN_SERVER_API_URL = "http://server.wx.moore.ren/";
            MOORE_SERVER_URL = "http://www.moore.ren/";
            ADMIN_SERVER_URL = "http://adminp.moore.ren/";
            EMAIL_SERVER_URL = "http://www.moore.ren/weixinEmail/";
            SMS_SERVER_URL = "http://www.moore.ren/weixin/";
            MOORE_ENGINE_SERVER = "http://172.16.10.11:8290/";
            WEIXIN_MOOREREN_TEMPLATEMESSAGE_INTERVIEW = "1:iWlXNjyULGhLcyQZ005iopZMkT8f4HqRS9Ud-nKe7mI,"+
                    "2:PIaxNSuMn-W6--82a6u1KjjdJd8LoVqgL51-P44wvFA,"+
                    "3:7QHEpfaSCxt0dR5nZk3OYUQloPkaKx4Pfwr4xVxkBLY," +
                    "4:-A9nmuyrYeG2rsOdFb_jp6ZdywdEveEUdl9csHYRXq0,"+//1:面试提醒，2:明日面试提醒，3:重新安排面试
                    "5:NBYN_BZqeVmes3MJqfAyXQdEzfYmKfGSCOGE2U4ZR9c,"+
                    "6:ngs88kMqPEKLC427svphwjERlDHDBSAALMwvldM3X9U";
            ACCESS_TOKEN_URL = "http://172.16.10.21:3032/token?name=mooreren-weixin";
            JS_TICKEY_URL = "http://172.16.10.21:3032/token?name=mooreren-weixin_jsticket";
            A_ACCOUNT_ID = "bfd4e5ec6f227912";
            WEIXIN_MCH_ID = "1267365501";
            WEIXIN_MCH_KEY = "dwxHsEMZqBZiTTHXHBt4mPYuEXZ9fnqs";
            WEIXIN_KEY_PATH = "classpath:paykey/moore";
        }
    }




}
