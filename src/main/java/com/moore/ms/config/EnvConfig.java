package com.moore.ms.config;

/**
 * Created by 汤云飞 on 2016/11/8.
 */
public class EnvConfig {
    private String mooreServerForWx;//各种跳转和分享都需要这个在微信上注册的回调服务器地址

    private String mooreServerAPIForWx;//各种跳转和分享都需要这个在微信上注册的回调服务器地址

    private String weixinAppId;//JS签名需要

    private String qiniuServerForImage;

    private String qiniuServerForFile;

    private String qiniuAccessKey;

    private String qiniuSecretKey;

    private String uploadImageBucket;

    private String uploadFileBucket;

    private Integer cookieLifetime;//cookie的存活时间

    private String mooreServerForEmail; //官网提供的发送email的地址

    private String mooreServerForSms; //官网提供的发送sms的地址

    private String mooreServer; //摩尔精英网站

    private String adminServer;

    private String mooreEngineServer;//推荐引擎


    private String templateMessageForInterview;

    private String s_accountId;

    private String env;

    public String getTemplateMessageForInterview() {
        return templateMessageForInterview;
    }

    public void setTemplateMessageForInterview(String templateMessageForInterview) {
        this.templateMessageForInterview = templateMessageForInterview;
    }

    public Integer getCookieLifetime() {
        return cookieLifetime;
    }

    public void setCookieLifetime(Integer cookieLifetime) {
        this.cookieLifetime = cookieLifetime;
    }

    public String getMooreServerForWx() {
        return mooreServerForWx;
    }

    public void setMooreServerForWx(String mooreServerForWx) {
        this.mooreServerForWx = mooreServerForWx;
    }

    public String getWeixinAppId() {
        return weixinAppId;
    }

    public void setWeixinAppId(String weixinAppId) {
        this.weixinAppId = weixinAppId;
    }

    public String getQiniuServerForImage() {
        return qiniuServerForImage;
    }

    public void setQiniuServerForImage(String qiniuServerForImage) {
        this.qiniuServerForImage = qiniuServerForImage;
    }

    public String getQiniuServerForFile() {
        return qiniuServerForFile;
    }

    public void setQiniuServerForFile(String qiniuServerForFile) {
        this.qiniuServerForFile = qiniuServerForFile;
    }

    public EnvConfig() {
    }

    public String getMooreServerForSms() {
        return mooreServerForSms;
    }

    public void setMooreServerForSms(String mooreServerForSms) {
        this.mooreServerForSms = mooreServerForSms;
    }

    public String getMooreServerForEmail() {
        return mooreServerForEmail;
    }

    public void setMooreServerForEmail(String mooreServerForEmail) {
        this.mooreServerForEmail = mooreServerForEmail;
    }

    public String getQiniuAccessKey() {
        return qiniuAccessKey;
    }

    public void setQiniuAccessKey(String qiniuAccessKey) {
        this.qiniuAccessKey = qiniuAccessKey;
    }

    public String getQiniuSecretKey() {
        return qiniuSecretKey;
    }

    public void setQiniuSecretKey(String qiniuSecretKey) {
        this.qiniuSecretKey = qiniuSecretKey;
    }

    public String getUploadImageBucket() {
        return uploadImageBucket;
    }

    public void setUploadImageBucket(String uploadImageBucket) {
        this.uploadImageBucket = uploadImageBucket;
    }

    public String getMooreServer() {
        return mooreServer;
    }

    public void setMooreServer(String mooreServer) {
        this.mooreServer = mooreServer;
    }

    public String getUploadFileBucket() {
        return uploadFileBucket;
    }

    public void setUploadFileBucket(String uploadFileBucket) {
        this.uploadFileBucket = uploadFileBucket;
    }

    public String getMooreEngineServer() {
        return mooreEngineServer;
    }

    public void setMooreEngineServer(String mooreEngineServer) {
        this.mooreEngineServer = mooreEngineServer;
    }

    public String getS_accountId() {
        return s_accountId;
    }

    public void setS_accountId(String s_accountId) {
        this.s_accountId = s_accountId;
    }

    public String getAdminServer() {
        return adminServer;
    }

    public void setAdminServer(String adminServer) {
        this.adminServer = adminServer;
    }

    public String getMooreServerAPIForWx() {
        return mooreServerAPIForWx;
    }

    public void setMooreServerAPIForWx(String mooreServerAPIForWx) {
        this.mooreServerAPIForWx = mooreServerAPIForWx;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
