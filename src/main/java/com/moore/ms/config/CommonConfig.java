package com.moore.ms.config;

import cn.submail.sdk.config.AppConfig;
import cn.submail.sdk.config.MessageConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * User: Caixiaopig
 * Date: 15/8/23
 */
@Configuration
public class CommonConfig {


    @Value("${jwt.secretkey}")
    private String jwtSecretkey;    //JWT密钥

    //submail 短信配置
    @Value("${sms.submail.url}")
    private String smsSubmailUrl;

    @Value("${sms.submail.appId}")
    private String smsSubmailAppId;

    @Value("${sms.submail.appKey}")
    private String smsSubmailAppKey;

    @Value("${sms.submail.msgSigntype}")
    private String smsMsgSigntype;

    @Value("${spring.profiles.active}")
    private String producation;

    @Value("${moore.im.url}")
    private String imServer;

    @Value("${mt.log}")
    private String mtLogServer;

    @Value("${spring.profiles}")
    private String profiles;

//    @Value("${}")
//    private String wxUrl;

    public String getJwtSecretkey() {
        return jwtSecretkey;
    }

    public void setJwtSecretkey(String jwtSecretkey) {
        this.jwtSecretkey = jwtSecretkey;
    }

    @Bean
    AppConfig appConfig(){
        AppConfig config = new MessageConfig();
        config.setAppId(smsSubmailAppId);
        config.setAppKey(smsSubmailAppKey);
        config.setSignType(smsMsgSigntype);
        return config;
    }

    public String getSmsSubmailUrl() {
        return smsSubmailUrl;
    }

    public void setSmsSubmailUrl(String smsSubmailUrl) {
        this.smsSubmailUrl = smsSubmailUrl;
    }

    //    @Bean
//    Rollbar rollbar(){
//        return  new Rollbar("9aa6582753644e8aad8b503a96c69c2f",producation);
//    }


    public String getImServer() {
        return imServer;
    }

    public void setImServer(String imServer) {
        this.imServer = imServer;
    }

    public String getMtLogServer() {
        return mtLogServer;
    }

    public void setMtLogServer(String mtLogServer) {
        this.mtLogServer = mtLogServer;
    }

    public String getProfiles() {
        return profiles;
    }

    public void setProfiles(String profiles) {
        this.profiles = profiles;
    }
}
