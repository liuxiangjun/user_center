package com.moore.ms.config;

import com.netflix.hystrix.HystrixCommand;
import feign.Feign;
import feign.Retryer;
import feign.hystrix.HystrixFeign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.feign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by 汤云飞 on 2016/8/20.
 */
//@Configuration
public class FeignConfig extends FeignClientsConfiguration {

    @Configuration
    @ConditionalOnClass({ HystrixCommand.class, HystrixFeign.class })
    public static class HystrixFeignConfiguration {
        @Bean
        @Scope("prototype")
        @ConditionalOnMissingBean
        @ConditionalOnProperty(name = "feign.hystrix.enabled", matchIfMissing = true)
        public Feign.Builder feignHystrixBuilder() {
            return HystrixFeign.builder().retryer(new  Retryer.Default(100,SECONDS.toMillis(1),1).clone());
        }
    }
}
