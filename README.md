一，简介
    1，此服务用于用户注册，登陆相关功能
    
二，接口说明
    1，图片验证码：image-code-white
    2，图片验证码：image-code
    3，手机号注册：phone-register；
        1）如果是微信端，则将openid绑定。
        2）注册用户通过utm标记来源
    4，email 注册：email-register
        1）如果是微信端，则将openid绑定。
        2）注册用户通过utm标记来源
    5，手机号登陆：phone-code
    6，登陆：login  ？
    7，手机验证码登陆：login/code 
    8，免密登陆：autologin
    9，手机找回密码：phone-findpassword
    10，email找回密码：email-findpassword
    11，意见反馈：feedback
    12，微信文章权限验证：read/check
    13，邮件验证码验证：email/validata
    14，邮件重置密码：email/reset-password
    