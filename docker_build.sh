#gradle build buildDockerfile
cd build/docker/
docker build -t user_center:v1 -f Dockerfile .
docker tag user_center:v1 172.16.20.2:8083/user_center:v1
docker push 172.16.20.2:8083/user_center:v1